import { unref } from "vue";
import { Document } from "yaml";

import { CREW_LABELS } from "@/globals";
import { Ship } from "@/ship/Ship";
import type { DehydratedShip } from "@/types/dehydrated";
import type { CrewType } from "@/types/prototypes";
import { sum } from "@/utils";

export function ship_yaml_export(ship: Ship | DehydratedShip): string {
    const root = new Document({
        name: ship.name ?? "",
        prefix: ship.prefix || undefined,
        callsign: ship.callsign || undefined,
        armour: ship.armour || 0,
        author: ship.author || undefined,
        notes: "",
        components: [],
        combat_stations: {},
    });

    if (ship.notes) {
        const notes = root.createNode(ship.notes);
        if ("value" in notes) {
            // makes TS happy
            notes.type = "BLOCK_LITERAL";
        }
        root.set("notes", notes);
    }

    for (const c of ship.components) {
        // avoid exporting internal stuff
        /** @type {YAMLMap} */
        const tmp = root.createNode({
            id: c.id,
            //type: c.prototype().type,
            volume: c.volume,
            info: c.info == null ? undefined : c.info,
        });
        tmp.flow = true;
        if ("prototype" in c) {
            tmp.commentBefore = ` ${
                c.prototype()?.name ?? c.id
            } | ${c.mass()}M | $${c.component_cost()}`;
        }
        root.addIn(["components"], tmp);
    }

    for (const station_id of Object.keys(ship.combat_stations)) {
        // hide stations and assignments with no crew
        const assignments: Partial<Record<CrewType, number | undefined>> = {};
        for (const crew_type of CREW_LABELS.keys()) {
            const amount = unref(ship.combat_stations[station_id].crew[crew_type]);
            if (amount) {
                assignments[crew_type] = amount;
            }
        }
        const total = sum(Object.values(assignments));
        const node = root.createNode(assignments);
        if (Object.values(assignments).length > 1) {
            node.comment = ` (total ${total})`;
        }
        if (total > 0) {
            const info = ship.combat_stations[station_id].info;
            root.addIn(["combat_stations", station_id], {
                crew: node,
                info: Object.keys(info).length == 0 ? undefined : info,
            });
        }
    }
    return root.toString({
        lineWidth: -1,
        minContentWidth: -1,
        indentSeq: false,
        flowCollectionPadding: false,
        defaultKeyType: "PLAIN",
        defaultStringType: "QUOTE_DOUBLE",
    });
}

/**
 * @deprecated
 */
export function ship_yaml_import(parsed: DehydratedShip): Ship {
    return new Ship(parsed);
}
