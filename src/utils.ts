/**
 * shorthand for <code>.reduce((a, b) => (a ?? 0) + (b ?? 0), 0)</code>
 * ignores null, undefined, and NaN
 * @param a iterable to sum over
 */
export function sum(a: Iterable<number | undefined | null>): number {
    const norm = (x: number | undefined | null) => (isNaN(x ?? 0) ? 0 : x ?? 0);
    return Array.from(a).reduce<number>((a, b) => norm(a) + norm(b), 0);
}

/**
 * combine maps by summing their values.
 * ignores undefined.
 * probably very slow.
 * @deprecated use CIGroup.combine or MIGroup.combine
 */
export function reduce_maps<T>(maps: Iterable<Map<T, number>>): Map<T, number> {
    const result = new Map();
    for (const map of maps) {
        for (const [k, v] of map.entries()) {
            if (v == undefined) continue;
            result.set(k, result.get(k) ?? 0 + v);
        }
    }
    return result;
}

/**
 * picks a random item from the provied array
 * @param a array to choose randomly from
 */
export function random_choice<T>(a: Array<T> | ReadonlyArray<T>): T {
    const index = Math.floor(Math.random() * a.length);
    return a[index];
}

/**
 * n-sided die.
 * @returns {number} in range [1,sides]
 */
export function die(sides: number): number {
    return Math.floor(Math.random() * sides) + 1;
}

/**
 * forces the sign to always be present even if the number is positive
 */
export function force_sign(x: number, opts: { zero?: "+" | "-" | "" } = {}): string {
    let prefix = "";
    if (x == 0) {
        prefix = opts.zero ?? "";
    } else if (x > 0) {
        prefix = "+";
    }
    return prefix + x.toLocaleString();
}
