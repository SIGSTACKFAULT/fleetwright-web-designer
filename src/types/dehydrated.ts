// types for "dehydrated" (i.e., serialized) ships, components, etc
import type { CrewType } from "@/types/prototypes";

export interface DehydratedFleet {
    __PLACEHOLDER__: boolean;
}

export interface DehydratedShip {
    name: string;
    prefix?: string;
    callsign?: string;
    armour: number;
    author?: string;
    notes?: string;
    components: DehydratedComponent[];
    combat_stations: Record<string, DehydratedCombatStation>;
}

export interface DehydratedComponent {
    id: string;
    volume: number;
    info?: { [key: string]: any };
}

export type DehydratedStationCrew = Record<CrewType, number>;

export interface DehydratedCombatStation {
    crew: Partial<DehydratedStationCrew>;
    info?: any;
}
