import { round10 } from "expected-round";
import { v1 as uuidv1 } from "uuid";

import { FLOATING_POINT_ROUNDING, data } from "../globals";
import type { ModuleSimulationState } from "../simulation";
import type { DehydratedComponent } from "./dehydrated";
import type { ModuleIterator } from "./interfaces";
import type { ComponentProto, ModuleProto } from "./prototypes";
import { eval_volume } from "@/expressions";
import type { ModuleInterface } from "@/modules/modules";

export class Component implements ModuleIterator {
    id: string;
    volume: number;
    info: { [key: string]: any } | undefined;
    _uuid: string;
    _state: ModuleSimulationState[];

    constructor({ id, volume, info }: DehydratedComponent) {
        this.id = id;
        this.volume = volume;
        this.info = info;
        this._uuid = uuidv1();
        this._state = new Array(this.prototype()?.modules?.length ?? 0).fill({});

        // do it this way to make TS happy
        const fixed_volume = this.prototype()?.fixed_volume;
        if (fixed_volume != undefined) {
            this.volume = fixed_volume;
        }
    }

    prototype(): ComponentProto | undefined {
        return data.components?.[this.id];
    }

    *modules(): Generator<ModuleInterface> {
        const prototype = this.prototype();
        if (prototype == undefined) {
            return;
        }
        for (const i in prototype.modules) {
            const module = prototype.modules[i];
            yield {
                context: {
                    component: this,
                },
                state: this._state[i] ?? {},
                prototype: module,
            };
        }
    }

    *modules_filtered<TModule extends { type: string }>(filter: ModuleProto["type"]) {
        for (const ctx of this.modules()) {
            if (ctx.prototype.type == filter) {
                yield ctx as ModuleInterface<TModule>;
            }
        }
    }

    get_single_module<TModule extends { type: string }>(type: string) {
        for (const ctx of this.modules()) {
            if (ctx.prototype.type == type) {
                return ctx as ModuleInterface<TModule>;
            }
        }
        return undefined;
    }

    set_id(new_id: string) {
        this.id = new_id;
    }

    set_volume(new_volume: number) {
        //console.log("set_volume", this.volume, "->", new_volume);
        this.volume = new_volume;
    }

    set_data(data: any) {
        this.info = data;
    }

    set_state(index: number, state: ModuleSimulationState) {
        console.log("[Component.set_state()", index, state, "=>", this._state);
        this._state[index] = state;
    }

    shielding_bonuses(): { mass: number; durability: number; cost: number } {
        if (this.info?.shielding == "light") {
            return {
                mass: this.volume * 4,
                durability: 10,
                cost: this.volume * 2,
            };
        }
        if (this.info?.shielding == "heavy") {
            return {
                mass: this.volume * 8,
                durability: 20,
                cost: 6 * this.volume,
            };
        }
        return {
            mass: 0,
            durability: 0,
            cost: 0,
        };
    }

    mass(): number {
        const density = this.prototype()?.density ?? 1;
        return round10(
            density * this.volume + this.shielding_bonuses().mass,
            FLOATING_POINT_ROUNDING,
        );
    }

    cost(): number {
        return this.component_cost() + this.resource_cost() + this.shielding_bonuses().cost;
    }

    component_cost(): number {
        const price: number | string | undefined = this.prototype()?.price;
        if (price === undefined) {
            return this.volume;
        }
        if (typeof price == "number") {
            return price * this.volume;
        }
        if (typeof price == "string") {
            return eval_volume(price, this.volume);
        }
        return 1;
    }

    resource_cost(): number {
        if (this.info?.filled) {
            let result = 0;
            for (const ctx of this.modules_filtered<any>("tank")) {
                const tank = ctx.prototype;
                const resource_proto = data.resources?.[tank.stores];
                if (resource_proto == undefined) {
                    return 0;
                }
                result += this.volume * (resource_proto.price ?? 0);
            }
            return result;
        } else {
            return 0;
        }
    }

    durability(): number {
        return (
            this.volume * (this.prototype()?.durability ?? 1) + this.shielding_bonuses().durability
        );
    }

    /**
     * should this be hidden by default in the VTT?
     */
    is_passive(): boolean {
        if (this.get_single_module("engine")) {
            return false;
        }
        if (this.get_single_module("ftl_drive")) {
            return false;
        }
        if (this.get_single_module("weapon_guided")) {
            return false;
        }
        if (this.get_single_module("weapon_direct")) {
            return false;
        }
        if (this.get_single_module("converter")) {
            return false;
        }
        return true;
    }

    /**
     * crew required for this component
     * "strategic" => maintenance crew
     * "tactical" => combat crew
     */
    crew(phase: "strategic" | "tactical"): number {
        const crew = this.get_single_module<any>("crew")?.prototype;
        if (crew === undefined) {
            return 0;
        }
        return (crew[phase]?.base ?? 0) + Math.ceil((crew[phase]?.per_volume ?? 0) * this.volume);
    }

    /**
     * convert to an object that can be JSON.stringified
     * @param options simulation: include simulation state
     */
    toJSON(options?: {
        simulation: true;
    }): DehydratedComponent & { _state: ModuleSimulationState[] };
    toJSON(options?: { simulation: false }): DehydratedComponent;
    toJSON(
        options: { simulation?: boolean } = {},
    ): DehydratedComponent & { _state?: ModuleSimulationState[] } {
        return {
            id: this.id,
            volume: this.volume,
            info: this.info || undefined,
            _state: options.simulation ?? false ? this._state : undefined,
        };
    }
}
