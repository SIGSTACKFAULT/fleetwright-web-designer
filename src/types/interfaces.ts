import type { DehydratedCombatStation, DehydratedComponent } from "./dehydrated";
import type { ModuleProto } from "./prototypes";
import type { ModuleInterface } from "@/modules/modules";

export type ComponentContext = DehydratedComponent;
export type CombatStationContext = DehydratedCombatStation;
export type FactionContext = { [x: string]: never }; // nothing yet
export type AnyContext = ComponentContext | CombatStationContext | FactionContext;

/**
 * Implemented by Ships, Components, Factions, and probably anything else I apply the module pattern to
 * #TODO avoid repeating the implementations of these methods somehow
 */
export interface ModuleIterator {
    modules(): Iterable<ModuleInterface>;

    modules_filtered<TModule extends { type: string }>(
        type: ModuleProto["type"],
        options?: any,
    ): Iterable<ModuleInterface<TModule>>;

    /**
     * return the first module of the given type
     * @param type
     * @returns the module, or `undefined` if not present
     */
    get_single_module<TModule extends { type: string }>(
        type: ModuleProto["type"],
    ): ModuleInterface<TModule> | undefined;
}

export function* unwrap_buff_choice(
    modules: Iterable<ModuleInterface>,
): Generator<ModuleInterface> {
    for (const module of modules) {
        if (module.prototype.type != "buff_choice") {
            yield module;
        } else {
            const index = module.persistence?.buff_choice;
            if (index === undefined) {
                console.warn(
                    "unwrap_buff_choice: buff_choice module found but no persistence.buff_choice. ignoring.",
                    module,
                );
                continue;
            }
            yield {
                prototype: {
                    type: "buff",
                    ...module.prototype.choices[index],
                },
                context: module.context,
                state: module.state,
            };
        }
    }
}

export function get_single_module<TModule = ModuleProto>(
    modules: ModuleProto[],
    type: ModuleProto["type"],
): TModule | undefined {
    if (modules == undefined) {
        return undefined;
    }
    for (const module of modules) {
        if (module.type == type) {
            return module as TModule;
        }
    }
}

/**
 * implemented by things which use industry capacity and/or money
 * e.g., Build Orders
 */
export interface Consumer {
    industry_consumed(): number;
    money_consumed(): number;
}
