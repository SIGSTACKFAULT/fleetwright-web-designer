export type CrewType =
    | "biological"
    | "construct"
    | "infomorph"
    | "mechanical"
    | "brainwashed"
    | "any";

export type ComponentProto = {
    name: string;
    type: string;
    density: number;
    durability: number;
    price: number | string;
    fixed_volume?: number;
    modules: ModuleProto[];
};

export type ModuleProto =
    | BuffModuleProto
    | BuffChoiceModuleProto
    | { type: "catapult"; [rest: string]: any }
    | { type: "command"; [rest: string]: any }
    | ConverterModuleProto
    | { type: "crew"; [rest: string]: any }
    | { type: "engine"; [rest: string]: any }
    | FTLModuleProto
    | { type: "magazine"; [rest: string]: any }
    | { type: "quarters"; [rest: string]: any }
    | { type: "recreational"; [rest: string]: any }
    | { type: "stores"; [rest: string]: any }
    | TankModuleProto
    | { type: "vls"; [rest: string]: any }
    | { type: "weapon_direct"; [rest: string]: any }
    | { type: "crit"; table: string; row: string };

export type BuffModuleProto = {
    type: "buff";
    scaling_function?: "volume" | "crew" | 1;
    stacking?: boolean;
    add?: Record<string, number>;
    multiply?: Record<string, number>;
    active?: any;
};

export type BuffChoiceModuleProto = {
    type: "buff_choice";
    choices: Omit<BuffModuleProto, "type">[];
};

/**
 * String is interpreted as an expression with parameters:
 * - v = the volume of the component
 *
 * The following parameters can be used if the component requests it in wants_variables:
 * - light_level = the light level of the ship
 *
 * Number is interpreted implicitly as:
 * value * v
 * where value is the input value and v is the volume of the component
 */
export type ConverterExpression = string | number;
export type ConverterVariableName = "light_level";

export type ConverterModuleProto = {
    type: "converter";
    output?: Record<string, ConverterExpression>;
    input?: Record<string, ConverterExpression>;
    wants_variables?: ConverterVariableName[];
};

export type EngineModuleProto = {
    type: "engine";
    thrust: number; // per volume
    fuel: Record<string, string>; // resource -> expr
};

export type FTLModuleProto = {
    type: "ftl_drive";
    ftl_method: "jump";
    jump_range: string; // expression; u = mass fraction;
    jump_str_energy: number; // needs refactor
    jump_tac_energy: number;
    jump_tac_tidi: number;
};

export type TankModuleProto = {
    type: "tank";
    stores: string; // resource
};

// ============================================================================ COMBAT STATIONS

export type CombatStationProto = {
    name?: string;
    levels: {
        requirements: string; // MathJS expressions
        modules: ModuleProto[];
    }[];
};

export type ResourceProto = {
    name: string;
    storeable?: boolean;
    price?: number;
};

// ============================================================================ MISSILES

// shared between guidance, payload, and propulsion
export type MissilePartProto = {
    name: string;
    flavour?: string;
    price: {
        light: number;
        heavy: number;
    };
};

// ============================================================================
// FACTION STUFF
// ============================================================================

export type GovernmentProto = {
    requirements: string; // expression
    stats: {
        authority_type: string[];
        legitimacy: number;
        legislation_time: number;
        debate_capacity: number;
        base_admin: number;
        admin_efficiency: number;
    };
    tags: string[];
};

export type PolicyProto = {
    requirements: string; // expression
    level?: number; // arbitrary metric of how good it is; used in event chances
    admin_cost: string; // expression
    modules: ModuleProto[];
};

export type EducationLevelProto = {
    modules: ModuleProto[];
};

export type EducationProto = {
    levels: EducationLevelProto[];
};

export type WorksProto = {
    requirements?: string; // expression
    admin_cost?: string; // expression
    modules?: ModuleProto[];
};
