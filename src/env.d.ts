// actual values in vite.config.js
// this is just to trick TypeScript into not flagging them in TheFooter.vue
declare const __WEB_GIT_DESCRIBE__: string;
declare const __META_GIT_DESCRIBE__: string;
// same but used in TheToolbar.vue
declare const __APP_NAME__: string;
declare const __APP_ACRONYM__: string;

declare module "vue3-sortablejs";
declare module "expected-round";
declare module "vuetify/lib/util/colors";
declare module "vuetify-notifier";
