import { inject } from "vue";

export default function useDebug() {
    return {
        DEBUG: inject<boolean>("DEBUG", false),
        DEBUG_LEVEL: inject<number>("DEBUG_LEVEL", 0),
    };
}
