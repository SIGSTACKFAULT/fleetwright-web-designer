import { watchDebounced } from "@vueuse/core";
import { type Ref, ref, shallowReactive, toRaw, watch } from "vue";

import { Faction } from "./faction/Faction";
import type { DehydratedFaction } from "./faction/Faction";
import type { DehydratedShip } from "./types/dehydrated";
import { Ship } from "@/ship/Ship";

const DELAY = 500;
const MAX_DELAY = 1000;

export function force_save() {
    save_ships();
    save_factions();
}

// ============================================================================ SHIPS

export const ships: DehydratedShip[] = shallowReactive(
    JSON.parse(localStorage.getItem("ships") ?? "[]"),
);
export function save_ships() {
    console.log("[autosave] saving ships", toRaw(ships));
    localStorage.setItem("ships", JSON.stringify(ships));
}
watchDebounced(ships, save_ships, { debounce: DELAY, maxWait: MAX_DELAY });
export function useShip(index: number): Ref<Ship> {
    const ship = ref(new Ship(ships[index]));
    watch(
        () => ship.value.toJSON(),
        (data: DehydratedShip) => {
            console.log(`ships[${index}] updated`);
            ships[index] = data;
        },
    );
    return ship;
}

// ============================================================================ FACTIONS

export const factions: DehydratedFaction[] = shallowReactive(
    JSON.parse(localStorage.getItem("factions") ?? "[]"),
);
export function save_factions() {
    console.log("[autosave] saving factions", toRaw(factions));
    localStorage.setItem("factions", JSON.stringify(factions));
}
watchDebounced(factions, save_factions, { debounce: DELAY, maxWait: MAX_DELAY });
export function useFaction(index: number): Ref<Faction> {
    const faction = ref(new Faction(factions[index]));
    watch(
        () => faction.value.toJSON(),
        (data: DehydratedFaction) => {
            factions[index] = data;
        },
    );
    return faction;
}
