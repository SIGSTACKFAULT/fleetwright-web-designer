import type { Component as VueComponent } from "vue";

import type { Faction } from "@/faction/Faction";
import type { Ship } from "@/ship/Ship";
import type { ModuleSimulationState } from "@/simulation";
import type { Component } from "@/types/Component";
import type { DehydratedCombatStation } from "@/types/dehydrated";

import BuffChoiceModule from "./BuffChoiceModule.vue";
import BuffModule from "@/modules/BuffModule.vue";
import CatapultModule from "@/modules/CatapultModule.vue";
import CommandModule from "@/modules/CommandModule.vue";
import ConverterModule from "@/modules/ConverterModule.vue";
import CrewModule from "@/modules/CrewModule.vue";
import CritModule from "@/modules/CritModule.vue";
import EngineModule from "@/modules/EngineModule.vue";
import FTLModule from "@/modules/FTLModule.vue";
import MagazineModule from "@/modules/MagazineModule.vue";
import QuartersModule from "@/modules/QuartersModule.vue";
import RecreationalModule from "@/modules/RecreationalModule.vue";
import ShieldingModule from "@/modules/ShieldingModule.vue";
import StoresModule from "@/modules/StoresModule.vue";
import TankModule from "@/modules/TankModule.vue";
import VLSModule from "@/modules/VLSModule.vue";
import WeaponDirectModule from "@/modules/WeaponDirectModule.vue";

export interface ModuleInterface<TProto extends { type: string } = any> {
    context:
        | {
              ship?: Ship;
              component: Component;
          }
        | {
              ship?: Ship;
              combat_station: DehydratedCombatStation;
          }
        | {
              faction: Faction;
              part: "government" | "policy" | "education" | "works";
          };
    prototype: TProto;
    state: ModuleSimulationState;
    persistence?: { [key: string]: any };
    readonly?: boolean;
}

/**
 * The component we use for each module.
 * null indicates a placeholder.
 */
export const MODULE_COMPONENTS = new Map<string, VueComponent<ModuleInterface<any>> | null>([
    ["buff", BuffModule],
    ["buff_choice", BuffChoiceModule],
    ["command", CommandModule],
    ["converter", ConverterModule],
    ["engine", EngineModule],
    ["ftl_drive", FTLModule],
    ["quarters", QuartersModule],
    ["stores", StoresModule],
    ["tank", TankModule],
    ["weapon_direct", WeaponDirectModule],
    ["vls", VLSModule],
    ["catapult", CatapultModule],
    ["magazine", MagazineModule],
    ["recreational", RecreationalModule],
    // intentionally at the bottom
    ["crew", CrewModule],
    ["crit", CritModule],
    ["shielding", ShieldingModule],
]);

/**
 * is this a module type we have a component for?
 */
export function is_known(module_type: string): boolean {
    return MODULE_COMPONENTS.has(module_type);
}
