import { v4 as uuid_v4 } from "uuid";

import type { DehydratedComponent, DehydratedShip } from "../types/dehydrated";
import { combat_level } from "@/combat_stations";
import { deltav } from "@/engines";
import { data } from "@/globals";
import type { ModuleInterface } from "@/modules/modules";
import { simulate_buff, simulate_jump_range } from "@/simulation";
import { Component } from "@/types/Component";
import type { ModuleIterator } from "@/types/interfaces";
import type { CrewType, FTLModuleProto, ModuleProto } from "@/types/prototypes";
import { sum } from "@/utils";

export class Ship implements ModuleIterator {
    /**
     * just so dragndrop can keep track of which is which
     */
    _sortable_unique = uuid_v4();
    name: string;
    prefix: string;
    callsign: string;
    armour: number;
    author: string;
    notes: string;
    components: Component[];
    combat_stations: {
        [id: string]: {
            crew: Record<CrewType, number | undefined>;
            info?: any;
        };
    };
    _light_level: number = 100;
    _faction_morale: number = 0;

    constructor({
        name,
        prefix,
        callsign,
        armour,
        author,
        notes,
        components,
        combat_stations,
    }: Partial<DehydratedShip>) {
        this.name = name ?? "";
        this.prefix = prefix ?? "";
        this.callsign = callsign ?? "";
        this.armour = armour ?? 0;
        this.author = author ?? "";
        this.notes = notes ?? "";
        this.components = components?.map((c) => new Component(c)) ?? [];
        this.combat_stations = {};

        for (const id in combat_stations) {
            // handle old combat station schema
            const assignment: any = combat_stations[id]?.crew ?? combat_stations[id] ?? {};
            this.combat_stations[id] = { crew: assignment, info: combat_stations[id]?.info ?? {} };
        }
        // console.trace("new Ship()");
    }

    toJSON(): DehydratedShip {
        return {
            name: this.name ?? "",
            prefix: this.prefix || undefined,
            callsign: this.callsign || undefined,
            armour: this.armour || 0,
            author: this.author || undefined,
            notes: this.notes.trim() || undefined,
            components: this.components.map((c) => c.toJSON()),
            combat_stations: this.combat_stations,
        };
    }

    /**
     * makes a copy by serializing and deserializing
     */
    clone(): Ship {
        return new Ship(this.toJSON());
    }

    /**
     * add the given component (insert at the given index)
     * @param component
     * @param index
     */
    add_component(component: DehydratedComponent, index = -1) {
        const target = new Component(component);
        if (index == -1) {
            this.components.push(target);
        } else {
            this.components.splice(index, 0, target);
        }
    }

    /**
     * remove the given component
     * @param component
     */
    remove_component(component: Component) {
        const i = this.components.indexOf(component);
        if (i == -1) {
            throw new Error("component not present");
        }
        this.components.splice(i, 1);
    }

    /**
     * total volume of all components
     */
    total_volume(): number {
        let sum = 0;
        for (const c of this.components) {
            sum += c.volume;
        }
        return sum;
    }

    /**
     * total mass of ship's components
     */
    total_component_mass(): number {
        let sum = 0;
        for (const c of this.components) {
            sum += c.mass();
            //console.log(`total_component_mass: ${c.volume} -> ${sum}`);
        }
        return sum;
    }

    /**
     * total cost of the ship
     */
    total_cost(): number {
        let sum = this.armour * 0.2; // armour costs 1/mass
        for (const c of this.components) {
            sum += c.cost();
        }
        return Math.ceil(sum);
    }

    /**
     * total durability of the ship
     */
    total_durability() {
        let sum = 0;
        for (const c of this.components) {
            sum += c.durability();
        }
        return sum;
    }

    /**
     * total mass of the ship
     */
    total_mass() {
        let sum = 0;
        sum += this.total_component_mass();
        sum += this.armour;
        return sum;
    }

    protection(): number {
        const m = this.total_mass();
        const v = this.total_volume();
        return Math.floor(120 * (m / Math.pow(v, 2 / 3)));
    }

    *modules(): Generator<ModuleInterface> {
        for (const component of this.components) {
            yield* component.modules();
        }
        for (const [id, station] of Object.entries(this.combat_stations)) {
            const prototype = data.combat_stations?.[id];
            if (prototype === undefined) {
                console.warn(`prototype for ${id} is undefined?!`);
                continue;
            }
            const level = combat_level(station, prototype, this.worstcase_crew(), this.morale());
            if (level == 0) {
                continue;
            }
            const level_proto = prototype.levels[level - 1];
            if (level_proto == undefined) {
                console.warn(`prototype for ${id} level ${level} is undefined?!`);
                continue;
            }
            for (const module of level_proto.modules) {
                yield {
                    prototype: module,
                    state: { enabled: true },
                    context: {
                        ship: this,
                        combat_station: station,
                    },
                    persistence: station.info,
                };
            }
        }
    }

    *modules_filtered<TModule extends { type: string }>(
        filter: ModuleProto["type"],
        options: { skip_stations?: boolean } = {},
    ): Iterable<ModuleInterface<TModule>> {
        for (const component of this.components) {
            yield* component.modules_filtered<TModule>(filter);
        }
        if (options.skip_stations != true) {
            for (const [id, station] of Object.entries(this.combat_stations)) {
                // avoiding calling `this.morale` unnecissarily
                // to avoid infinite loops with `assign_quarters`
                // and it's expensive
                const prototype = data.combat_stations?.[id];
                if (prototype == undefined) {
                    continue;
                }
                // does it have any of the module we're interested in?
                const all_module_ids = prototype?.levels.flatMap((level) =>
                    level.modules.map((module) => module.type),
                );
                const has_any_module_we_care_about = all_module_ids?.includes(filter);
                if (!has_any_module_we_care_about) {
                    continue;
                }

                const level = combat_level(
                    station,
                    prototype,
                    this.worstcase_crew(),
                    this.morale(),
                );
                if (level == 0) {
                    continue;
                }
                const level_proto = prototype.levels[level - 1];
                if (level_proto == undefined) {
                    console.error("no prototype for", id, "level", level);
                    continue;
                }
                for (const module of level_proto.modules) {
                    if (module.type == filter) {
                        yield {
                            prototype: module as TModule,
                            state: { enabled: true },
                            context: {
                                ship: this,
                                combat_station: station,
                            },
                            persistence: station.info,
                        };
                    }
                }
            }
        }
    }

    get_single_module<TModule extends { type: string }>(type: ModuleProto["type"]) {
        for (const ctx of this.modules()) {
            if (ctx.prototype.type == type) {
                return ctx as ModuleInterface<TModule>;
            }
        }
        return undefined;
    }

    /**
     * Ship's morale after everything
     * will eventually work in combat
     */
    morale(): number {
        const assignments = this.assign_quarters();
        let highest_stores = Math.max(
            ...Array.from(this.modules_filtered<any>("stores")).map((m) => m.prototype.morale ?? 0),
        );
        if (highest_stores == -Infinity) {
            highest_stores = 0;
        }
        const base = this._faction_morale + assignments.morale + highest_stores;
        return simulate_buff(this, "morale", base, { skip_stations: true });
    }

    /**
     * crew from components and combat stations
     * @param phase 'strategic' or 'tactical'
     */
    _crew(phase: "strategic" | "tactical") {
        let total = 0;
        for (const c of this.components) {
            total += c.crew(phase);
        }
        if (phase == "tactical") {
            for (const station of Object.values(this.combat_stations)) {
                total += sum(Object.values(station.crew));
            }
        }
        return total;
    }

    /**
     * final actual worst-case crew for real
     */
    worstcase_crew(): number {
        return Math.max(this._crew("strategic"), this._crew("tactical"));
    }

    /**
     * Ship's best jump range
     */
    jump_range(): number {
        const ranges = Array.from(this.modules_filtered<FTLModuleProto>("ftl_drive")).map(
            (module) => simulate_jump_range(this, module),
        );
        const highest = Math.max(0, ...ranges.map((x) => x.jump_range));
        return Math.floor(highest);
    }

    /**
     * total space in quarters
     */
    quarters(): number {
        let total = 0;
        for (const module of this.modules()) {
            if ("component" in module.context) {
                if (module.prototype.type == "quarters") {
                    if (module.context.component) {
                        total += module.prototype.crew * (module.context.component.volume ?? 0);
                    }
                }
            }
        }
        return total;
    }

    /**
     * assign crew to quarters
     * #TODO cache?
     */
    assign_quarters(): {
        leftover: number;
        morale: number;
        details: QuartersAssignment[];
    } {
        const worstcase = this.worstcase_crew();
        let quarters = Array.from(this.modules_filtered<any>("quarters"));
        quarters = quarters.filter((m) => m.state.enabled ?? true);
        quarters = quarters.sort((a, b) => (b.prototype.morale ?? 0) - (a.prototype.morale ?? 0));
        const details: QuartersAssignment[] = [];
        let morale: number | undefined = undefined;
        let leftover = worstcase;
        for (const module of quarters) {
            if (!("component" in module.context)) {
                // combat stations shouldn't have quarters anyways
                continue;
            }
            const beds =
                module.prototype.crew *
                ("component" in module.context ? module.context.component.volume : 0);
            const assigned = Math.min(leftover, beds);
            details.push({
                assigned,
                beds,
                module: {
                    // #TODO the 'in' check doesn't narrow the type of `module`; is there a workaround?
                    context: module.context,
                    prototype: module.prototype,
                    state: module.state,
                },
            });
            if (leftover > 0) {
                leftover -= assigned;
                morale = module.prototype.morale ?? 0;
            }
        }
        const result = { details: details, leftover, morale: morale ?? 0 };
        // console.log("[Ship.assign_quarters()] done", result);
        return result;
    }

    /**
     * # of components where `Component.is_passive()` returns true
     */
    count_passives(): number {
        return this.components.filter((x) => x.is_passive()).length;
    }

    /**
     * Delta-V *estimation*
     * @deprecated
     */
    deltav(): number {
        return deltav(this);
    }
}

type QuartersAssignment = {
    module: ModuleInterface<any>;
    beds: number;
    assigned: number;
};
