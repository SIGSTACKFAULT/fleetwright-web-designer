import type { Ship } from "@/ship/Ship";

type Row = {
    group: string;
    volume: number;
    mass: number;
    cost: number;
};

export function table(ship: Ship, sort_by?: "volume" | "mass" | "cost"): Row[] {
    const groups: { [group: string]: { volume: number; mass: number; cost: number } } = {};
    for (const component of ship.components) {
        const group = component.prototype()?.type ?? "???";
        groups[group] = {
            volume: component.volume + (groups[group]?.volume ?? 0),
            mass: component.mass() + (groups[group]?.mass ?? 0),
            cost: component.cost() + (groups[group]?.cost ?? 0),
        };
    }
    const result = Object.entries(groups).map<Row>(([k, v]) => ({ group: k, ...v }));
    if (sort_by) {
        result.sort((a, b) => b[sort_by] - a[sort_by]);
    }
    return result;
}
