<script setup lang="ts">
import { useMagicKeys } from "@vueuse/core";
import { useVModel } from "@vueuse/core";
import { computed, watch } from "vue";

import { _engine_deltav, _engines_info, acceleration, thrust } from "@/engines";
import { Ship } from "@/ship/Ship";
import { simulate_buff, simulate_ecm, simulate_resource_delta } from "@/simulation";
import useDebug from "@/useDebug";
import { force_sign } from "@/utils";

import ShipCostChip from "./ShipCostChip.vue";
import ShipMassChip from "./ShipMassChip.vue";
import ShipVolumeChip from "./ShipVolumeChip.vue";
import NumberChip from "@/components/NumberChip.vue";

const { DEBUG, DEBUG_LEVEL } = useDebug();
const props = defineProps<{
    ship: Ship;
    simulation: boolean;
}>();
const emits = defineEmits(["update:simulation"]);

const simulation = useVModel(props, "simulation", emits);
const { alt_a } = useMagicKeys();

watch(alt_a, (v) => {
    if (v) {
        simulation.value = !simulation.value;
    }
});

const sim_chip_color = computed(() => {
    if (props.simulation == false || props.ship.components.length == 0) {
        return "disabled";
    } else {
        return "simulation";
    }
});

function format_wia_kia() {
    const ratio = simulate_buff(props.ship, "wia_ratio", 0.5);
    return `${Math.round(ratio * 100)}:${Math.round((1 - ratio) * 100)} `;
}
</script>

<template>
    <section id="overview" class="flex flex-col gap-2">
        <div class="flex gap-2 chip-box big">
            <ShipVolumeChip :ship="ship" data-cy="overview-volume" />
            <ShipMassChip :ship="ship" data-cy="overview-mass" />
            <ShipCostChip :ship="ship" data-cy="overview-cost" />
            <NumberChip name="Integrity" data-cy="overview-integrity" tooltip="Health">
                {{ Math.floor(ship.total_durability()) }}
            </NumberChip>
            <NumberChip name="Protection" data-cy="overview-integrity" tooltip="Reduces damage">
                {{ ship.protection().toLocaleString() }}
            </NumberChip>
            <div class="hidden print:block">
                <NumberChip name="Armour Mass">
                    {{ ship.armour }}
                </NumberChip>
            </div>
        </div>
        <div class="flex gap-2 chip-box">
            <NumberChip name="Sensors" data-cy="overview-sensors">
                {{ simulate_buff(ship, "sensors") }}
            </NumberChip>
            <NumberChip name="Crew" title="required / quarters" data-cy="overview-crew">
                {{ ship.worstcase_crew() }}/{{ ship.quarters() }}
            </NumberChip>
            <NumberChip name="Jump" data-cy="overview-jump">
                {{ ship.jump_range() }}
            </NumberChip>
            <NumberChip name="Clear Targeting" data-cy="overview-cleartargeting">
                {{ Math.floor(Math.sqrt(simulate_buff(ship, "sensors"))) }}
            </NumberChip>
            <NumberChip name="Repair Points" data-cy="overview-repair">
                {{ Math.floor(simulate_buff(ship, "repair")) }}
            </NumberChip>
            <NumberChip name="WIA:KIA" data-cy="overview-wiakia">
                {{ format_wia_kia() }}
            </NumberChip>
        </div>
        <div class="flex gap-2 chip-box sim-box big" :data-sim="simulation">
            <div class="print:hidden self-stretch flex items-stretch">
                <v-sheet
                    class="chip"
                    :color="ship.components.length == 0 ? 'disabled' : 'simulation'"
                >
                    <v-tooltip v-if="ship.components.length == 0" activator="parent" location="top">
                        Simulation Mode -- Add some components first!
                    </v-tooltip>
                    <v-tooltip v-else activator="parent" location="top">
                        Toggle Simulation Mode
                    </v-tooltip>
                    <v-switch
                        v-model="simulation"
                        data-cy="simulation"
                        :disabled="ship.components.length == 0"
                        inset
                        color="secondary"
                        hide-details="auto"
                        density="compact"
                        label="Simulate"
                    />
                </v-sheet>
            </div>
            <v-menu open-on-hover>
                <template #activator="{ props: trigger }">
                    <NumberChip
                        :color="sim_chip_color"
                        name="Thrust"
                        data-cy="overview-thrust"
                        v-bind="trigger"
                    >
                        {{ thrust(ship).toLocaleString() }}
                    </NumberChip>
                </template>
                <v-sheet v-if="_engines_info(ship).length > 0" class="p-4">
                    <table class="border-collapse compact">
                        <thead style="border-bottom: 1px solid">
                            <th>Component</th>
                            <th class="pl-4">Thrust</th>
                            <th class="pl-4">Burn Time</th>
                        </thead>
                        <tr
                            v-for="(engine, i) in _engines_info(ship)"
                            :key="i"
                            :class="{ 'text-grey': !engine.enabled }"
                        >
                            <td>
                                <span v-if="!engine.enabled" class="text"> (disabled) </span>
                                {{ engine.name() }}
                            </td>
                            <td class="text-right font-bold mono">
                                {{ engine.thrust.toLocaleString() }}
                            </td>
                            <td class="text-right font-bold mono">
                                {{ engine.burn_time().toLocaleString() }}
                            </td>
                        </tr>
                    </table>
                </v-sheet>
            </v-menu>
            <v-tooltip location="bottom">
                <template #activator="{ props: trigger }">
                    <NumberChip
                        v-bind="trigger"
                        :color="sim_chip_color"
                        name="Accel."
                        title="acceleration"
                        data-cy="overview-acceleration"
                    >
                        {{ Math.floor(acceleration(ship)) }}
                    </NumberChip>
                </template>
                Ship can change it's velocity vector by this many hexes per turn.
            </v-tooltip>
            <v-menu open-on-hover>
                <template #activator="{ props: trigger }">
                    <NumberChip
                        v-bind="trigger"
                        :color="sim_chip_color"
                        name="&Delta;V"
                        abbr_style="letter-spacing:-4px;"
                        data-cy="overview-dv"
                    >
                        {{ Math.floor(ship.deltav()).toLocaleString() }}
                    </NumberChip>
                </template>
                <v-sheet v-if="_engines_info(ship).length > 0" class="p-4">
                    <table class="border-collapse compact">
                        <thead style="border-bottom: 1px solid">
                            <th>Component</th>
                            <th>&Delta;V</th>
                        </thead>
                        <tr
                            v-for="(engine, i) in _engines_info(ship)"
                            :key="i"
                            :class="{ 'text-grey': !engine.enabled }"
                        >
                            <td>
                                <span v-if="!engine.enabled" class="text"> (disabled) </span>
                                {{ engine.name() }}
                            </td>
                            <td class="pl-2 mono font-bold text-right" style="min-width: 6ch">
                                {{
                                    Math.floor(
                                        _engine_deltav(ship, engine.module).deltav,
                                    ).toLocaleString()
                                }}
                            </td>
                        </tr>
                    </table>
                </v-sheet>
            </v-menu>
            <NumberChip :color="sim_chip_color" name="Morale" data-cy="overview-morale">
                {{ force_sign(ship.morale()) }}
            </NumberChip>
            <NumberChip :color="sim_chip_color" name="ECM" data-cy="overview-ecm">
                {{ Math.floor(simulate_ecm(ship)) }}
            </NumberChip>
        </div>
        <div class="flex gap-2 chip-box sim-box" :data-sim="simulation">
            <NumberChip :color="sim_chip_color" name="Energy" data-cy="overview-energy">
                {{ Math.floor(simulate_resource_delta(ship, "ENERGY")) }}
            </NumberChip>
            <NumberChip :color="sim_chip_color" name="Heat" data-cy="overview-heat">
                {{ Math.floor(simulate_resource_delta(ship, "HEAT")) }}
            </NumberChip>
        </div>
    </section>
</template>

<style scoped lang="less">
.chip-box {
    display: flex;
    flex-wrap: wrap;
    max-width: max-content;
}

@media print {
    .sim-box {
        display: none;
    }

    .sim-box[data-sim="true"] {
        display: flex;
    }
}
</style>
