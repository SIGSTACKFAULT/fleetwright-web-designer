<script setup lang="ts">
import { computed } from "vue";
import { ref } from "vue";

import type { Ship } from "./Ship";
import useDebug from "@/useDebug";
import { die, sum } from "@/utils";

import CommonDialog from "@/components/dialogs/CommonDialog.vue";

const { DEBUG, DEBUG_LEVEL } = useDebug();

const FLAVOUR: Record<
    string,
    Record<string, { repair_flavour: string; repair_fn?: () => number; effect_flavour: string }>
> = {
    Power: {
        "Propellant Leak": {
            repair_flavour: "1 per 3 total volume of Propellant Tank",
            effect_flavour:
                "Lose 5% of your maximum Delta V each turn until repaired. Certain propellants have worse leaks. Orion Drive bombs and Fissile Pellets only have a one-time loss of 5% max propellant, without an ongoing leak.",
        },
        "Radiators Damaged": {
            repair_flavour: "1 per total volume of power plant",
            effect_flavour:
                "-50% total Energy production until repaired. Multiple crits of this type stack multiplicatively.",
        },
        "Reactor Offline": {
            repair_flavour: "2 per volume of power plant disabled",
            effect_flavour:
                "One of your largest power plants stops generating Energy until repaired.",
        },
        "Engine Offline": {
            repair_flavour: "2 per volume of drive disabled",
            effect_flavour: "One of your largest drives stops generating Thrust until repaired.",
        },
        "Jump Offline": {
            repair_flavour: "2 per volume of Jump Drive disabled",
            effect_flavour: "One of your largest Jump Drives cannot be used until repaired.",
        },
    },
    Systems: {
        "Sensors Damaged": {
            repair_flavour: "1 per total Sensor volume",
            effect_flavour:
                "-50% Sensor rating until repaired. Multiple crits of this type stack multiplicatively.",
        },
        "ECM Damaged": {
            repair_flavour: "1 per total ECM volume",
            effect_flavour:
                "-50% ECM rating until repaired. Multiple crits of this type stack multiplicatively.",
        },
        "Weapon Disabled": {
            repair_flavour: "2 per volume of weapon disabled",
            effect_flavour:
                "One of your largest weapons can't be fired until repaired. If that weapon is a VLS, you also receive the effect of a Magazine Hit.",
        },
        "Magazine Hit": {
            repair_flavour: "Not Repairable.",
            effect_flavour:
                "Lose 25% of loaded Guided Weapons. If your largest Magazine is a VLS, it is also Disabled as per the Weapon Disabled crit.",
        },
        "Command Hit": {
            repair_flavour: "Not Repairable.",
            effect_flavour:
                "One of your Command Modules is destroyed. All personnel manning said module become Casualties.",
        },
    },
    Personnel: {
        "Hull Breach: Damcon": {
            repair_flavour: "N/A",
            effect_flavour: "30% of Crew assigned to Damage Control become Casualties",
        },
        "Hull Breach: Medevac": {
            repair_flavour: "N/A",
            effect_flavour: "50% of Crew assigned to Medevac become Casualties",
        },
        "Hull Breach: Medbay": {
            repair_flavour: "N/A",
            effect_flavour: "50% of Crew assigned to the Medbay become Casualties",
        },
        "Hull Breach: Engineering": {
            repair_flavour: "N/A",
            effect_flavour: "50% of Crew assigned to Engineering become Casualties",
        },
        "Hull Breach: Nerve Center": {
            repair_flavour: "N/A",
            effect_flavour: "50% of Crew assigned to the Nerve Center become Casualties",
        },
    },
} as const;

const props = withDefaults(
    defineProps<{
        ship: Ship;
        table: string;
        compact?: boolean;
    }>(),
    {
        compact: false,
    },
);

const modules = computed(() =>
    Array.from(props.ship.modules_filtered<any>("crit")).filter(
        (m) => m.prototype.table == props.table,
    ),
);
const module_volumes = computed(() =>
    Array.from(
        modules.value.map((m) => {
            if ("component" in m.context) {
                return m.context.component.volume;
            }
            if ("combat_station" in m.context) {
                return sum(Object.values(m.context.combat_station.crew));
            }
            return NaN;
        }),
    ),
);
const row_volumes = computed<Array<number>>(() => {
    return Array.from(Object.keys(FLAVOUR[props.table])).map<number>((row) => {
        let total = 0;
        for (let i = 0; i < modules.value.length; i++) {
            if (modules.value[i].prototype.row == row) {
                total += module_volumes.value[i];
            }
        }
        return total;
    });
});
const row_weights = computed<number[]>(() => {
    const DIE_SIDES = 20;
    const total_volume = sum(row_volumes.value);
    const target_fractions = Array.from(row_volumes.value.map((v) => v / total_volume));
    const result = row_volumes.value.map((v) => (v > 0 ? 1 : 0));
    const actually_present_rows = sum(result);
    if (result.length == 0 || actually_present_rows == 0) {
        // probably not loaded yet
        return result.map(() => 0);
    }
    for (let i = 0; i < DIE_SIDES - actually_present_rows; i++) {
        const diff_from_target: [number, number][] = target_fractions.map((frac, i) => [
            i,
            frac - result[i] / DIE_SIDES,
        ]);
        diff_from_target.sort(([, a], [, b]) => b - a);
        const [k] = diff_from_target[0];
        result[k] += 1;
    }
    if (sum(result) != DIE_SIDES) {
        console.warn("row_weights() had a bad problem :(", result, target_fractions, total_volume);
    }
    return result;
});
const row_ranges = computed<([number, number] | null)[]>(() => {
    const result: ([number, number] | null)[] = [];
    for (let i = 0; i < row_weights.value.length; i++) {
        if (row_weights.value[i] > 0) {
            let lo = sum(row_weights.value.slice(0, i)) + 1;
            let hi = lo + row_weights.value[i] - 1;
            result.push([lo, hi]);
        } else {
            result.push(null);
        }
    }
    return result;
});

const roll_result = ref<null | number>(null);
function roll() {
    const roll = die(20);
    for (let i = 0; i < row_ranges.value.length; i++) {
        const range = row_ranges.value[i];
        if (range == null) continue;
        if (range[0] <= roll && roll <= range[1]) {
            roll_result.value = i;
            return;
        }
    }
}

function roll_reset() {
    roll_result.value = null;
}

defineExpose({ roll, roll_reset });
</script>

<template>
    <div class="self-stretch">
        <table class="mb-4 break-inside-avoid" style="table-layout: fixed; width: 100%">
            <thead
                style="
                    background-color: rgb(var(--v-theme-simulation));
                    color: rgb(var(--v-theme-on-simulation));
                "
            >
                <th class="w-48">
                    {{ table }}
                </th>
                <th class="font-normal w-36">d20</th>
                <template v-if="!compact">
                    <th class="font-normal">Repair</th>
                    <th class="font-normal">Effect</th>
                </template>
                <th v-else></th>
            </thead>
            <tr
                v-for="(data, row, i) in FLAVOUR[table]"
                :key="i"
                :class="{ rolled: i == roll_result }"
            >
                <th class="h-10 font-normal">{{ row }}</th>
                <td v-if="row_ranges[i] == null" class="text-center">&osol;</td>
                <td v-else>
                    <div class="flex gap-2 items-center ml-2">
                        <div class="mono" style="width: 5ch; text-align: center">
                            <template v-if="row_ranges[i]![0] == row_ranges[i]![1]">
                                {{ row_ranges[i]![0] }}
                            </template>
                            <template v-else>
                                <!-- use non-breaking space (\xa0) to ensure the dash is always in the center -->
                                {{ String(row_ranges[i]![0]).padStart(2, "\xa0") }}-{{
                                    String(row_ranges[i]![1]).padEnd(2, "\xa0")
                                }}
                            </template>
                        </div>
                        <div class="flex flex-wrap grow" style="gap: 1px">
                            <span v-for="j in row_weights[i]" :key="j" class="w-2 h-2 bg-red-600" />
                        </div>
                    </div>
                </td>
                <template v-if="!compact">
                    <td>
                        {{ data.repair_flavour || "[missing]" }}
                    </td>
                    <td class="py-1">
                        {{ data.effect_flavour || "[missing]" }}
                    </td>
                </template>
                <template v-else>
                    <td class="text-right">
                        <v-btn class="mx-4" density="compact" variant="text" icon>
                            <v-icon>mdi mdi-information-outline</v-icon>
                            <CommonDialog>
                                <template #title>
                                    Crit: &nbsp;<b>{{ row }}</b>
                                </template>

                                <div class="flex gap-2 items-center">
                                    <b>Roll:</b>
                                    <div class="mono" style="width: 6ch; text-align: center">
                                        <template v-if="row_ranges[i]![0] == row_ranges[i]![1]">
                                            {{ row_ranges[i]![0] }}
                                        </template>
                                        <template v-else>
                                            <!-- use non-breaking space (\xa0) to ensure the dash is always in the center -->
                                            {{ String(row_ranges[i]![0]).padStart(2, "\xa0") }}-{{
                                                String(row_ranges[i]![1]).padEnd(2, "\xa0")
                                            }}
                                        </template>
                                    </div>
                                    <div class="flex flex-wrap grow" style="gap: 1px">
                                        <span
                                            v-for="j in row_weights[i]"
                                            :key="j"
                                            class="w-2 h-2 bg-red-600"
                                        />
                                    </div>
                                </div>

                                <p>
                                    <b>Repair:</b>
                                    {{ data.repair_flavour || "[missing]" }}
                                </p>
                                <p>
                                    <b>Effect:</b>
                                    {{ data.effect_flavour || "[missing]" }}
                                </p>
                            </CommonDialog>
                        </v-btn>
                    </td>
                </template>
            </tr>
        </table>
    </div>
    <VExpansionPanels v-if="DEBUG" class="self-start">
        <VExpansionPanel>
            <VExpansionPanelTitle>
                {{ table }}
            </VExpansionPanelTitle>
            <VExpansionPanelText>
                <div class="flex gap-2">
                    <v-btn variant="tonal" @click="roll">Roll</v-btn>
                    <v-btn variant="plain" @click="roll_reset">Reset</v-btn>
                </div>
                <p>roll_result = {{ roll_result ?? "null" }}</p>
                <p>total_volume = {{ sum(module_volumes) }}</p>
                <table class="debug">
                    <tr>
                        <th>[i]</th>
                        <th>component</th>
                        <th>table</th>
                        <th>row</th>
                        <th>volume</th>
                    </tr>
                    <tr v-for="(v, i) in modules" :key="i">
                        <th>[{{ i }}]</th>
                        <td>{{ "component" in v.context ? v.context.component.id : "???" }}</td>
                        <td>{{ v.prototype.table }}</td>
                        <td>{{ v.prototype.row }}</td>
                        <td>{{ module_volumes[i] }}</td>
                    </tr>
                </table>
                <br />
                <b>Rows</b>
                <table class="debug">
                    <tr>
                        <th>Row</th>
                        <th>Volume</th>
                        <th>Weight</th>
                        <th>Range</th>
                    </tr>
                    <tr v-for="(v, i) in row_volumes" :key="i">
                        <td class="text-left">{{ i }}</td>
                        <td class="mono">{{ v }}</td>
                        <td class="mono">{{ row_weights[i] }}</td>
                        <td class="mono">{{ row_ranges[i] }}</td>
                    </tr>
                </table>
            </VExpansionPanelText>
        </VExpansionPanel>
    </VExpansionPanels>
</template>

<style scoped lang="scss">
table:not(.debug) {
    border-collapse: collapse;
    thead {
        border-bottom: 1px solid;
    }
    tr > th {
        border-right: 1px solid;
        text-align: right;
    }
}

tr.rolled {
    @apply bg-slate-500;
}
</style>
