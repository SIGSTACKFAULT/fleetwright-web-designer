// needs to be below vuetify
import "@fortawesome/fontawesome-free/css/all.css";
import "@mdi/font/css/materialdesignicons.css";
import { createApp } from "vue";
import VueSortable from "vue3-sortablejs";
import VuetifyNotifier from "vuetify-notifier";

// start loading
import "./globals";
import router from "./plugins/router";
import { vuetify } from "./plugins/vuetify";

import App from "@/App.vue";

import "@/styles/main.scss";
import "@/styles/print.scss";

const app = createApp(App);
app.use(VueSortable);
app.use(vuetify);
app.use(router);
app.use(VuetifyNotifier, {
    default: {
        defaultColor: "surface",
        closeIcon: "mdi mdi-close",
        toastProps: {
            location: "top center",
        },
    },
});

app.mount("#app");
