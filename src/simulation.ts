import { combat_total } from "./combat_stations";
import { eval_jump_range, eval_volume } from "./expressions";
import type { ModuleInterface } from "./modules/modules";
import { Ship } from "./ship/Ship";
import type { ModuleIterator } from "./types/interfaces";
import { unwrap_buff_choice } from "./types/interfaces";
import type { BuffModuleProto, FTLModuleProto, TankModuleProto } from "./types/prototypes";

export type ModuleSimulationState = {
    enabled?: boolean;
};

/**
 * evaluate the given expression with mathjs, or multiply a number by the volume.
 * if expr is "" returns 0.
 * @returns
 */
export function evaluate_expression(
    expr: string | number | undefined,
    volume: number,
    extras?: Record<string, number>,
): number {
    if (expr == undefined) {
        return 0;
    }
    if (expr == "") {
        return 0;
    }
    if (typeof expr == "string") {
        return eval_volume(expr, volume, extras);
    }
    if (typeof expr == "number") {
        return expr * volume;
    }
    throw TypeError();
}

export function _get_scaling_value(module: ModuleInterface<BuffModuleProto>): number {
    const scaling_function = module.prototype.scaling_function;
    switch (scaling_function) {
        case 1:
            return 1;
        default:
        case "volume":
            if ("component" in module.context) {
                return module.context.component.volume;
            } else {
                return 1;
            }
        case "crew":
            if ("combat_station" in module.context) {
                const total = combat_total(module.context.combat_station.crew);
                return total;
            } else {
                console.warn(
                    module.prototype,
                    "uses scaling_function=crew, but is outside a combat station",
                );
                return 0;
            }
    }
}

/**
 * calculate the adder and multiplier of the given buff
 * @param ship ModuleIterator<DehydratedComponent | DehydratedCombatStation>
 * @param stat which buff to calculate
 * @returns `{ add: number; multiply: number }`
 */
export function simulate_buff_internal(
    ship: ModuleIterator,
    stat: string,
    options?: any,
): { add: number; multiply: number } {
    let add = 0;
    let multiply = 1;
    const non_stacking = new Set<string>();
    const buff = Array.from(ship.modules_filtered<BuffModuleProto>("buff", options));
    const buff_choice = Array.from(
        unwrap_buff_choice(ship.modules_filtered<any>("buff_choice", options)),
    );
    for (const module of buff.concat(buff_choice)) {
        if (module.prototype.stacking == false) {
            if ("component" in module.context) {
                if (non_stacking.has(module.context.component.id)) {
                    // already applied
                    continue;
                } else {
                    // not yet applied
                    non_stacking.add(module.context.component.id);
                }
            } else {
                console.warn(
                    "[simulate_buff_internal] prototype has stacking=false but context 'id' member missing",
                    module,
                );
            }
        }
        const scaling_value = _get_scaling_value(module);
        add += (module.prototype.add?.[stat] ?? 0) * scaling_value;
        multiply *= Math.pow(module.prototype.multiply?.[stat] ?? 1, scaling_value);
    }
    return { add, multiply };
}

/**
 * do all the buff math
 * @param ship ModuleIterator<DehydratedComponent | DehydratedCombatStation>
 * @param stat which buff to calculate
 * @param base initial value
 */
export function simulate_buff(
    ship: ModuleIterator,
    stat: string,
    base: number = 1,
    options?: any,
): number {
    const { add, multiply } = simulate_buff_internal(ship, stat, options);
    const result = (base + add) * multiply;
    console.debug("simulate_buff:", base, `+${add} *${multiply}`, "=>", result);
    return result;
}

export function simulate_tank(ship: Ship, resource: string, include_disabled = false) {
    let sum = 0;
    for (const module of ship.modules_filtered<TankModuleProto>("tank")) {
        if ("component" in module.context) {
            if (module.prototype.stores == resource) {
                if (module.state.enabled || include_disabled) {
                    sum += 10 * module.context.component.volume;
                }
            }
        }
    }
    return sum;
}

export function simulate_ecm(ship: Ship): number {
    const volume = ship.total_volume();
    const heat = 0;
    const raw = simulate_buff(ship, "raw_ecm", 0);
    const ecm = (1800 * raw) / (volume + heat);
    return simulate_buff(ship, "ecm", ecm);
}

export function simulate_resource_delta(ship: ModuleIterator, resource: string): number {
    let delta = 0;
    for (const module of ship.modules()) {
        if (module.state.enabled) {
            if ("component" in module.context) {
                const volume = module.context.component.volume;
                if (module.prototype.type == "converter") {
                    let light_level = 0;

                    if (module.prototype.wants_variables?.includes("light_level")) {
                        if ("_light_level" in ship && typeof ship._light_level === "number") {
                            light_level = ship._light_level;
                        } else {
                            console.warn(
                                "Module requested light_level but no light_level was specified",
                            );
                            light_level = 100;
                        }
                    }

                    delta -= evaluate_expression(module.prototype.input?.[resource], volume, {
                        light_level,
                    });
                    delta += evaluate_expression(module.prototype.output?.[resource], volume, {
                        light_level,
                    });
                }
                if (module.prototype.type == "engine") {
                    delta -= evaluate_expression(module.prototype.fuel?.[resource], volume);
                }
            }
        }
    }
    return delta;
}

/**
 * Calculate ship's jump range. Also returns the mass fraction to avoid code repetition
 * @param ship Ship (for getting the numerator of the mass fraction of)
 * @param module ModuleWithContext (for getting the denominator of mass fraction)
 */
export function simulate_jump_range(
    ship: Ship,
    module: ModuleInterface<FTLModuleProto>,
): {
    jump_range: number;
    u: number;
} {
    if ("component" in module.context) {
        const mass = module.context.component.mass();
        if (mass == 0) {
            // avoid div/0
            return { jump_range: 0, u: 0 };
        }
        const mass_fraction = mass / ship.total_mass();
        const expression = module.prototype.jump_range ?? "0"; // in case `jump_range` is missing
        const result = eval_jump_range(expression, mass_fraction);
        return {
            jump_range: result,
            u: mass_fraction,
        };
    } else {
        console.error("FTLModule not on a component!", module);
        throw TypeError("Expected component in module.context");
    }
}
