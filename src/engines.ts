//! engine-related math
//! logically part of simulation.ts
import { data } from "./globals";
import type { ModuleInterface } from "./modules/modules";
import {
    evaluate_expression,
    simulate_buff,
    simulate_resource_delta,
    simulate_tank,
} from "./simulation";
import type { EngineModuleProto } from "./types/prototypes";
import { sum } from "./utils";
import { Ship } from "@/ship/Ship";

function engine_burn_time(ship: Ship, engine: ModuleInterface<EngineModuleProto>): number {
    const fuels = Object.keys(engine.prototype.fuel);
    const burn_times = fuels.map((fuel) => {
        return (
            simulate_tank(ship, fuel) /
            -(
                simulate_resource_delta(ship, fuel) -
                (engine.state.enabled == true // if our engine is disabled, calculate it's drain and add it
                    ? 0
                    : evaluate_expression(
                          engine.prototype.fuel?.[fuel],
                          "component" in engine.context ? engine.context.component.volume : NaN,
                      ))
            )
        );
    });
    const burn_time = Math.min(...burn_times);
    if (burn_time == -Infinity) {
        return 0;
    } else {
        return burn_time;
    }
}

/**
 * precalculate all the things for each engine
 * used by UI to generate thrst/accel breakdowns
 */
export function _engines_info(ship: Ship): {
    enabled: boolean;
    volume: number;
    thrust: number;
    module: ModuleInterface;
    burn_time: () => number;
    name: () => string;
}[] {
    const result = [];
    for (const engine of ship.modules_filtered<EngineModuleProto>("engine")) {
        if ("component" in engine.context) {
            const component = engine.context.component;
            const volume = engine.context.component.volume;
            const thrust = engine.prototype.thrust * volume;
            result.push({
                enabled: engine.state.enabled ?? false,
                volume,
                thrust,
                name: () => data.components?.[component.id].name ?? component.id,
                burn_time: () => engine_burn_time(ship, engine),
                module: engine,
            });
        }
    }
    return result;
}

/**
 * ship's total thrust for enabled modules
 * @param ship
 */
export function thrust(ship: Ship, options: { all?: boolean } = {}): number {
    return simulate_buff(
        ship,
        "thrust",
        sum(
            _engines_info(ship)
                .filter((e) => e.enabled || options.all)
                .map((e) => e.thrust),
        ),
    );
}

/**
 * ship's total acceleration for enabled modules
 * @param ship
 */
export function acceleration(ship: Ship, options: { all?: boolean } = {}): number {
    const total_mass = ship.total_mass();
    if (total_mass == 0) return 0; // prevent div/0
    const accel = thrust(ship, options) / total_mass;
    if (accel > 1) {
        return simulate_buff(ship, "acceleration", accel);
    } else {
        return accel;
    }
}

/**
 * approx deltav for a single engine
 * @param cached_total_mass if you're iterating over a bunch of engines you can calculate the total mass once and pass it here
 */
export function _engine_deltav(
    ship: Ship,
    engine: ModuleInterface<EngineModuleProto>,
    cached_total_mass?: number,
) {
    const total_mass = cached_total_mass ?? ship.total_mass();
    const burn_time = engine_burn_time(ship, engine);
    const thrust =
        engine.prototype.thrust *
        ("component" in engine.context ? engine.context.component.volume : 0);
    return {
        burn_time,
        thrust,
        deltav: (burn_time * thrust) / total_mass,
    };
}

/**
 * approximate ship's total delta-v for enabled modules
 * @param ship
 */
export function deltav(ship: Ship): number {
    console.groupCollapsed("[Ship.deltav()]");
    const total_mass = ship.total_mass();
    const estimates = Array.from(ship.modules_filtered<EngineModuleProto>("engine"))
        .filter((module) => module.state.enabled)
        .map((module) => _engine_deltav(ship, module, total_mass));
    console.log("estimates:", estimates);
    console.groupEnd();
    return sum(estimates.map((x) => x.deltav));
}
