import { eval_station_predicate } from "./expressions";
import type { DehydratedCombatStation, DehydratedStationCrew } from "./types/dehydrated";
import type { CombatStationProto } from "@/types/prototypes";
import { sum } from "@/utils";

/**
 * the total crew of the given crew object
 */
export function combat_total(crew: Partial<DehydratedStationCrew>): number {
    return sum(Object.values(crew));
}

/**
 * calculate the level of the given combat station, with the given prototype and morale.
 * @param station Crew Assignments
 * @param prototype CombatStationProto
 * @param worstcase_crew for calculating the crew_fraction
 * @param morale
 * #TODO cache?
 */
export function combat_level(
    station: DehydratedCombatStation,
    prototype: CombatStationProto,
    worstcase_crew: number,
    morale: number,
): number {
    let n = 0;
    const total = combat_total(station.crew);
    const crew: Required<DehydratedStationCrew> = {
        biological: station.crew.biological ?? 0,
        construct: station.crew.construct ?? 0,
        infomorph: station.crew.infomorph ?? 0,
        mechanical: station.crew.mechanical ?? 0,
        brainwashed: station.crew.brainwashed ?? 0,
        any: station.crew.any ?? 0,
    };
    // const scope = {
    //     crew: crew,
    //     crew_fraction: total / worstcase_crew,
    //     morale: morale,
    //     total: total,
    // };
    for (const level of prototype.levels) {
        const passed = eval_station_predicate(
            level.requirements,
            crew,
            total / worstcase_crew,
            morale,
            total,
        );
        if (passed) {
            n++;
            continue;
        } else {
            break;
        }
    }
    return n;
}
