import { computed, reactive } from "vue";
import { parse } from "yaml";

import type {
    CombatStationProto,
    ComponentProto,
    EducationProto,
    GovernmentProto,
    MissilePartProto,
    PolicyProto,
    ResourceProto,
    WorksProto,
} from "./types/prototypes";
import type { CrewType } from "./types/prototypes";

export type Data = {
    resources?: Record<string, ResourceProto>;
    components?: {
        [id: string]: ComponentProto;
    };
    combat_stations?: {
        [id: string]: CombatStationProto;
    };
    missiles?: {
        guidance: Record<string, MissilePartProto>;
        payload: Record<string, MissilePartProto>;
        propulsion: Record<string, MissilePartProto>;
    };
};

let fetch: (u: string) => Promise<Response>;
if (typeof window == "undefined") {
    // we're in Node
    // cwd is vtt/server
    fetch = (url: string) => {
        const fs = require("fs");
        return new Promise<Response>((resolve, reject) => {
            fs.readFile("../../static" + url, "utf8", (error: any, data: any) => {
                if (error) {
                    reject(error);
                } else {
                    resolve({
                        ok: true,
                        text: () => Promise.resolve(data),
                    } as any);
                }
            });
        });
    };
} else {
    fetch = (url: string) => {
        return window.fetch("./" + url);
    };
}

// round to this many decimal places in assorted places
// to avoid having like 0.30000000000000004
export const FLOATING_POINT_ROUNDING = -5;

export const data = reactive<Data>({});
export const samples = reactive<Array<any>>([]);
export const government = reactive<{
    governments?: Record<string, GovernmentProto>;
    policies?: Record<string, Record<string, PolicyProto>>;
    education?: Record<string, EducationProto>;
    colonies?: Record<string, { industry: string; tax: string }>;
    public_works?: Record<string, WorksProto>;
}>({});

export const data_promise = new Promise<Data>((resolve, reject) => {
    fetch("/junctspace.yaml").then((response: Response) => {
        if (response.ok) {
            response.text().then((txt) => {
                const parsed = parse(txt);
                Object.assign(data, parsed);
                resolve(parsed);
            });
        } else {
            reject();
        }
    });
});

fetch("/samples.yaml").then((response: Response) => {
    if (response.ok) {
        response.text().then((txt) => {
            Object.assign(samples, parse(txt));
        });
    }
});

fetch("/government.yaml").then((response: Response) => {
    if (response.ok) {
        response.text().then((txt) => {
            Object.assign(government, parse(txt));
        });
    }
});

export const type_cache = computed<Record<string, Record<string, ComponentProto>>>(() => {
    if (Object.keys(data).length == 0) {
        return {};
    }
    const cache: { [type: string]: { [id: string]: ComponentProto } } = {
        // force these to be at the top
        engine: {},
        tank: {},
        powerplant: {},
        weapon_direct: {},
        weapon_guided: {},
        ftl_drive: {},
        command: {},
        living_quarter: {},
        store: {},
        recreational: {},
        ewar: {},
        utility: {},
    };
    for (const id in data.components) {
        const component = data.components[id];
        if (cache[component?.type] == undefined) {
            cache[component?.type] = {};
        }
        cache[component?.type][id] = component;
    }
    return cache;
});

export const CREW_LABELS: Map<CrewType, string> = new Map([
    ["biological", "Biological"],
    ["construct", "Construct"],
    ["infomorph", "Infomorph"],
    ["mechanical", "Mechanical"],
    ["brainwashed", "Brainwashed"],
    ["any", "Don't Care"],
]);

export const GROUP_NAMES: Record<string, string> = {
    engine: "Engines",
    tank: "Fuel Tanks",
    powerplant: "Powerplants",
    weapon_direct: "Weapons - Direct Fire",
    weapon_guided: "Weapons - Guided",
    ftl_drive: "FTL",
    command: "Command",
    living_quarter: "Quarters",
    store: "Storage",
    recreational: "Recreation",
    ewar: "Sensors & EWAR",
    utility: "Utility",
};
