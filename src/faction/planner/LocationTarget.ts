// #TODO move this back a directory or two because it'll proably be used for other stuff
// only used in jsdoc
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { Faction } from "@/faction/Faction";
import type { ColonyType } from "@/faction/infrastructure";

export type LocationTarget = {
    system: number; // index into faction's systems
    slot: number | ColonyType;
};
