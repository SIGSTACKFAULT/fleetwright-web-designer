import { CIGroup, type ContainsInfrastructure, MIGroup } from "../infrastructure";
import { type DehydratedInfraOrder, InfraOrder } from "./BuildOrder";
import { type DehydratedYardJob, YardJob } from "./YardJob";
import { Faction } from "@/faction/Faction";
import type { Consumer } from "@/types/interfaces";
import { sum } from "@/utils";

export type DehydratedTurnPlan = {
    infra: DehydratedInfraOrder[];
    shipbuilding: DehydratedYardJob[];
};

export class TurnPlan implements Consumer, ContainsInfrastructure {
    infra: InfraOrder[];
    shipbuilding: YardJob[];

    constructor(faction: Faction, { infra, shipbuilding }: Partial<DehydratedTurnPlan>) {
        this.infra = infra?.map((o) => new InfraOrder(faction, o)) ?? [];
        this.shipbuilding = shipbuilding?.map((j) => new YardJob(faction, j)) ?? [];
    }

    industry_consumed(): number {
        return (
            sum(this.infra.map((j) => j.industry_consumed())) +
            sum(this.shipbuilding.map((j) => j.industry_consumed()))
        );
    }

    money_consumed(): number {
        return (
            sum(this.infra.map((j) => j.money_consumed())) +
            sum(this.shipbuilding.map((j) => j.money_consumed()))
        );
    }

    ci(): CIGroup {
        return CIGroup.combine(...this.infra.map((s) => s.ci()));
    }
    mi(): MIGroup {
        return MIGroup.combine(...this.infra.map((s) => s.mi()));
    }

    toJSON(): DehydratedTurnPlan {
        return {
            infra: this.infra.map((o) => o.toJSON()),
            shipbuilding: this.shipbuilding.map((s) => s.toJSON()),
        };
    }
}
