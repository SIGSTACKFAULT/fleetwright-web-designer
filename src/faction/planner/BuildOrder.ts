import { v1 as uuidv1 } from "uuid";

import type { LocationTarget } from "./LocationTarget";
import { Faction } from "@/faction/Faction";
import {
    CIGroup,
    type ColonyType,
    type ContainsInfrastructure,
    MIGroup,
} from "@/faction/infrastructure";
import type { Consumer } from "@/types/interfaces";

export type DehydratedInfraOrder =
    | {
          type: "ci";
          location: LocationTarget;
          amount: number;
          spirit: boolean;
      }
    | {
          type: "mi";
          location: LocationTarget;
          facilities: Record<string, number>;
      };

export class InfraOrder implements Consumer, ContainsInfrastructure {
    _uuid = uuidv1(); // so dragndrop can keep track of which is which
    faction: Faction;
    type: "ci" | "mi";
    location: LocationTarget;
    _ci_amount: number = 1;
    _mi: MIGroup;
    spirit: boolean = false; // temp logic until i implement real systems

    constructor(faction: Faction, order: Partial<DehydratedInfraOrder>) {
        this.faction = faction;
        this.type = order.type ?? "ci";
        if ("amount" in order) {
            this._ci_amount = order.amount ?? 1;
        }
        this.location = order.location ?? {
            system: 0,
            slot: 0,
        };
        if ("spirit" in order) {
            this.spirit = order.spirit ?? false;
        }
        if ("facilities" in order) {
            this._mi = new MIGroup(order.facilities);
        } else {
            this._mi = new MIGroup();
        }
    }

    ci(): CIGroup {
        if (this.type == "ci") {
            return CIGroup.single(this.location.slot as ColonyType, this._ci_amount);
        } else {
            return new CIGroup();
        }
    }

    mi(): MIGroup {
        if (this.type == "mi") {
            return this._mi;
        } else {
            return new MIGroup();
        }
    }

    /**
     * The full word "Civillian" or "Military"
     */
    type_name(): string {
        return {
            ci: "Civillian",
            mi: "Military",
        }[this.type];
    }

    colony_type(): string {
        if (typeof this.location.slot === "string") return this.location.slot;
        else throw new Error("not implemented");
    }

    /**
     * wrapper around {@link Faction.location_name}
     */
    location_name(): string {
        return this.faction.location_name(this.location);
    }

    industry_consumed(): number {
        if (this.type == "mi") {
            return Math.ceil(this.mi().total() / 20);
        } else {
            let multiplier;
            if (this.spirit) {
                multiplier = 25;
            } else {
                multiplier = 50;
            }
            return this._ci_amount * multiplier;
        }
    }

    money_consumed(): number {
        if (this.type == "mi") {
            return Math.ceil(this.mi().total() * 0.025);
        }
        return Math.ceil((this._ci_amount * 25) / (this.spirit ? 2 : 1));
    }

    toJSON(): DehydratedInfraOrder {
        if (this.type == "ci") {
            return {
                type: "ci",
                amount: this._ci_amount,
                location: this.location,
                spirit: this.spirit,
            };
        }
        if (this.type == "mi") {
            return {
                type: "mi",
                location: this.location,
                facilities: this._mi.toJSON(),
            };
        }
        throw Error("unknown type", this.type);
    }
}
