import { v1 as uuidv1 } from "uuid";

import type { LocationTarget } from "./LocationTarget";
import type { Faction } from "@/faction/Faction";
import { Ship } from "@/ship/Ship";
import type { DehydratedShip } from "@/types/dehydrated";
import type { Consumer } from "@/types/interfaces";

export type DehydratedYardJob = {
    count: number;
    location: LocationTarget;
    ship: DehydratedShip;
};

export class YardJob implements Consumer {
    _uuid = uuidv1(); // so dragndrop can keep track of which is which
    count: number;
    location: LocationTarget;
    ship?: Ship;

    constructor(faction: Faction, { count, location, ship }: Partial<DehydratedYardJob>) {
        this.count = count ?? 1;
        this.location = location ?? { system: 0, slot: 0 };
        this.ship = ship != undefined ? new Ship(ship) : undefined;
    }

    volume(): number {
        const ship_vol = this.ship?.total_volume() ?? 0;
        return ship_vol * this.count;
    }

    yards_per_ship(): number {
        const ship_vol = this.ship?.total_volume() ?? 0;
        return Math.max(ship_vol / 300, 1);
    }

    /**
     * Number of yards this job needs
     */
    yards(): number {
        return Math.ceil(this.yards_per_ship() * this.count);
    }

    /**
     * Number of turns this job will take
     */
    turns(): number {
        const ship_vol = this.ship?.total_volume() ?? 0;
        return Math.max(Math.ceil(ship_vol / 100), 1);
    }

    industry_consumed(): number {
        return Math.ceil(0.001 * this.volume());
    }

    money_consumed(): number {
        return 0;
    }

    toJSON(): DehydratedYardJob {
        return {
            count: this.count,
            location: this.location,
            ship: this.ship?.toJSON() ?? new Ship({ name: "<NOT SELECTED>" }),
        };
    }
}
