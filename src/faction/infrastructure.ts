import { sum } from "@/utils";

export const COLONY_TYPES = [
    "sun",
    "void",
    "asteroid",
    "rocky",
    "terraformed",
    "gas_giant",
] as const;

export const FACILITY_TYPES = [
    "shipyard",
    "crew_academy",
    "supply_factory",
    "supply_depot",
    "boot_camp",
    "army_base",
    "anti_orbit",
    "point_defense",
] as const;

export type FacilityType = (typeof FACILITY_TYPES)[number];
export type ColonyType = (typeof COLONY_TYPES)[number];

export class Group<TKey extends string> extends Map<TKey, number> {
    /**
     * Total CI/MI in this group
     */
    total(): number {
        return sum(this.values());
    }

    constructor(from?: Record<TKey, number> | Map<TKey, number>) {
        if (from == undefined) {
            super();
            return;
        }
        if (from instanceof Map) {
            super(from);
            return;
        }
        super(Object.entries(from) as [TKey, number][]);
    }

    get(key: TKey, fallback?: number): number {
        return super.get(key) || fallback || 0;
    }

    toJSON(): Record<TKey, number> {
        return Object.fromEntries(this.entries()) as Record<TKey, number>;
    }
}

/**
 * Represents a bunch of CI, indexed by colony type
 */
export class CIGroup extends Group<ColonyType> {
    /**
     * Generate a CIGroup with only `which` set to `amount`
     * @param which
     * @param amount
     */
    static single(which: ColonyType, amount: number): CIGroup {
        const temp = new CIGroup();
        temp.set(which, amount);
        return temp;
    }
    toJSON(): Record<ColonyType, number> {
        // drop keys which aren't in COLONY_TYPES
        return Object.fromEntries(
            COLONY_TYPES.map((type) => [type, this.get(type) || undefined]),
        ) as Record<ColonyType, number>;
    }
    static combine(...others: CIGroup[]): CIGroup {
        // tried to do this on Group using generics but couldn't figure it out
        const result = new CIGroup();
        for (const other of others) {
            for (const k of other.keys()) {
                result.set(k, result.get(k, 0) + other.get(k, 0));
            }
        }
        return result;
    }
}

/**
 * Represents a bunch of military facilities, indexed by facility type
 */
export class MIGroup extends Group<FacilityType> {
    toJSON(): Record<FacilityType, number> {
        // drop keys which aren't in FACILITY_TYPES
        return Object.fromEntries(
            FACILITY_TYPES.map((type) => [type, this.get(type) || undefined]),
        ) as Record<FacilityType, number>;
    }
    static combine(...others: MIGroup[]): MIGroup {
        const result = new MIGroup();
        for (const other of others) {
            for (const k of other.keys()) {
                result.set(k, result.get(k, 0) + other.get(k, 0));
            }
        }
        return result;
    }
}

/**
 * anything which contains infrastructure
 * planets, systems, factions, etc.
 */
export interface ContainsInfrastructure {
    ci(): CIGroup;
    mi(): MIGroup;
}
