export const IDEOLOGY_VALUES = [-2, -1, +1, +2] as const;
export const IDEOLOGY_KEYS = [
    "supportiveness",
    "inclusivity",
    "egalitarianism",
    "progressivism",
] as const;
export type IdeologyValue = (typeof IDEOLOGY_VALUES)[number];
export type IdeologyKey = (typeof IDEOLOGY_KEYS)[number];
export type Ideology = Record<IdeologyKey, IdeologyValue>;

export type IdeologyScope = {
    S: number;
    I: number;
    E: number;
    P: number;
    hi: number;
    lo: number;
};

export function* ideology_generator(): Generator<Ideology> {
    for (const supportiveness of IDEOLOGY_VALUES) {
        for (const inclusivity of IDEOLOGY_VALUES) {
            for (const egalitarianism of IDEOLOGY_VALUES) {
                for (const progressivism of IDEOLOGY_VALUES) {
                    yield { supportiveness, inclusivity, egalitarianism, progressivism };
                }
            }
        }
    }
}

/**
 * returns the scope object for government and policy requirements
 * @deprecated
 */
export function requirements_scope(x: Ideology): IdeologyScope {
    return {
        S: x.supportiveness,
        I: x.inclusivity,
        E: x.egalitarianism,
        P: x.progressivism,
        hi: Math.max(...Object.values(x)),
        lo: Math.min(...Object.values(x)),
    };
}
