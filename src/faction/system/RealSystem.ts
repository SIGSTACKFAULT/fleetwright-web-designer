import * as math from "mathjs";

import type { ContainsInfrastructure } from "../infrastructure";
import { CIGroup, MIGroup } from "@/faction/infrastructure";
import { LazySystem } from "@/faction/system/LazySystem";
import { type DehydratedSlot, Slot } from "@/faction/system/Slot";
import { government } from "@/globals";
import { die, sum } from "@/utils";

export const SYSTEM_SLOTS = 11;
export const SPECIAL_SLOT_INDEXES = {
    STAR: 0,
    VOID: 10,
};

export const STAR_TYPES = [
    "Red-Orange Dwarf",
    "Yellow Dwarf",
    "Blue Giant",
    "White Dwarf",
    "Black Hole",
] as const;
export const SLOT_TEMPS = ["Hot", "Temperate", "Cold"] as const;
export type StarType = (typeof STAR_TYPES)[number];
export type SlotTemp = (typeof SLOT_TEMPS)[number];

export type DehydratedRealSystem = {
    name: string;
    star: StarType;
    location?: string;
    slots: DehydratedSlot[];
};

export class RealSystem implements ContainsInfrastructure {
    name: string = "";
    star: StarType = "Red-Orange Dwarf";
    location: string = "";
    slots: Slot[] = [];

    constructor({ name, star, location, slots }: Partial<DehydratedRealSystem>) {
        this.name = name ?? "";
        this.star = star ?? "Red-Orange Dwarf";
        this.location = location ?? "";
        this.slots = (slots ?? []).map((ds) => new Slot(ds));
    }

    static random_star(): StarType {
        const roll = die(100);
        if (roll <= 8) return "White Dwarf";
        if (roll <= 60) return "Red-Orange Dwarf";
        if (roll <= 89) return "Yellow Dwarf";
        if (roll <= 99) return "Blue Giant";
        return "Black Hole";
    }

    /**
     * generates a random system according to the Sector Generation rules
     */
    static random(): RealSystem {
        const temp = new RealSystem({});
        temp.star = RealSystem.random_star();
        temp.slots = [];
        function generate_slot(i: number): Slot {
            switch (i) {
                case SPECIAL_SLOT_INDEXES.STAR:
                    return new Slot({ slot: "Star" });
                case SPECIAL_SLOT_INDEXES.VOID:
                    return new Slot({ slot: "Void" });
                default: {
                    return Slot.random();
                }
            }
        }
        for (let i = 0; i < SYSTEM_SLOTS; i++) {
            temp.slots.push(generate_slot(i));
        }
        return temp;
    }

    /**
     * mostly an internal helper method.
     * you should generally use {@link RealSystem#slot_capacity}; index {@link SPECIAL_SLOT_INDEXES.STAR} is the star.
     */
    static star_capacity(star: StarType) {
        switch (star) {
            case "White Dwarf":
                return 0;
            case "Red-Orange Dwarf":
                return 1600;
            case "Yellow Dwarf":
                return 3200;
            case "Blue Giant":
                return 6400;
            case "Black Hole":
                return 0;
            default:
                return NaN;
        }
    }

    static slot_temps(star: StarType): SlotTemp[] {
        function unabbreviate(abbr: string): SlotTemp[] {
            if (abbr.length != SYSTEM_SLOTS) {
                throw new TypeError(
                    `unabbreviate(): wrong length ${abbr.length}, expected ${SYSTEM_SLOTS}`,
                );
            }
            return Array.from(abbr).map<SlotTemp>((c) => {
                switch (c) {
                    case "h":
                        return "Hot";
                    case "t":
                        return "Temperate";
                    case "c":
                        return "Cold";
                }
                throw new TypeError(`unabbreviate(): don't know what to do with ${c}`);
            });
        }
        switch (star) {
            case "White Dwarf":
                return unabbreviate("htccccccccc");
            case "Red-Orange Dwarf":
                return unabbreviate("hhttccccccc");
            case "Yellow Dwarf":
                return unabbreviate("hhhhttccccc");
            case "Blue Giant":
                return unabbreviate("hhhhhhttccc");
            case "Black Hole":
                return unabbreviate("ccccccccccc");
            default:
                throw new TypeError(`unknown star type ${star}`);
        }
    }

    /**
     * capacity for the slot at the given index.
     * Generally used in an iteration over Colony.slots. <br>
     * {@link SPECIAL_SLOT_INDEXES.STAR} is the star; {@link SPECIAL_SLOT_INDEXES.VOID} is the void colony.
     * @param index
     */
    slot_capacity(index: number): number {
        const slot = this.slots[index];
        switch (index) {
            case SPECIAL_SLOT_INDEXES.STAR:
                return RealSystem.star_capacity(this.star);
            case SPECIAL_SLOT_INDEXES.VOID:
                return 5000;
            default:
                switch (this.slots[index].slot) {
                    case "Asteroid Belt":
                        return 250 * index;
                    case "Rocky":
                    case "Terraformed":
                        return ((slot.gravity ?? NaN) + (slot.moons ?? NaN)) * 100;
                    case "Gas Giant":
                        return (slot.moons ?? NaN) * 100;
                    case "Empty":
                        return 0;
                    default:
                        return NaN;
                }
        }
    }

    ci(): CIGroup {
        return CIGroup.combine(...this.slots.map((s) => s.ci()));
    }

    mi(): MIGroup {
        return MIGroup.combine(...this.slots.map((s) => s.mi()));
    }

    *colonies(): Generator<Slot> {
        for (const slot of this.slots) {
            yield slot;
        }
    }

    /**
     * Industry capacity of this system
     * (does not include Faction buffs)
     */
    industry(): number {
        if (government.colonies == undefined) {
            // not loaded yet
            return NaN;
        }
        let total = 0;
        for (const colony of this.colonies()) {
            const expression = government.colonies?.[colony.old_colony_type()]?.industry;
            if (expression == undefined) {
                console.warn("no indy multiplier for colony type", colony.old_colony_type());
                total += colony._ci;
            } else {
                total += math.evaluate(expression, { ci: colony._ci });
            }
        }
        return total;
    }

    /**
     * tax income from this system
     * (does not include Faction buffs)
     */
    tax(): number {
        if (government.colonies == undefined) {
            // not loaded yet
            return NaN;
        }
        let total = 0;
        for (const colony of this.colonies()) {
            const expression = government.colonies?.[colony.old_colony_type()]?.tax;
            if (expression == undefined) {
                console.warn("no tax multiplier for colony type", colony.old_colony_type());
                total += colony._ci;
            } else {
                total += math.evaluate(expression, { ci: colony._ci });
            }
        }
        return total;
    }

    /**
     * number of terraformed planets
     */
    terraformed() {
        return sum(this.slots.map((s) => (s.slot == "Terraformed" ? 1 : 0)));
    }

    /**
     * loses information!
     */
    into_LazySystem(): LazySystem {
        return new LazySystem({
            name: this.name,
            ci: this.ci().toJSON(),
            mi: this.mi().toJSON(),
        });
    }

    toJSON(): DehydratedRealSystem {
        return {
            name: this.name,
            star: this.star,
            location: this.location || undefined,
            slots: this.slots.map((s) => s.toJSON()),
        };
    }
}
