<script setup lang="ts">
import { computed } from "vue";
import { watch } from "vue";

import { RealSystem, type StarType } from "./RealSystem";
import { SPECIAL_SLOT_INDEXES, STAR_TYPES } from "./RealSystem";
import { SLOT_TYPES, Slot, type SlotType } from "./Slot";
import { FACILITY_TYPES } from "@/faction/infrastructure";

import NumberEditor from "@/components/NumberEditor.vue";
import NumberInput from "@/components/NumberInput.vue";
import CommonDialog from "@/components/dialogs/CommonDialog.vue";

const props = withDefaults(
    defineProps<{
        system: RealSystem; // 'slot' is reserved
        index: number;
        item: Slot;
        readonly?: boolean;
        edit_mi?: boolean;
    }>(),
    { readonly: false, edit_mi: false },
);

const emit = defineEmits<{
    (e: "update:star", v: StarType): void;
    (e: "update:type", v: SlotType): void;
    (e: "update:gravity", v: number): void;
    (e: "update:moons", v: number): void;
    (e: "update:ci", v: number): void;
}>();

// option to pass it the slot type in case vue hasn't gotten around to updating props yet
function shuffle_gravity_moons(slot?: SlotType) {
    const { gravity, moons } = Slot.random_gravity_moons(slot ?? props.item.slot);
    emit("update:gravity", gravity);
    emit("update:moons", moons);
}

watch(
    // randomize gravity and moons when changing slot type
    () => props.item.slot,
    (after, before) => {
        if (
            (before == "Rocky" && after == "Terraformed") ||
            (before == "Terraformed" && after == "Rocky")
        ) {
            // don't shuffle when changing between Rocky <=> Terraformed
            return;
        }
        shuffle_gravity_moons(after);
    },
);

/**
 * this info isn't shown if {@link props.item.has_moons()} == false
 * so we don't have to check
 */
const min_gravity = computed<number>(() => {
    if (props.item.slot == "Gas Giant") {
        return 2;
    } else {
        return 1;
    }
});
const max_gravity = computed<number>(() => {
    if (props.item.slot == "Gas Giant") {
        return 16;
    } else {
        return 6;
    }
});
const max_moons = computed<number>(() => {
    if (props.item.slot == "Gas Giant") {
        return props.item.gravity * 6;
    } else {
        return 4;
    }
});
const capacity = computed(() => props.system.slot_capacity(props.index));
const temperature = computed(() => RealSystem.slot_temps(props.system.star)[props.index]);
const temperature_color = computed(
    () => ({ Hot: "#fb0", Temperate: "#0f0", Cold: "#58f" }[temperature.value]),
);
</script>

<template>
    <div class="flex" data-cy="SlotCard">
        <div class="self-stretch w-2" :style="{ 'background-color': temperature_color }">
            <v-tooltip activator="parent" location="left">{{ temperature }}</v-tooltip>
        </div>
        <div
            class="p-2 grow border border-white border-solid border-l-0 rounded-e-xl flex flex-col container"
        >
            <div class="flex items-center flex-wrap header" :data-slot-type="item.slot">
                <h3 class="slot-header flex items-center">
                    <p class="mono mr-2 text-grey">[{{ index }}]</p>
                    <v-btn
                        v-if="!readonly && index != SPECIAL_SLOT_INDEXES.VOID"
                        data-cy="edit-slot-button"
                        icon
                        size="small"
                        variant="plain"
                        density="compact"
                        class="mr-2"
                    >
                        <v-icon> mdi mdi-pencil</v-icon>
                        <CommonDialog close_button>
                            <template #title>Edit Slot</template>
                            <v-select
                                v-if="index == SPECIAL_SLOT_INDEXES.STAR"
                                :items="STAR_TYPES"
                                label="Star Type"
                                :model-value="system.star"
                                @update:model-value="$emit('update:star', $event)"
                            />
                            <v-select
                                v-else
                                :items="SLOT_TYPES.slice(1, 6)"
                                data-cy="select-slot-type"
                                label="Slot Type"
                                :model-value="item.slot"
                                @update:model-value="$emit('update:type', $event)"
                            />
                            <div class="flex">
                                <div class="grid gap-2 grid-cols-2 grow">
                                    <template v-if="item.has_moons()">
                                        <v-text-field
                                            type="number"
                                            label="Gravity"
                                            :min="min_gravity"
                                            :max="max_gravity"
                                            :hint="`${min_gravity}-${max_gravity}`"
                                            density="compact"
                                            :model-value="item.gravity"
                                            @update:model-value="
                                                $emit('update:gravity', parseInt($event))
                                            "
                                        />
                                        <v-text-field
                                            type="number"
                                            label="Moons"
                                            min="0"
                                            :max="max_moons"
                                            :hint="`0-${max_moons}`"
                                            density="compact"
                                            :model-value="item.moons"
                                            @update:model-value="
                                                $emit('update:moons', parseInt($event))
                                            "
                                        />
                                    </template>
                                    <template v-else>
                                        <!-- fake textfields just so there's no layout jump -->
                                        <v-text-field
                                            disabled
                                            label="Gravity - N/A"
                                            density="compact"
                                        />
                                        <v-text-field
                                            disabled
                                            label="Moons - N/A"
                                            density="compact"
                                        />
                                    </template>
                                </div>
                                <v-btn
                                    icon="mdi mdi-dice-multiple"
                                    :disabled="!item.has_moons()"
                                    variant="plain"
                                    @click="shuffle_gravity_moons()"
                                />
                            </div>
                            <p>
                                CI Capacity:
                                <b class="mono">{{
                                    system.slot_capacity(index).toLocaleString()
                                }}</b>
                            </p>
                        </CommonDialog>
                    </v-btn>
                    {{ item.slot }}
                    <span v-if="item.slot == 'Star'" class="font-normal">
                        &nbsp;&mdash; {{ system.star }}
                    </span>
                </h3>
                <div class="pl-2 h-6 slot-secondary">
                    <span v-if="item.slot == 'Rocky' && temperature == 'Temperate'">
                        Terraformable
                    </span>
                    <em v-if="item.has_moons()" class="text-grey text-right">
                        <span v-if="item.slot == 'Rocky' && temperature == 'Temperate'">
                            &bull;
                        </span>
                        {{ item.gravity }} gravity &bull; {{ item.moons }} moon{{
                            item.moons == 1 ? "" : "s"
                        }}
                    </em>
                </div>
            </div>
            <div v-if="capacity > 0" class="grid grid-cols-2 gap-2 mt-2 items-center">
                <p>
                    <NumberInput
                        :n="item._ci"
                        :label="`CI / ${capacity}`"
                        :min="0"
                        :max="capacity"
                        :class="{}"
                        :bigstep="30"
                        cy_name="edit_ci"
                        @update:n="$emit('update:ci', $event)"
                    />
                </p>
                <p v-if="!readonly && edit_mi">
                    MI: <b class="mono">{{ item._mi.total() }}</b>
                    <v-btn
                        size="small"
                        append-icon="mdi mdi-chevron-right"
                        class="float-right"
                        variant="text"
                    >
                        Edit
                        <CommonDialog close_button>
                            <template #title>
                                {{ system.name }}
                                <v-icon>mdi mdi-chevron-right</v-icon>
                                Slot {{ index }}
                                <v-icon>mdi mdi-chevron-right</v-icon>
                                Edit MI
                            </template>
                            <table>
                                <tr v-for="(type, i) of FACILITY_TYPES" :key="i">
                                    <td class="capitalize font-bold">
                                        {{ type.replace("_", " ") }}
                                    </td>
                                    <td>
                                        <NumberEditor
                                            :n="item._mi.get(type)"
                                            @update:n="item._mi.set(type, $event)"
                                        />
                                    </td>
                                </tr>
                            </table>
                        </CommonDialog>
                    </v-btn>
                </p>
            </div>
            <div v-else class="flex items-center justify-center grow">
                <em class="text-grey">&mdash; Can't be colonized &mdash;</em>
            </div>
        </div>
    </div>
</template>

<style scoped lang="scss">
.container {
    container-type: inline-size;
    .header {
        .slot-header {
            align-self: flex-start;
        }
        .slot-secondary {
            margin-left: auto;
        }
        @container (width < 32ch) {
            // grid-template-columns: repeat(2, 1fr);
            flex-direction: column;
        }
    }
}
</style>
