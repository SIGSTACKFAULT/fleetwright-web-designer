import { QualityMap } from "../QualityMap";
import { CIGroup, type ContainsInfrastructure, MIGroup } from "@/faction/infrastructure";
import { die, random_choice } from "@/utils";

export const SLOT_TYPES = [
    "Star",
    "Empty",
    "Asteroid Belt",
    "Rocky",
    "Terraformed",
    "Gas Giant",
    "Void",
] as const;
export type SlotType = (typeof SLOT_TYPES)[number];

export type DehydratedSlot = {
    slot: SlotType;
    name?: string;
    gravity?: number;
    moons?: number;
    ci?: number;
    mi?: Record<string, number>;
    trained_crew?: Record<string, number>;
    ground_forces?: Record<string, number>;
};

export class Slot implements ContainsInfrastructure {
    slot: SlotType = "Empty";
    name: string = "";
    gravity: number; // planets & gas giants
    moons: number; // planets & gas giants
    _ci: number = 0;
    _mi: MIGroup = new MIGroup();
    trained_crew: QualityMap;
    ground_forces: QualityMap;

    constructor({
        slot,
        name,
        gravity,
        moons,
        ci,
        mi,
        trained_crew,
        ground_forces,
    }: Partial<DehydratedSlot>) {
        this.slot = slot ?? "Empty";
        this.name = name ?? "";
        this.gravity = gravity ?? 0;
        this.moons = moons ?? 0;
        this._ci = ci ?? 0;
        this._mi = new MIGroup(mi);
        this.trained_crew = new QualityMap(trained_crew);
        this.ground_forces = new QualityMap(ground_forces);
    }

    static random_gravity_moons(slot: SlotType) {
        let moons = 0;
        let gravity = 0;
        if (slot == "Rocky" || slot == "Terraformed") {
            gravity = die(6);
            moons = Math.max(die(6) - 2, 0);
        }
        if (slot == "Gas Giant") {
            gravity = die(8) + die(8);
            for (let i = 0; i < gravity; i++) {
                moons += die(6);
            }
        }
        return { moons, gravity };
    }

    /**
     * generates random slot according to sector generation rules
     */
    static random(): Slot {
        const slot: SlotType = random_choice(["Empty", "Asteroid Belt", "Rocky", "Gas Giant"]);
        const { gravity, moons } = this.random_gravity_moons(slot);
        return new Slot({ slot, gravity, moons });
    }

    /**
     * checks if {@link slot} is Rocky, Terraformed, or Gas Giant
     */
    has_moons() {
        return this.slot == "Rocky" || this.slot == "Terraformed" || this.slot == "Gas Giant";
    }

    ci(): CIGroup {
        switch (this.slot) {
            case "Empty":
                return new CIGroup();
            case "Asteroid Belt":
                return CIGroup.single("asteroid", this._ci);
            case "Gas Giant":
                return CIGroup.single("gas_giant", this._ci);
            case "Rocky":
                return CIGroup.single("rocky", this._ci);
            case "Terraformed":
                return CIGroup.single("terraformed", this._ci);
            case "Star":
                return CIGroup.single("sun", this._ci);
            case "Void":
                return CIGroup.single("void", this._ci);
        }
    }

    mi(): MIGroup {
        return this._mi;
    }

    toJSON(): DehydratedSlot {
        return {
            slot: this.slot,
            gravity: this.has_moons() ? this.gravity || undefined : undefined,
            moons: this.has_moons() ? this.moons || undefined : undefined,
            ci: this._ci || undefined,
            mi: this._mi.total() == 0 ? undefined : this._mi.toJSON(),
            trained_crew: this.trained_crew.total() > 0 ? this.trained_crew.toJSON() : undefined,
            ground_forces: this.ground_forces.total() > 0 ? this.ground_forces.toJSON() : undefined,
        };
    }

    /**
     * @deprecated
     */
    old_colony_type() {
        switch (this.slot) {
            case "Empty":
                return "";
            case "Asteroid Belt":
                return "asteroid";
            case "Gas Giant":
                return "gas_giant";
            case "Rocky":
                return "rocky";
            case "Terraformed":
                return "terraformed";
            case "Star":
                return "sun";
            case "Void":
                return "void";
        }
    }
}
