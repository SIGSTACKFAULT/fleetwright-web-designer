import { eval_ci_mi } from "@/expressions";
import {
    CIGroup,
    COLONY_TYPES,
    type ContainsInfrastructure,
    MIGroup,
} from "@/faction/infrastructure";
import { government } from "@/globals";

/**
 * doesn't track planets, only amount of infra per colony type
 */
export type DehydratedLazySystem = {
    name?: string;
    ci: Record<string, number>;
    mi?: Record<string, number>;
};

/**
 * Common system methods
 * @deprecated
 */
export abstract class System<TDehydrated = unknown> implements ContainsInfrastructure {
    abstract name: string;
    /**
     * Generates a list of colonies
     */
    abstract colonies(): Generator<{ type: string; ci: number }>;
    abstract ci(): CIGroup;
    abstract mi(): MIGroup;

    /**
     * tax income from this system
     * (does not include Faction buffs)
     */
    tax(): number {
        let total = 0;
        for (const colony of this.colonies()) {
            const expression = government.colonies?.[colony.type].tax;
            if (expression == undefined) {
                console.warn("no tax multiplier for colony type", colony.type);
                total += colony.ci;
            } else {
                total += eval_ci_mi(expression, colony.ci, 0);
            }
        }
        return total;
    }

    /**
     * Industry capacity of this system
     * (does not include Faction buffs)
     */
    industry(): number {
        let total = 0;
        for (const colony of this.colonies()) {
            const expression = government.colonies?.[colony.type].industry;
            if (expression == undefined) {
                console.warn("no indy multiplier for colony type", colony.type);
                total += colony.ci;
            } else {
                total += eval_ci_mi(expression, colony.ci, 0);
            }
        }
        return total;
    }

    /**
     * Serialize.
     */
    abstract toJSON(): TDehydrated;
}

/**
 * System which doesn't track planets or orbits or any of that; only amount of infra per colony type
 * @deprecated
 */
export class LazySystem extends System<DehydratedLazySystem> {
    name: string;
    _ci: CIGroup;
    _mi: MIGroup;
    __lazy = true;

    constructor({ name, ci, mi }: Partial<DehydratedLazySystem>) {
        super();
        this.name = name ?? "";
        this._ci = new CIGroup(ci);
        this._mi = new MIGroup(mi);
    }

    ci(): CIGroup {
        return this._ci;
    }

    mi(): MIGroup {
        return this._mi;
    }

    *colonies() {
        for (const type of COLONY_TYPES) {
            const ci = this._ci.get(type) ?? 0;
            if (ci > 0) {
                yield { type, ci };
            }
        }
    }

    toJSON() {
        return {
            name: this.name || undefined,
            ci: this._ci.toJSON(),
            mi: this._mi.size > 0 ? this._mi.toJSON() : undefined,
        };
    }
}
