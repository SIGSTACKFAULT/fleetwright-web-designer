<script setup lang="ts">
import copy from "copy-to-clipboard";
import { sum } from "mathjs";
import { computed, ref } from "vue";

import type { TurnPlan } from "./planner/TurnPlan";
import { type Faction, INITIAL_FACTION_POINTS } from "@/faction/Faction";
import { RANDOM_EVENTS } from "@/faction/events/RandomEvent";
import { FACILITY_TYPES } from "@/faction/infrastructure";
import useDebug from "@/useDebug";
import { force_sign } from "@/utils";

import NumberChip from "@/components/NumberChip.vue";
import CommonDialog from "@/components/dialogs/CommonDialog.vue";

const { DEBUG } = useDebug();

const props = defineProps<{
    faction: Faction;
    plan?: TurnPlan;
    short?: boolean;
    parts?: {
        fp: boolean;
    };
}>();

function roll_events_temp() {
    const all = [...RANDOM_EVENTS.POSITIVE, ...RANDOM_EVENTS.NEGATIVE];
    const longest_name = all.reduce<number>(
        (prev, current) => Math.max(current.id.length, prev),
        0,
    );
    let result = ">Event".padEnd(longest_name) + "  | Chance% | Happens?\n";
    result += "".padEnd(longest_name, "-") + "--|---------|----------\n";
    for (const event of all) {
        const roll = Math.random();
        const happens = roll < event.odds(props.faction);
        result += happens ? (event.type == "positive" ? "+" : "-") : " ";
        result += event.id.padEnd(longest_name);
        result += " | ";
        result += event.format_odds(props.faction).padEnd(7);
        result += " | ";
        result += happens ? "[x]" : "[ ]";
        result += "\n";
    }
    return result;
}

const roll_result = ref();

const warn_unique_system_names = computed(() => {
    const names = props.faction.systems.map((s) => s.name).map((n) => n.toLowerCase());
    const set = new Set(names);
    return set.size != names.length;
});

const financial_table = computed<{ item: string; amount: number }[]>(() => {
    // #TODO maybe move this to Faction.ts
    const result: { item: string; amount: number }[] = [];
    result.push({
        item: "Tax",
        amount: props.faction.tax_income(),
    });
    result.push({
        item: "Administration",
        amount: -props.faction.expenses_parts().administration(),
    });
    result.push({
        item: "MI Upkeep",
        amount: -props.faction.expenses_parts().mi_upkeep(),
    });
    if (props.plan) {
        result.push({
            item: "Infrastructure Construction",
            amount: -sum(props.plan.infra.map((i) => i.money_consumed())),
        });
    }
    return result.map((row) => ({ ...row, amount: Math.floor(row.amount) }));
});
const partial_table_sums = computed<{ pos: number; neg: number }>(() => {
    return {
        pos: sum(financial_table.value.filter((x) => x.amount > 0).map((x) => x.amount)),
        neg: sum(financial_table.value.filter((x) => x.amount < 0).map((x) => x.amount)),
    };
});
const bottom_line = computed(() =>
    Math.floor(partial_table_sums.value.pos + partial_table_sums.value.neg),
);

const warn_starter = computed(() => props.faction.is_valid() && !props.faction.is_starter());
</script>

<template>
    <div class="flex big flex-wrap gap-2">
        <NumberChip v-if="parts?.fp ?? true" name="FP" data-cy="chip-FP">
            {{ INITIAL_FACTION_POINTS - faction.fp_spent().total() }}/{{ INITIAL_FACTION_POINTS }}
        </NumberChip>
        <NumberChip name="CI" data-cy="chip-CI">
            {{ faction.ci().total()
            }}<template v-if="plan?.ci()">+{{ plan.ci().total() }}</template>
        </NumberChip>
        <NumberChip name="MI" data-cy="chip-MI">
            {{ faction.mi().total()
            }}<template v-if="plan?.mi()">+{{ plan.mi().total() }}</template>
        </NumberChip>
    </div>
    <div class="flex big flex-wrap gap-2">
        <NumberChip v-if="faction.bank > 0" name="Bank" data-cy="bank">
            {{ faction.bank.toLocaleString() }}&curren;
        </NumberChip>
        <NumberChip name="Bottom Line" data-cy="bottom_line">
            {{ force_sign(Math.round(bottom_line)) }}&curren;/mo
        </NumberChip>
    </div>
    <div class="flex flex-wrap gap-2">
        <NumberChip name="Happiness"> {{ faction.happiness() }} </NumberChip>
        <NumberChip name="Legitimacy"> {{ faction.legitimacy() }} </NumberChip>
        <NumberChip name="Industry">
            <template #label> <v-icon size="small">mdi mdi-bolt</v-icon>Industry </template>
            <template v-if="plan"> {{ plan.industry_consumed() }}/</template
            >{{ faction.industry_cap() }}
        </NumberChip>
        <NumberChip v-if="DEBUG" name="Colonies">
            {{ Array.from(faction.colonies()).length }}
        </NumberChip>
    </div>
    <v-expansion-panels
        v-if="!(short ?? false)"
        multiple
        variant="accordion"
        style="border-right: 4px solid rgb(var(--v-theme-secondary))"
        class="print:hidden"
    >
        <v-expansion-panel>
            <v-expansion-panel-title>
                <div class="grid grid-cols-4 flex-grow">
                    <span class="col-span-2">Financial</span>
                    <span class="mono text-base">
                        {{ force_sign(Math.round(partial_table_sums.pos)) }}&curren;
                    </span>
                    <span class="mono text-base">
                        {{ force_sign(Math.round(partial_table_sums.neg)) }}&curren;
                    </span>
                </div>
            </v-expansion-panel-title>
            <v-expansion-panel-text>
                <table>
                    <tr v-for="({ item, amount }, i) in financial_table" :key="i">
                        <td>{{ item }}</td>
                        <td class="number">{{ force_sign(Math.round(amount)) }}&curren;</td>
                    </tr>
                    <tr style="border-top: 1px solid">
                        <td>Total</td>
                        <td class="number">{{ force_sign(Math.round(bottom_line)) }}&curren;</td>
                    </tr>
                </table>
            </v-expansion-panel-text>
        </v-expansion-panel>
        <v-expansion-panel>
            <v-expansion-panel-title> Administration </v-expansion-panel-title>
            <v-expansion-panel-text>
                <table>
                    <tr>
                        <td>Admin Units</td>
                        <td class="number">
                            <b class="mono">&nbsp;{{ faction.admin() }}&alpha;</b>
                        </td>
                    </tr>
                    <tr>
                        <td>Base Admin</td>
                        <td class="number">
                            -{{ faction.government_prototype()?.stats.base_admin }}&alpha;
                        </td>
                    </tr>
                    <tr style="border-top: 1px solid">
                        <td>Defecit</td>
                        <td class="number">={{ faction.admin_defecit() }}&alpha;</td>
                    </tr>
                </table>
                <p>
                    Admin Cost:
                    <span class="mono">
                        {{ faction.admin_defecit() }}&alpha; &div;
                        {{ faction.government_prototype()?.stats.admin_efficiency }}&alpha;/&curren;
                        =
                        <b>{{ faction.admin_defecit_cost() }}&curren;/mo</b>
                    </span>
                </p>
            </v-expansion-panel-text>
        </v-expansion-panel>
        <v-expansion-panel>
            <v-expansion-panel-title> Events </v-expansion-panel-title>
            <v-expansion-panel-text>
                <div class="float-right flex flex-col items-stretch gap-2">
                    <v-btn
                        variant="outlined"
                        prepend-icon="mdi mdi-dice-multiple"
                        @click="roll_result = roll_events_temp()"
                    >
                        Roll
                        <CommonDialog close_button>
                            <b class="text-grey">Temporary Dialog</b>
                            <pre class="mono" style="line-height: 1.3">{{ roll_result }}</pre>
                            <template #actions>
                                <v-btn variant="outlined" @click="copy(roll_result)"> Copy </v-btn>
                            </template>
                        </CommonDialog>
                    </v-btn>
                </div>
                <table>
                    <thead>
                        <th class="text-left">Event</th>
                        <th class="text-left">Chance/mo</th>
                    </thead>
                    <tbody style="border-color: yellowgreen">
                        <tr v-for="event in RANDOM_EVENTS.POSITIVE" :key="event.name">
                            <td>{{ event.id }}</td>
                            <td class="mono">{{ event.format_odds(faction) }}</td>
                        </tr>
                    </tbody>
                    <tbody style="border-color: red">
                        <tr v-for="event in RANDOM_EVENTS.NEGATIVE" :key="event.name">
                            <td>{{ event.id }}</td>
                            <td class="mono">{{ event.format_odds(faction) }}</td>
                        </tr>
                    </tbody>
                </table>
            </v-expansion-panel-text>
        </v-expansion-panel>
        <v-expansion-panel v-if="faction.mi().total() + (plan?.mi()?.total() ?? 0) > 0">
            <v-expansion-panel-title>
                <div class="grid grid-cols-2 grow">
                    <span>Military</span>
                </div>
            </v-expansion-panel-title>
            <v-expansion-panel-text>
                <table class="compact">
                    <tr class="text-grey">
                        <th class="text-left">Facility</th>
                        <th>Amount</th>
                        <th v-if="plan" class="pl-2">Change</th>
                    </tr>
                    <tr v-for="(key, i) in FACILITY_TYPES" :key="i">
                        <td class="capitalize">{{ key.replace("_", " ") }}</td>
                        <td class="number">
                            {{ faction.mi().get(key) ?? 0 }}
                        </td>
                        <td v-if="plan" class="number">
                            {{ force_sign(plan.mi().get(key) ?? 0, { zero: "+" }) }}
                        </td>
                    </tr>
                    <tr style="border-top: 1px solid">
                        <th class="text-left">Total</th>
                        <td class="number">
                            {{ faction.mi().total().toLocaleString() }}
                        </td>
                        <td v-if="plan" class="number">
                            {{ force_sign(plan.mi().total()) }}
                        </td>
                    </tr>
                </table>
                <p>Upkeep Cost: <b class="mono">xxx &curren;/mo</b></p>
            </v-expansion-panel-text>
        </v-expansion-panel>
    </v-expansion-panels>
    <div data-cy="overview-warnings">
        <v-card v-if="warn_unique_system_names" color="warning" variant="outlined">
            <v-card-text>
                <v-icon>mdi mdi-alert</v-icon>
                Please pick unique system names
            </v-card-text>
        </v-card>
        <v-card v-if="warn_starter" color="white" variant="outlined">
            <v-card-text>
                <v-icon>mdi mdi-information-outline</v-icon>
                Cannot be used at start of game
            </v-card-text>
        </v-card>
    </div>
</template>

<style scoped lang="scss">
:deep(.v-expansion-panel-title) {
    font-size: 1.5rem;
    .v-expansion-panel-title__icon {
        font-size: 1rem;
    }
}

table {
    td.number {
        @apply font-mono font-bold text-right;
        padding-left: 1ch;
        min-width: 6ch;
    }

    border-collapse: collapse;
    tbody {
        border-left: 4px solid;
        margin-bottom: 10px;
        tr *:first-child {
            padding-left: 5px;
        }
    }
}
</style>
