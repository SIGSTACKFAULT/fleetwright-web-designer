import { sum } from "@/utils";

export const GROUND_FORCE_QUALITIES = [-2, 0, +2];
export const CREW_QUALITIES = [-4, -2, -1, 0, +1, +2, +3];

/**
 * Map of personnel quality to amount.
 * Used for Slot.trained_crew and Slot.ground_forces
 */
export class QualityMap extends Map<number, number> {
    constructor(from?: Record<string, number>) {
        super();
        if (from !== undefined) {
            for (const k in from) {
                const quality = parseInt(k);
                if (isNaN(quality)) {
                    console.warn("[QualityMap] ignoring non-int key", k);
                } else {
                    this.set(quality, from[k]);
                }
            }
        }
    }
    total(): number {
        return sum(this.values());
    }
    toJSON(): Record<string, number> {
        return Object.fromEntries(Array.from(this.entries()).map(([k, v]) => [k.toString(), v]));
    }
}
