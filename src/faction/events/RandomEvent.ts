/* eslint-disable @typescript-eslint/no-unused-vars */
import { round10 } from "expected-round";

import { Faction } from "@/faction/Faction";

export abstract class RandomEvent {
    static readonly id: string;
    static readonly type: "positive" | "negative";
    static odds(faction: Faction): number {
        throw Error("Not Implemented");
    }

    static format_odds(faction: Faction) {
        return `${round10(this.odds(faction) * 100, -3)}%`;
    }
}

// ==================================== POSITIVE

class Windfall extends RandomEvent {
    static readonly id = "Windfall";
    static readonly type = "positive";

    static odds(faction: Faction) {
        const economic_education_level = faction.education.economics ?? 0;
        const one_percent_increments = Math.max(0, economic_education_level - 2);
        return 0.01 * one_percent_increments;
    }
}

class UnexppectedProductivity extends RandomEvent {
    static readonly id = "Unexpected Productivity";
    static readonly type = "positive";

    static odds(faction: Faction) {
        const science_education_level = faction.education.science ?? 0;
        const one_percent_increments = Math.max(0, science_education_level - 2);
        return 0.01 * one_percent_increments;
    }
}

class GoodOptics extends RandomEvent {
    static readonly id = "Good Optics";
    static readonly type = "positive";

    static odds(faction: Faction) {
        const divisor = Math.pow(50, Math.floor(faction.corruption() / 50));
        return 0.05 / divisor;
    }
}

class PatrioticFervor extends RandomEvent {
    static readonly id = "Patriotic Fervor";
    static readonly type = "positive";

    static odds(faction: Faction) {
        const happiness = faction.happiness();
        const chance = happiness * 0.0025;
        // #TODO multiply by 4 during justified war
        return Math.max(0, chance);
    }
}

class ExcessCivilianShipbuilding extends RandomEvent {
    static readonly id = "Excess Civilian Shipbuilding";
    static readonly type = "positive";

    static odds(faction: Faction) {
        const colonies = Array.from(faction.colonies()).length;
        return 0.01 * colonies;
    }
}

class Whistleblower extends RandomEvent {
    static readonly id = "Whistleblower";
    static readonly type = "positive";

    static odds(faction: Faction) {
        const corruption = faction.corruption();
        const below_100 = Math.min(100, corruption);
        const above_100 = Math.max(0, corruption - 100);
        return Math.max(0, 0.002 * (above_100 - below_100));
    }
}

// ==================================== NEGATIVE

class Scandal extends RandomEvent {
    static readonly id = "Scandal";
    static readonly type = "negative";

    static odds(faction: Faction) {
        const base = 0;
        const corruption = faction.corruption();
        return base + 0.1 * corruption;
    }
}

class MilitaryConspiracy extends RandomEvent {
    static readonly id = "Military Conspiracy";
    static readonly type = "negative";

    static odds(faction: Faction) {
        let base = 0.1;
        base -= 0.005 * faction.legitimacy();
        // base -= [defense corruption]
        return base;
    }
}

class EconomicCrash extends RandomEvent {
    static readonly id = "Economic Crash";
    static readonly type = "negative";

    static odds(_faction: Faction) {
        return 0.01;
        // #TODO +0.1% per production corruption
    }
}

class PoliticalRadicalization extends RandomEvent {
    static readonly id = "Political Radicalization";
    static readonly type = "negative";

    static odds(_faction: Faction) {
        return 0.01;
        // #TODO +0.1% per Enforcement, Justice, or Stratification Corruption.
    }
}

class UnresolvedGrievance extends RandomEvent {
    static readonly id = "Unresolved Grievance";
    static readonly type = "negative";

    static odds(_faction: Faction) {
        return 0.01;
        // #TODO +0.5% per Justice Corruption
    }
}

class DiseaseOutbreak extends RandomEvent {
    static readonly id = "Disease Outbreak";
    static readonly type = "negative";

    static odds(faction: Faction) {
        // Base 3%, +1% per Healthcare Corruption; total divided by Healthcare Level
        const base = 0.03;
        const healthcare_level = faction.policy_prototypes().healthcare?.level ?? 0;
        const healthcare_corruption = 0; // #TODO
        return (base + 0.01 * healthcare_corruption) / healthcare_level;
    }
}

class IndustrialDisaster extends RandomEvent {
    static readonly id = "Industrial Disaster";
    static readonly type = "negative";

    static odds(_faction: Faction) {
        const base = 0;
        const production_corruption = 0; // #TODO
        const works_corruption = 0; // #TODO
        return base + 0.1 * (production_corruption + works_corruption);
    }
}

class Terrorism extends RandomEvent {
    static readonly id = "Terrorism";
    static readonly type = "negative";

    static odds(faction: Faction) {
        const negative_happiness = Math.min(0, faction.happiness());
        const dissent = 0; // #TODO
        return (negative_happiness + dissent) * 0.001;
    }
}

// ==================================== EVENTS LIST

export const RANDOM_EVENTS: { POSITIVE: (typeof RandomEvent)[]; NEGATIVE: (typeof RandomEvent)[] } =
    {
        POSITIVE: [
            Windfall,
            UnexppectedProductivity,
            GoodOptics,
            PatrioticFervor,
            Whistleblower,
            ExcessCivilianShipbuilding,
        ],
        NEGATIVE: [
            Scandal,
            EconomicCrash,
            PoliticalRadicalization,
            UnresolvedGrievance,
            DiseaseOutbreak,
            IndustrialDisaster,
            MilitaryConspiracy,
            Terrorism,
        ],
    };
