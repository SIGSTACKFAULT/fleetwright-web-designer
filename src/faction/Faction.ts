import * as math from "mathjs";
import { v4 as uuid_v4 } from "uuid";

import { type DehydratedRealSystem, RealSystem } from "./system/RealSystem";
import { type Ideology, type IdeologyScope, requirements_scope } from "@/faction/ideology";
import { COLONY_TYPES, MIGroup } from "@/faction/infrastructure";
import { CIGroup, type ContainsInfrastructure } from "@/faction/infrastructure";
import type { LocationTarget } from "@/faction/planner/LocationTarget";
import type { TurnPlan } from "@/faction/planner/TurnPlan";
import { type DehydratedLazySystem, LazySystem } from "@/faction/system/LazySystem";
import { government } from "@/globals";
import type { ModuleInterface } from "@/modules/modules";
import { simulate_buff } from "@/simulation";
import type { ModuleIterator } from "@/types/interfaces";
import type { GovernmentProto, ModuleProto, PolicyProto } from "@/types/prototypes";
import { sum } from "@/utils";

export type FactionModifiers = string[];
export type FactionAuthoity = "E" | "A" | "C";
export type FactionPolicies = Record<string, string | undefined>;
export type FactionEducation = Record<string, number>;
export type FactionWorks = string[];

export interface DehydratedFaction {
    name: string;
    turn?: number;
    bank?: number;
    type: string;
    authority?: FactionAuthoity; // optional if gov type has only one choice
    modifiers?: FactionModifiers; // optional if none chosen, or no mods available for current gov type
    ideology: Ideology;
    policies: Partial<FactionPolicies> & {
        education?: FactionEducation;
        public_works?: FactionWorks;
    };
    systems?: (DehydratedRealSystem | DehydratedLazySystem)[];
    terraformed_planets?: number;
}

export const INITIAL_FACTION_POINTS = 18;

const DEFAULT_IDEOLOGY = {
    supportiveness: -2,
    inclusivity: -2,
    egalitarianism: -2,
    progressivism: -2,
} as const;

const DEFAULT_EDUCATION = {
    political: 0,
    economics: 0,
    science: 0,
    culture: 0,
} as const;

function ideology_to_points(axis: number): number {
    if (axis == -2) {
        return 0;
    }
    if (axis == -1) {
        return 1;
    }
    if (axis == +1) {
        return 2;
    }
    if (axis == +2) {
        return 3;
    }
    throw Error(`invalid axis value ${axis}`);
}

export class Faction implements ModuleIterator, ContainsInfrastructure {
    /**
     * just so dragndrop can keep track of which is which
     */
    _sortable_unique = uuid_v4();
    name: string;
    turn: number = 0; // kinda like a version number
    bank: number = 0;
    type: string | null;
    ideology: Ideology;
    authority?: FactionAuthoity;
    modifiers: Map<string, boolean>;
    policies: FactionPolicies;
    education: FactionEducation;
    public_works: FactionWorks;
    systems: (LazySystem | RealSystem)[] = [];
    terraformed_planets?: number;

    constructor({
        name,
        turn,
        bank,
        type,
        ideology,
        authority,
        modifiers,
        policies,
        systems,
    }: Partial<DehydratedFaction>) {
        this.name = name ?? "";
        this.turn = turn ?? 0;
        this.bank = bank ?? 0;
        this.type = type ?? null;
        this.authority = authority;
        this.modifiers = new Map(modifiers?.map((s) => [s, true]) ?? []);
        this.ideology = ideology ?? DEFAULT_IDEOLOGY;
        if (policies !== undefined) {
            const { education, public_works, ...rest } = policies;
            this.policies = rest;
            this.education = education ?? DEFAULT_EDUCATION;
            this.public_works = public_works ?? [];
        } else {
            this.policies = {};
            this.education = DEFAULT_EDUCATION;
            this.public_works = [];
        }
        for (const system of systems ?? []) {
            if ("ci" in system) {
                this.systems.push(new LazySystem(system));
            } else if ("slots" in system) {
                this.systems.push(new RealSystem(system));
            } else {
                // #TODO actually use all those types i wrote
                console.warn("don't know what to do with system", system);
            }
        }
    }

    toJSON(): DehydratedFaction {
        // if (this.type == null) throw Error("no type specified");
        // if (Object.values(this.policies).some((p) => p == null)) {
        //     throw Error("some policies are null");
        // }
        return {
            name: this.name,
            turn: this.turn || undefined,
            type: this.type || "",
            bank: this.bank || undefined,
            authority: this.authority,
            ideology: this.ideology,
            modifiers: Array.from(this.modifiers.entries())
                .filter(([, v]) => v == true)
                .map(([k]) => k),
            terraformed_planets: this.terraformed_planets,
            policies: Object.assign({}, this.policies, {
                education: this.total_education() == 0 ? undefined : this.education,
                public_works: this.public_works.length == 0 ? undefined : this.public_works,
            }),
            systems: this.systems.map((s) => s.toJSON()),
        };
    }

    /**
     * Does this faction actually have the government type and policies selected?
     */
    is_complete(): boolean {
        if (!this.type) {
            return false;
        }
        if (
            Object.keys(government.policies ?? {})
                .map((k) => this.policies[k])
                .includes(undefined)
        ) {
            return false;
        }
        return true;
    }

    /**
     * are all the requirements met for government and policies?
     */
    is_valid(): boolean {
        const gov_proto = this.government_prototype();
        const policy_protos = this.policy_prototypes();
        if (gov_proto == undefined) return false;
        const gov_valid = math.evaluate(
            gov_proto.requirements ?? "true",
            this.requirements_scope(),
        );
        if (!gov_valid) return false;
        for (const policy of Object.keys(government.policies ?? [])) {
            const policy_proto = policy_protos[policy];
            if (policy_proto == undefined) return false;
            const policy_valid = math.evaluate(
                policy_proto.requirements ?? "true",
                this.requirements_scope(),
            );
            if (!policy_valid) return false;
        }
        return true;
    }

    /**
     * could this faction be used at the start of a game?
     */
    is_starter(): boolean {
        if (!this.is_valid()) return false;
        if (this.fp_spent().total() > INITIAL_FACTION_POINTS) return false;
        return true;
    }

    *modules(): Generator<ModuleInterface> {
        // policies
        for (const policy of Object.values(this.policy_prototypes())) {
            if (policy == undefined) {
                continue;
            }
            if (policy.modules == undefined) {
                continue;
            }
            for (const module of policy.modules) {
                yield {
                    context: {
                        faction: this,
                        part: "policy",
                    },
                    prototype: module,
                    state: {},
                };
            }
        }
        // education
        for (const type of Object.keys(government.education ?? {})) {
            const level = this.education[type];
            const level_prototype = government.education?.[type]?.levels?.[level];
            if (level_prototype == undefined) {
                console.debug("could not find prototype for", type, "level", level);
                continue;
            }
            for (const module of level_prototype.modules) {
                yield {
                    context: {
                        faction: this,
                        part: "education",
                    },
                    prototype: module,
                    state: {},
                };
            }
        }
        // public works
        for (const work of this.public_works) {
            const prototype = government.public_works?.[work];
            if (prototype == undefined) {
                console.warn("could not find prototype for", work);
                continue;
            }
            for (const module of prototype.modules ?? []) {
                yield {
                    context: {
                        faction: this,
                        part: "education",
                    },
                    prototype: module,
                    state: {},
                };
            }
        }
    }

    *modules_filtered<TModule extends { type: string }>(
        filter: ModuleProto["type"],
    ): Generator<ModuleInterface<TModule>> {
        for (const module of this.modules()) {
            if (module.prototype.type == filter) {
                yield module as ModuleInterface<TModule>;
            }
        }
    }

    get_single_module<TModule extends { type: string }>(type: ModuleProto["type"]) {
        for (const module of this.modules()) {
            if (module.prototype.type == type) {
                return module as ModuleInterface<TModule>;
            }
        }
        return undefined;
    }

    // ======================================================================== FINANCIAL

    /**
     * Total money which goes in per month
     */
    income(): number {
        return this.tax_income();
    }

    /**
     * Total money which goes out per month
     */
    expenses(): number {
        // call all the methods returned by `expenses_parts` and add them up
        return sum(Object.values(this.expenses_parts()).map((f) => f()));
    }

    /**
     * All the things which cost money just by existing
     * - administration: money spent on admin upkeep past the admin cap
     * - military_upkeep: MI just costs 0.005 each
     * ! does NOT include stuff the player chooses to spend money on (e.g., building)
     */
    expenses_parts() {
        return {
            administration: () => this.admin_defecit_cost(),
            mi_upkeep: () => Math.ceil(this.mi().total() * 0.005),
            navy_upkeep: () => 0, // placeholder
        };
    }

    /**
     * total corruption
     */
    corruption(): number {
        return 0; // #TODO
    }

    ci(): CIGroup {
        return CIGroup.combine(...this.systems.map((s) => s.ci()));
    }

    mi(): MIGroup {
        return MIGroup.combine(...this.systems.map((s) => s.mi()));
    }

    *colonies(): Generator<{
        /* not sure yet */
    }> {
        for (const system of this.systems) {
            yield* system.colonies();
        }
    }

    /**
     * Expression scope for Government/Policy/Works requirements
     */
    requirements_scope(): IdeologyScope {
        return requirements_scope(this.ideology);
    }

    /**
     * Industrial capactiy
     */
    industry_cap(): number {
        const base = sum(this.systems.map((s) => s.industry()));
        const buffed = simulate_buff(this, "industry", base);
        return Math.floor(buffed);
    }

    government_prototype(): GovernmentProto | undefined {
        if (this.type == null) {
            return undefined;
        }
        return government?.governments?.[this.type] ?? undefined;
    }

    policy_prototypes(): Record<string, PolicyProto | undefined> {
        return Object.fromEntries(
            Object.keys(this.policies).map((key) => [
                key,
                government.policies?.[key]?.[this.policies[key] ?? ""],
            ]),
        );
    }

    /**
     * how much FP you've spent on various things
     */
    fp_spent(): { ideology: number; ci: number; terraforming: number; total: () => number } {
        const ideology = sum(Object.values(this.ideology).map(ideology_to_points));
        const ci = Math.ceil(this.ci().total() / 30);
        const terraforming = (this.terraformed_planets ?? 0) * 2;
        return {
            ideology,
            ci,
            terraforming,
            total: () => ideology + ci + terraforming,
        };
    }

    /**
     * INITIAL_FACTION_POINTS - this.fp_spent().total();
     */
    fp_available(): number {
        return INITIAL_FACTION_POINTS - this.fp_spent().total();
    }

    total_education(): number {
        return sum(Object.values(this.education));
    }

    /**
     * all the admin cost expressions, in a string array
     */
    all_admin_costs(): string[] {
        const result = [];
        for (const proto of Object.values(this.policy_prototypes())) {
            if (proto?.admin_cost) {
                result.push(proto.admin_cost);
            }
        }
        return result;
    }

    /**
     * the mathjs expression for the admin cost, simplified, and already parsed into a MathNode
     * #TODO expensive, cache me!
     */
    _admin_cost_expression(): math.MathNode {
        const parts = this.all_admin_costs().map((e) => `(${e})`);
        if (parts.length == 0) {
            return math.parse("0");
        }
        return math.simplify(parts.join("+"), {}, { exactFractions: false });
    }

    /**
     * Total admin units
     */
    admin(): number {
        const expression = this._admin_cost_expression();
        const result = expression.evaluate({ ci: this.ci().total(), mi: this.mi().total() });
        if (typeof result == "number") {
            // round .99 to up, but everything else down
            return Math.floor(result + 0.01);
        } else {
            throw Error("expected number; instead, result was", result);
        }
    }

    /**
     * Currency you need to spend on administration
     */
    admin_defecit_cost(): number {
        const admin = this.admin();
        const government_prototype = this.government_prototype();
        if (government_prototype === undefined) {
            return 0;
        }
        if (admin <= (government_prototype.stats.base_admin ?? 0)) {
            return 0;
        }
        const result = this.admin_defecit() / government_prototype.stats.admin_efficiency;
        return Math.round(result);
    }

    /**
     * tax income subtotals per colony type
     * #TODO cache me
     */
    tax_subtotals() {
        const sum: Record<string, number> = {};
        for (const [type, ci] of this.ci().entries()) {
            const expr = government.colonies?.[type].tax ?? "1*ci";
            sum[type] = math.evaluate(expr, { ci });
        }
        return sum;
    }

    /**
     * Tax income before buff
     * #TODO better method name
     */
    _tax_income_unbuffed(): number {
        return sum(Object.values(this.tax_subtotals()));
    }

    /**
     * total tax income
     */
    tax_income(): number {
        const base = sum(this.systems.map((s) => s.tax()));
        const buffed = simulate_buff(this, "tax", base);
        return buffed;
    }

    /**
     * admin units above your base admin
     */
    admin_defecit(): number {
        const proto = this.government_prototype();
        const admin_cost = this.admin();
        if (proto == undefined) {
            return 0;
        }
        if (admin_cost < proto.stats.base_admin) {
            return 0;
        }
        return admin_cost - proto.stats.base_admin;
    }

    happiness(): number {
        return sum(Object.values(this.happiness_parts()).map((f) => f()));
    }

    /**
     * Things that affect happiness.
     */
    happiness_parts() {
        return {
            // > Each Faction Point spent on Ideology gives -1 Happiness. Every two points spent on Ideology gives -1 Legitimacy.
            ideology: () => -this.fp_spent().ideology,
            colonies: () => Math.round(this.colony_happiness()),
            buff: () => simulate_buff(this, "happiness", 0),
        };
    }

    legitimacy(): number {
        // > Each Faction Point spent on Ideology gives -1 Happiness. Every two points spent on Ideology gives -1 Legitimacy.
        const base = this.government_prototype()?.stats.legitimacy ?? 0;
        const ideology_offset = Math.floor(-this.fp_spent().ideology / 2);
        return simulate_buff(this, "legitimacy", base + ideology_offset);
    }

    colony_happiness(): number {
        const ci_fractions = this.ci_fractions();
        const offset_per_five_percent: Record<string, number | undefined> = {
            sun: -1,
            void: +1,
            terraformed: +1,
        };
        let result = 0;
        for (const type of COLONY_TYPES) {
            const offset = offset_per_five_percent[type] ?? 0;
            const fraction = ci_fractions.get(type) ?? 0;
            const five_percent_increments = fraction * 20;
            result += offset * five_percent_increments;
        }
        return result;
    }

    ci_fractions() {
        const colony_ci = this.ci();
        const total_ci = this.ci().total();
        const result: Map<string, number> = new Map(
            COLONY_TYPES.map((type) => {
                const ci = colony_ci.get(type) ?? 0;
                if (ci == 0) {
                    // avoid 0/0 => NaN
                    return [type, 0];
                }
                return [type, ci / total_ci];
            }),
        );
        return result;
    }

    /**
     * @param plan
     * @returns a NEW faction with the turn applied
     */
    apply_plan(plan: TurnPlan): Faction {
        const result = new Faction(this.toJSON());
        result.turn += 1;
        result.bank += Math.floor(this.income() - plan.money_consumed());
        for (const infra of plan.infra) {
            const system = result.systems[infra.location.system];
            if (system == undefined) {
                console.error(
                    "[Fation.apply_plan()] could not find sysem! continuing.",
                    this,
                    infra.location,
                );
                continue;
            }
            console.log("[apply_plan] updating", system, "with", infra);
            if (infra.type == "ci") {
                if (
                    system instanceof LazySystem &&
                    typeof infra.location.slot == "string" &&
                    (COLONY_TYPES as ReadonlyArray<string>).includes(infra.location.slot as string)
                ) {
                    const current = system._ci.get(infra.location.slot) ?? 0;
                    system._ci.set(infra.location.slot, infra._ci_amount + current);
                } else if ("slots" in system) {
                    // RealSystem
                    const slot = system.slots[infra.location.slot as number];
                    slot._ci += infra._ci_amount;
                } else {
                    console.trace("not implemented");
                }
            } else if (infra.type == "mi") {
                if ("_mi" in system) {
                    system._mi = MIGroup.combine(system._mi, infra._mi);
                } else {
                    // RealSystem
                    system.slots[infra.location.slot as number]._mi = MIGroup.combine(
                        system.slots[infra.location.slot as number]._mi,
                        infra._mi,
                    );
                }
            } else {
                console.trace("not implemented", infra);
            }
        }
        return result;
    }

    /**
     * might be a system, might be a colony, might be undefined
     * @param where
     */
    get_location(where: LocationTarget): ContainsInfrastructure | undefined {
        const system_filter = where.system;
        if (typeof system_filter == "string") {
            // LazySystem
            return this.systems.find((s) => s.name == system_filter);
        } else {
            // RealSystem
            return (this.systems[system_filter] as RealSystem).slots[where.slot as number];
        }
    }

    location_name(where: LocationTarget): string {
        const system = this.systems[where.system] as RealSystem;
        const colony_name = system.slots?.[where.slot as number];
        return `${system.name || "[untitled system]"} / [${where.slot}] ${colony_name.slot}`;
    }
}
