import { HexVector, ShipObject } from "@server/schema";
import type { HexCoordinates, PartialCubeCoordinates } from "honeycomb-grid";
import { Orientation, defineHex } from "honeycomb-grid";
import * as PIXI from "pixi.js";

import type { VTTGrid } from "./VTTGrid";
import { draw_arrow, layers } from "./renderer";
import { machine } from "./state_machine";
import { HEX_SIZE, room, selection } from "./vtt_globals";

/**
 * Client-side tile object
 */
export class Tile extends defineHex({
    dimensions: HEX_SIZE,
    orientation: Orientation.POINTY,
}) {
    hi: boolean = false;
    hover: boolean = false;
    graphics: PIXI.Graphics = new PIXI.Graphics();

    _cached_qrs_text: PIXI.Text | undefined;
    _cached_temp_burn?: PartialCubeCoordinates;

    constructor(coordinates?: HexCoordinates) {
        super(coordinates);
    }

    qrs_text(): PIXI.Text {
        if (this._cached_qrs_text === undefined) {
            this._cached_qrs_text = new PIXI.Text(`${this.q},${this.r},${this.s}`, {
                fill: 0x00ffff,
                fontSize: this.height / 8,
                fontFamily: "'Atkinson Hyperlegible'",
                fontStyle: "italic",
            });
            this._cached_qrs_text.setTransform(
                this.corners[3].x + this.height / 80,
                this.corners[3].y - this.height / 8,
            );
            this._cached_qrs_text.parentLayer = layers.coordinates;
        }
        return this._cached_qrs_text;
    }

    /**
     * @deprecated
     */
    onmouseenter() {
        console.warn("deprecated");
    }

    /**
     * @deprecated
     */
    onmouseleave() {
        console.warn("deprecated");
    }

    /**
     * get the ship on this hex, if any
     * #TODO caching?
     * @deprecated
     */
    get_ship(): ShipObject | undefined {
        console.warn("deprecated");
        return undefined;
    }

    /**
     * @deprecated
     */
    click_handler() {
        console.warn("deprecated");
    }

    border(): PIXI.ColorSource {
        console.warn("deprecated");
        return "#555";
    }

    fill(): PIXI.ColorSource {
        console.warn("deprecated");
        const result = { h: 0, s: 0, v: 15 };
        if (this.hi) {
            result.v += 10;
        }
        if (this.hover) {
            result.v += 10;
        }
        return result;
    }

    /**
     * @deprecated
     */
    draw(): PIXI.Container {
        console.warn("deprecated");
        return this.graphics;
    }

    toString() {
        return `Tile(${this.q}, ${this.r})`;
    }
}

export function draw_ship(grid: VTTGrid, ship: ShipObject) {
    console.warn("deprecated");
    const graphics = new PIXI.Graphics();
    return graphics;
}

/**
 * draw the ship's velocity and burn vectors.
 * assumes we're in ship-space
 */
export function draw_ship_vectors(grid: VTTGrid, ship: ShipObject): PIXI.Container {
    console.warn("deprecated");
    const container = new PIXI.Container();
    return container;
}
