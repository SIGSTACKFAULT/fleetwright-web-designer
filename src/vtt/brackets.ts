import { ShipObject } from "@server/schema";
import { reactive, unref } from "vue";

import type { Tile } from "@/vtt/Tile";
import { pixi, viewport } from "@/vtt/renderer";
import { grid, room } from "@/vtt/vtt_globals";

viewport.addListener("moved", update);

export const cache = reactive<
    Record<
        string,
        { x: number; y: number; inbounds: boolean; selected: boolean; ship: ShipObject; tile: Tile }
    >
>({});

export function update() {
    const g = unref(grid);
    const r = unref(room);

    if (g === undefined) {
        console.warn("[brackets] grid is undefined");
        return;
    }
    if (r === undefined) {
        console.warn("[brackets] room is undefined");
        return;
    }

    clear();
    for (const ship of r?.state.ships.values() ?? []) {
        const hex = g.getHex(ship);
        if (hex === undefined) {
            continue;
        }
        const offset = {
            x: (hex.corners[0].x + hex.corners[1].x) / 2,
            y: (hex.corners[0].y + hex.corners[1].y) / 2,
        };
        offset.x += g.world_size / 2;
        offset.y += g.world_size / 2;
        const point = viewport.toGlobal(offset);
        const inbounds_x = point.x > 0 && point.x < pixi.view.width;
        const inbounds_y = point.y > 0 && point.y < pixi.view.height;
        cache[ship.uuid] = {
            x: Math.round(point.x),
            y: Math.round(point.y),
            inbounds: inbounds_x && inbounds_y,
            selected: false,
            tile: hex,
            ship,
        };
    }
    console.debug(`[brackets] update done; ${Object.keys(cache).length} items`);
}

export function clear() {
    for (const key in cache) {
        delete cache[key];
    }
}
