import type { MySchema, ShipObject } from "@server/schema";
import type { Client, Room } from "colyseus.js";
import { type Ref, ref, shallowRef } from "vue";

import type { VTTGrid } from "@/vtt/VTTGrid";

export const HEX_SIZE = 1000;

export const grid: Ref<VTTGrid | undefined> = shallowRef();
export const client: Ref<Client | undefined> = shallowRef();
export const room: Ref<Room<MySchema> | undefined> = ref();

/**
 * @deprecated use state machine context instead
 */
export const selection: Ref<ShipObject | undefined> = ref();

/**
 * @deprecated
 */
export const room_state: Ref<any> = ref();
