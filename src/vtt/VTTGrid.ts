import type { HexVector } from "@server/schema";
import { Grid, type HexCoordinates, spiral } from "honeycomb-grid";
import * as PIXI from "pixi.js";

import { Tile } from "@/vtt/Tile";
import { draw, layers } from "@/vtt/renderer";
import { machine } from "@/vtt/state_machine";
import { HEX_SIZE, selection } from "@/vtt/vtt_globals";

const font_size = HEX_SIZE * 0.75;
const AXIS_LABELS: [string, { q: number; r: number; s: number }][] = [
    ["+Q", { q: +2, r: -1, s: -1 }],
    ["+R", { q: -1, r: +2, s: -1 }],
    ["+S", { q: -1, r: -1, s: +2 }],
    ["-Q", { q: -2, r: +1, s: +1 }],
    ["-R", { q: +1, r: -2, s: +1 }],
    ["-S", { q: +1, r: +1, s: -2 }],
];

const AXIS_TEXT_CACHE = AXIS_LABELS.map(([label]) => {
    return new PIXI.Text(label, {
        fill: 0xff00ff,
        fontSize: font_size,
        fontWeight: "bold",
        fontFamily: "'Atkinson Hyperlegible'",
    });
});

export class VTTGrid extends Grid<Tile> {
    _previous_hover: Tile | null = null;
    world_size: number;
    radius: number;

    constructor(radius: number) {
        super(Tile, spiral({ radius }));
        this.radius = radius;
        this.world_size = HEX_SIZE * (radius + 1) * 5;
    }

    draw(): PIXI.Container {
        const c = new PIXI.Container();
        if (machine.getSnapshot().matches("connected.select.burn")) {
            this.highlight_radius(selection.value!.velocity_tip(), selection.value!.acceleration());
        } else {
            for (const hex of this) {
                hex.hi = false;
            }
        }
        for (const hex of this) {
            c.addChild(hex.draw());
        }
        return c;
    }

    /**
     * draw Q, R, S outside the grid
     * @param wrapper origin at grid center
     */
    draw_axes(): PIXI.Container {
        const container = new PIXI.Container();
        container.parentLayer = layers.brackets;
        for (const i in AXIS_LABELS) {
            const [, vector] = AXIS_LABELS[i];
            const text = AXIS_TEXT_CACHE[i];
            const hex = this.getHex(vector);
            if (!hex) {
                // console.warn("draw_axes: couldn't get hex!");
                break;
            }
            let x = hex.x;
            let y = hex.y;
            x *= (this.radius + 1.5) / 2;
            y *= (this.radius + 1.5) / 2;
            // x -= font_size / 2; // hard-coded offset to roughly center
            // y -= font_size / 2;
            text.anchor.set(0.5);
            text.x = x;
            text.y = y;
            container.addChild(text);
        }
        return container;
    }

    draw_border(viewport: PIXI.Container) {
        // let box = (viewport.plugins.plugins["clamp"] as Clamp).options;
        // console.log("border", box);
        const line = viewport.addChild(new PIXI.Graphics());
        line.lineStyle(10, 0xff0000).drawRect(0, 0, this.world_size, this.world_size);
    }

    highlight_radius(start: HexCoordinates | HexVector, radius: number) {
        for (const hex of this.traverse(spiral({ radius, start }))) {
            hex.hi = true;
        }
    }

    mousemove_handler(x: number, y: number) {
        const hex = this.pointToHex({ x, y }, { allowOutside: false }) ?? null;
        if (this._previous_hover == hex) {
            return;
        }
        this.mousemove_handler_hex(hex);
        this._previous_hover = hex;
    }

    mousemove_handler_hex(hex: Tile | null) {
        const prev_string = this._previous_hover?.toString() ?? "outside";
        const next_string = hex?.toString() ?? "outside";
        console.debug("[hover]", prev_string, "->", next_string);
        if (hex === null) {
            this._previous_hover?.onmouseleave();
            draw();
        } else {
            this._previous_hover?.onmouseleave();
            this._previous_hover = hex;
            hex.onmouseenter();
            draw();
        }
    }

    click_handler(x: number, y: number) {
        const hex = this.pointToHex({ x, y }, { allowOutside: false }) ?? null;
        if (hex === null) {
            machine.send({ type: "CLICK_OUTSIDE" });
            return;
        }
        hex.click_handler();
        console.log(hex.toString(), "clicked");
    }
}
