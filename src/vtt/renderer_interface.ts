//! Functions that vtt-renderer calls
import { GridObject, MissileObject, ShipObject } from "@server/schema";

import { machine } from "./state_machine";
import { room, selection } from "./vtt_globals";

export function get_everything(): Array<GridObject> | undefined {
    const objs = room.value?.state?.everything();
    if (objs === undefined) return undefined;
    else return Array.from(objs);
}

export function get_ships(): Array<ShipObject> | undefined {
    const objs = room.value?.state?.ships;
    if (objs === undefined) return undefined;
    else return Array.from(objs.values());
}

export function get_missiles(): Array<MissileObject> | undefined {
    const objs = room.value?.state?.missiles;
    if (objs === undefined) return undefined;
    else return Array.from(objs.values());
}

export function update_primary_selection(uuid: string | null) {
    console.info("update_primary_selection", uuid);
    if (uuid == null) {
        // deselect
        machine.send({ type: "CLICK_OUTSIDE" });
    } else {
        const ship = room.value?.state?.ships?.get(uuid);
        if (ship) {
            machine.send({ type: "CLICK_OBJECT", object: ship });
            selection.value = ship;
        } else {
            console.error("update_primary_selection got invalud uuid", uuid);
        }
    }
}

export function update_burn(uuid: string, q: number, r: number) {
    const obj = room.value?.state.get_uuid(uuid);
    if (obj) {
        room.value?.send("change_burn", { uuid, vector: { q, r } });
    } else {
        console.warn("update_burn got invalid uuid", uuid);
    }
}

export function update_jump(uuid: string, q: number, r: number) {
    const ship = room.value?.state?.ships?.get(uuid);
    if (ship) {
        room.value?.send("change_jump", { uuid, vector: { q, r } });
    }
}

export function update_jump_null(uuid: string) {
    const ship = room.value?.state?.ships?.get(uuid);
    if (ship) {
        room.value?.send("change_jump", { uuid, vector: null });
    }
}

export function teleport(uuid: string, q: number, r: number) {
    room.value?.send("teleport", { uuid, pos: { q, r } });
}

/**
 * get the ship with the given uuid
 */
export function get_ship(uuid: string): ShipObject | undefined {
    return room.value?.state?.ships?.get(uuid);
}
