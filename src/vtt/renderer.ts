import * as PIXILayers from "@pixi/layers";
import { type HexCoordinates } from "honeycomb-grid";
import { Viewport } from "pixi-viewport";
import * as PIXI from "pixi.js";
import { triggerRef } from "vue";

import { draw_ship } from "./Tile";
import type { VTTGrid } from "./VTTGrid";
import { HEX_SIZE, grid, room } from "./vtt_globals";

const MOUSE_DEBUG = false;
let wrapper = new PIXI.Container();

export const layers = {
    coordinates: new PIXILayers.Layer(),
    arrows: new PIXILayers.Layer(),
    brackets: new PIXILayers.Layer(),
};
Object.values(layers).forEach((layer, i) => {
    layer.zIndex = 1000 + i;
});

export const pixi: PIXI.Application = new PIXI.Application({
    backgroundAlpha: 0,
    // view: canvas.value,
    antialias: true,
});

// https://github.com/davidfig/pixi-viewport
export const viewport = new Viewport({
    events: pixi.renderer.events,
    worldHeight: 10000,
    worldWidth: 10000,
    // passiveWheel: true,
    allowPreserveDragOutside: true,
    disableOnContextMenu: true,
});
viewport.drag({
    mouseButtons: "right",
    underflow: "center",
});
viewport.wheel();
viewport.pinch();
viewport.clamp({
    direction: "all",
    underflow: "center",
});

pixi.stage = new PIXILayers.Stage();
pixi.stage.addChild(viewport);

function update_world_size(grid: VTTGrid) {
    viewport.clampZoom({
        minWidth: HEX_SIZE * 3,
        maxWidth: grid.world_size * 2,
    });
    viewport.worldHeight = grid.world_size;
    viewport.worldWidth = grid.world_size;
}

viewport.addListener("mousemove", (event) => {
    if (viewport == undefined) {
        console.warn("mousemove handler called but viewport is undefined?!");
        return;
    }
    if (grid.value != undefined) {
        let { x, y } = viewport.toWorld(event.globalX, event.globalY);
        x -= grid.value.world_size / 2;
        y -= grid.value.world_size / 2;
        grid.value.mousemove_handler(x, y);
        triggerRef(grid); // mousemove_handler may have updated grid._previous_hover
    }
});

viewport.addListener("click", (event) => {
    if (viewport == undefined) {
        console.warn("click handler called but viewport is undefined?!");
        return;
    }
    if (grid.value != undefined) {
        let { x, y } = viewport.toWorld(event.globalX, event.globalY);
        x -= grid.value.world_size / 2;
        y -= grid.value.world_size / 2;
        grid.value.click_handler(x, y);
    }
});

export function draw() {
    if (!viewport) {
        console.warn("draw() called but viewport is undefined!");
        return;
    }
    if (!grid.value) {
        console.warn("draw() called but grid is undefined!");
        return;
    }
    if (!room.value) {
        console.warn("draw() called but room is undefined!");
        return;
    }
    // console.debug("draw()");
    viewport.screenWidth = (pixi.view as HTMLCanvasElement).width;
    viewport.screenHeight = (pixi.view as HTMLCanvasElement).height;
    update_world_size(grid.value);
    viewport.removeChildren();

    if (wrapper) {
        wrapper.destroy();
    }
    wrapper = new PIXI.Container();
    // move the origin to the center of the viewport
    wrapper.x = wrapper.y = grid.value.world_size / 2;
    wrapper.sortableChildren = true;
    viewport.addChild(wrapper);

    wrapper.addChild(...Object.values(layers));
    wrapper.addChild(grid.value.draw());

    for (const ship of room.value.state.ships.values()) {
        const ship_graphics = draw_ship(grid.value, ship);
        wrapper.addChild(ship_graphics);
    }
    wrapper.addChild(grid.value.draw_axes());
}

/**
 * returns a graphics object with an arrow drawn. origin at the grid origin.
 */
export function draw_arrow(
    grid: VTTGrid,
    from: HexCoordinates,
    to: HexCoordinates,
    options: {
        color?: PIXI.ColorSource;
        fill_tip?: boolean;
        debug_label?: string;
    } = {},
): PIXI.Graphics {
    const { x: x1, y: y1 } = grid.createHex(from);
    const { x: x2, y: y2 } = grid.createHex(to);
    const theta = Math.atan2(y2 - y1, x2 - x1);
    const len = HEX_SIZE / 4;
    const graphics = new PIXI.Graphics();
    if (x1 == x2 && y1 == y2) {
        // don't bother drawing, but we still have to return *something*
        return graphics;
    }
    // line
    graphics.lineStyle(HEX_SIZE / 20, options.color ?? 0xffffff);
    graphics.moveTo(x1, y1);
    graphics.lineTo(x2, y2);
    // head
    const a1 = {
        x: x2 + Math.cos(theta - Math.PI * (5 / 4)) * len,
        y: y2 + Math.sin(theta - Math.PI * (5 / 4)) * len,
    };
    const a2 = {
        x: x2 + Math.cos(theta + Math.PI * (5 / 4)) * len,
        y: y2 + Math.sin(theta + Math.PI * (5 / 4)) * len,
    };
    if (options.fill_tip ?? false) {
        graphics.beginFill(options.color ?? 0xffffff);
        graphics.drawPolygon(a1, { x: x2, y: y2 }, a2);
    } else {
        graphics.moveTo(a1.x, a1.y);
        graphics.lineTo(x2, y2);
        graphics.lineTo(a2.x, a2.y);
    }
    if (options.debug_label) {
        graphics.addChild(new PIXI.Text(options.debug_label).setTransform(x2, y2));
    }
    graphics.parentLayer = layers.arrows;
    return graphics;
}
