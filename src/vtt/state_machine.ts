import { ShipObject } from "@server/schema";
import { assign, createMachine, interpret } from "xstate";

import * as brackets from "@/vtt/brackets";
import { room } from "@/vtt/vtt_globals";

export type TEvent =
    | { type: "CONNECTING" }
    | { type: "CONNECTING_OK" }
    | { type: "CONNECTING_FAIL" }
    | { type: "CONNECTING_CANCEL" }
    | { type: "DISCONNECT" }
    // mouse
    | { type: "CLICK_OBJECT"; object: ShipObject }
    | { type: "CLICK_OUTSIDE" }
    | { type: "CLICK_HI" }
    | { type: "CLICK_NONHI" }
    | { type: "HOVER_HI" }
    | { type: "HOVER_NONHI" }
    // keyboard
    | { type: "PRESS_Q" }
    | { type: "RELEASE_Q" }
    | { type: "ESCAPE" }
    // buttons
    | { type: "BURN_BUTTON" };

const definition = createMachine(
    {
        schema: {
            events: {} as TEvent,
            context: {} as { selection: ShipObject | null },
        },
        initial: "disconnected",
        predictableActionArguments: true,
        context: {
            selection: null,
        },
        states: {
            disconnected: {
                on: {
                    CONNECTING: "connecting",
                },
            },
            connecting: {
                on: {
                    CONNECTING_OK: "connected",
                    CONNECTING_FAIL: "disconnected",
                    CONNECTING_CANCEL: "disconnected",
                },
            },
            connected: {
                initial: "idle",
                on: {
                    DISCONNECT: { target: "disconnected", actions: ["leave"] },
                },
                states: {
                    idle: {
                        id: "idle",
                        on: {
                            CLICK_OBJECT: { target: "select" },
                        },
                    },
                    select: {
                        id: "select",
                        initial: "idle",
                        entry: ["notify_select", "select"],
                        exit: ["deselect"],
                        on: {
                            CLICK_OUTSIDE: "idle",
                            CLICK_OBJECT: [
                                {
                                    // deselect when clicking current selection again
                                    target: "idle",
                                    cond: (context, event) => context.selection == event.object,
                                },
                                {
                                    // select something else
                                    target: "select",
                                    internal: false,
                                },
                            ],
                            CLICK_NONHI: { target: "idle" },
                        },
                        states: {
                            idle: { on: { PRESS_Q: "burn", BURN_BUTTON: "burn" } },
                            burn: {
                                on: {
                                    RELEASE_Q: "idle",
                                    BURN_BUTTON: "idle",
                                    ESCAPE: "idle",
                                },
                                initial: "invalid",
                                states: {
                                    valid: {
                                        on: {
                                            HOVER_NONHI: "invalid",
                                            CLICK_HI: { target: "#select", internal: true },
                                        },
                                    },
                                    invalid: { on: { HOVER_HI: "valid" } },
                                },
                            },
                        },
                    },
                },
            },
        },
    },
    {
        actions: {
            leave: () => {
                console.debug("[state_machine] leaving room");
                room.value?.leave();
                brackets.update();
            },
            select: assign({
                selection: (context, event) => {
                    if ("object" in event) {
                        return event.object;
                    }
                    throw Error(`expected ${JSON.stringify(event)} to have object property`);
                    // - did you forget to set `internal: true` when moving back to selection.idle?
                },
            }),
            deselect: assign({
                selection: null,
            }),
        },
    },
);

export const machine = interpret(definition);
machine.start();
