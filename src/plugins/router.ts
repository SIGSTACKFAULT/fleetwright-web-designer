import { type RouteRecordRaw, createRouter, createWebHashHistory } from "vue-router";

const MainView = () => import("@/views/MainView.vue");
const NotFound = () => import("@/views/NotFound.vue");
const ShipView = () => import("@/views/ShipView.vue");
const VTTView = () => import("@/views/VTTView.vue");
const FactionEditView = () => import("@/views/FactionEditView.vue");
const TurnPlanner = () => import("@/views/TurnPlanner.vue");
const FactionWizardView = () => import("@/views/FactionWizardView.vue");

export const routes: RouteRecordRaw[] = [
    {
        path: "/",
        name: "home",
        component: MainView,
        alias: ["/ship/", "/fleet/"],
        meta: { back: false, tab: "edit" },
    },
    {
        path: "/vtt/",
        name: "vtt",
        component: VTTView,
        meta: { back: false, tab: "vtt", fullscreen: true, breadcrumbs: ["VTT"] },
        props: true,
    },
    {
        path: "/vtt/server=:endpoint(.*)",
        name: "vtt_permalink",
        component: VTTView,
        meta: { back: false, tab: "vtt", fullscreen: true, breadcrumbs: ["VTT"] },
        props: true,
    },
    {
        path: "/ship/:index",
        props: true,
        name: "ship_editor",
        component: ShipView,
        meta: { back: true, tab: "edit", breadcrumbs: ["Ship Editor"] },
    },
    {
        path: "/faction/wizard",
        name: "faction_wizard",
        component: FactionWizardView,
        meta: { back: false, tab: "edit", breadcrumbs: ["Faction", "Wizard"] },
    },
    {
        path: "/faction/:index/edit",
        name: "faction_edit",
        props: true,
        component: FactionEditView,
        meta: { back: true, tab: "edit", breadcrumbs: ["Faction", "Edit"] },
    },
    {
        path: "/faction/:index/turn-planner",
        name: "faction_plan",
        props: true,
        component: TurnPlanner,
        meta: { back: true, tab: "edit", breadcrumbs: ["Faction", "Turn Planner"] },
    },
    {
        path: "/:pathMatch(.*)*",
        name: "404",
        component: NotFound,
        meta: { back: false, tab: null },
    },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

export default router;
