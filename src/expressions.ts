//! the only place in the project allowed to call mathjs expressions
//! enforces the type-checking on expressions
import { evaluate } from "mathjs";

import type { DehydratedStationCrew } from "./types/dehydrated";
import type { Ideology } from "@/faction/ideology";

/**
 * any expression with `v` or `volume`
 * - component price
 */
export function eval_volume(expr: string, v: number, extras?: Record<string, number>) {
    return evaluate(expr, { ...extras, v, volume: v });
}

/**
 * any function of `ci` or `mi`
 * - tax
 * - industry
 */
export function eval_ci_mi(expr: string, ci: number, mi: number): number {
    return evaluate(expr, { ci, mi });
}

export function eval_jump_range(expr: string, mass_fraction: number) {
    return evaluate(expr, { u: mass_fraction, mass_fraction });
}

/**
 * Goverment and Policy requirements
 */
export function eval_ideology_predicate(expr: string, ideology: Ideology): boolean {
    return evaluate(expr, {
        ...ideology,
        S: ideology.supportiveness,
        I: ideology.inclusivity,
        E: ideology.egalitarianism,
        P: ideology.progressivism,
        hi: Math.max(...Object.values(ideology)),
        lo: Math.min(...Object.values(ideology)),
    });
}

export function eval_station_predicate(
    expr: string,
    crew: DehydratedStationCrew,
    crew_fraction: number,
    morale: number,
    total_crew: number,
): boolean {
    return evaluate(expr, {
        crew,
        crew_fraction,
        morale,
        total: total_crew,
        total_crew,
    });
}
