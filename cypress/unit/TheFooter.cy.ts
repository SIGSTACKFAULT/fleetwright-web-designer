import TheFooter from "../../src/components/TheFooter.vue";

const VERSION_REGEX = /^v\d+\.\d+\.\d+(-\d+-g[0-9a-f]+)?/;

describe("<TheFooter />", () => {
    it("git describe worked", () => {
        cy.mount(TheFooter);
        cy.get("[data-cy=web_git_describe]").contains(VERSION_REGEX);
        cy.get("[data-cy=meta_git_describe]").contains(VERSION_REGEX);
    });
});
