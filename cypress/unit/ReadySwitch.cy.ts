import ReadySwitch from "@/vtt/ReadySwitch.vue";

describe("<ReadySwitch />", () => {
    it("works in multiplayer", () => {
        // cast to any to stop TS from complaining
        // because `room` and `state` are mockups
        const send = cy.stub().as("send");
        const ready = cy.spy().as("ready");
        cy.mount(ReadySwitch as any, {
            props: {
                ready: false,
                connected: true,
                room: { send },
                state: {
                    players: {
                        a: { ready: true },
                        b: { ready: false },
                    },
                },
                "onUpdate:ready": ready,
            },
        });
        cy.contains(".v-input__append", "1/2");
        for (const sel of ["#ready-up", ".v-label", "input[type=checkbox]", ".v-input__append"]) {
            cy.get(sel).click();
            cy.get("@ready").should("have.been.calledOnceWith", true);
            cy.get("@send").should("have.been.calledOnceWith", "ready_up", { state: true });
            cy.then(() => {
                send.resetHistory();
                ready.resetHistory();
            });
        }
    });
    it("works in singleplayer", () => {
        cy.mount(ReadySwitch as any, {
            props: {
                ready: false,
                connected: true,
                room: { send: cy.stub().as("send") },
                state: {
                    players: {
                        a: { ready: true },
                    },
                },
                "onUpdate:ready": cy.spy().as("ready"),
            },
        });
        cy.get("#ready-up").click();
        cy.get("@ready").should("have.been.calledOnceWith", true);
        cy.get("@send").should("have.been.calledOnceWith", "ready_up", { state: true });
    });
    it("doesn't work when disabled", () => {
        cy.mount(ReadySwitch as any, {
            props: {
                ready: false,
                connected: false,
                room: { send: cy.spy().as("send") },
                state: {
                    players: new Map([["a", { ready: true }]]),
                },
                "onUpdate:ready": cy.spy().as("ready"),
            },
        });
        cy.get("#ready-up").click();
        cy.get("@ready").should("not.have.been.called");
    });
});
