import { reactive } from "vue";

import NumberEditor from "@/components/NumberEditor.vue";

describe("<NumberEditor />", () => {
    it("increments", () => {
        // see: https://on.cypress.io/mounting-vue
        const foo = reactive({ n: 1337 });
        cy.mount(NumberEditor, {
            props: {
                n: foo.n,
                "onUpdate:n": cy
                    .spy((m) => {
                        foo.n = m;
                    })
                    .as("n"),
            },
        });
        cy.get("[data-cy=increment]").click();
        cy.get("@n").should("have.been.calledWith", 1338);
    });
    it("decrements", () => {
        // see: https://on.cypress.io/mounting-vue
        const foo = reactive({ n: 1337 });
        cy.mount(NumberEditor, {
            props: {
                n: foo.n,
                "onUpdate:n": cy
                    .spy((m) => {
                        foo.n = m;
                    })
                    .as("n"),
            },
        });
        cy.get("[data-cy=decrement]").click();
        cy.get("@n").should("have.been.calledWith", 1336);
    });
    it("edits", () => {
        // see: https://on.cypress.io/mounting-vue
        const foo = reactive({ n: 1337 });
        cy.mount(NumberEditor, {
            props: {
                n: foo.n,
                "onUpdate:n": cy
                    .spy((m) => {
                        foo.n = m;
                    })
                    .as("n"),
            },
        });
        cy.get("[data-cy=edit]").click();
        cy.get("input").clear().type("123");
        cy.get("[data-cy=confirm]").click();
        cy.get("@n").should("have.been.calledWith", 123);
    });
    it("handles n=undefined", () => {
        cy.mount(NumberEditor, {
            props: {
                n: undefined,
                "onUpdate:n": cy.spy().as("n"),
            },
        });
        cy.get("@n").should("have.been.calledWith", 0);
    });
});
