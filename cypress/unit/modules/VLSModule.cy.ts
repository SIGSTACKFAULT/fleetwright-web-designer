import type { ModuleInterface } from "@/modules/modules";
import { Ship } from "@/ship/Ship";
import { Component } from "@/types/Component";
import { get_single_module } from "@/types/interfaces";
import type { ComponentProto } from "@/types/prototypes";

import WeaponGuidedModule from "@/modules/VLSModule.vue";

const data = Cypress.env("data");

const vls_components = new Map(
    Object.entries<ComponentProto>(data.components).filter(([, proto]) =>
        get_single_module<any>(proto.modules, "vls"),
    ),
);

describe("<WeaponGuidedModule />", () => {
    beforeEach(() => {
        cy.viewport(1000, 500);
    });
    for (const [id, prototype] of vls_components.entries()) {
        const component = new Component({ id: "foo", volume: 1 });
        const ship = new Ship({ components: [component] });
        const module_prototype: any = get_single_module(prototype.modules, "vls");
        const props: ModuleInterface = {
            context: {
                ship,
                component,
            },
            prototype: module_prototype,
            state: {},
            persistence: {},
            readonly: true,
        };
        describe(id, () => {
            it("Initializes props.info", () => {
                cy.mount(WeaponGuidedModule, {
                    props: {
                        ...props,
                        "onUpdate:state": cy.spy().as("state"),
                        "onUpdate:info": cy.spy().as("info"),
                    },
                    global: {
                        provide: {
                            simulation: true,
                        },
                    },
                });
                cy.get("@info").should("have.been.called");
            });
            it("MissileEditor is sent right props", () => {
                cy.mount(WeaponGuidedModule, {
                    props: {
                        ...props,
                        "onUpdate:state": cy.spy().as("state"),
                        "onUpdate:info": cy.spy().as("info"),
                    },
                    global: {
                        provide: {
                            simulation: true,
                        },
                    },
                }).then(({ wrapper }) => {
                    const editor = wrapper.findComponent({ name: "MissilesEditor" });
                    cy.wrap(editor.props()).should("include", {
                        volume: component.volume,
                        size: module_prototype.size,
                    });
                });
            });
            it("fire toggle works", () => {
                cy.mount(WeaponGuidedModule, {
                    props: {
                        ...props,
                        "onUpdate:state": cy.spy().as("state"),
                        "onUpdate:info": cy.spy().as("info"),
                    },
                    global: {
                        provide: {
                            simulation: true,
                        },
                    },
                });
                cy.get("[data-cy=fire] input").should("not.have.attr", "disabled");
                cy.get("[data-cy=fire] input").click();
                cy.get("@state").should("have.been.calledWithMatch", { enabled: true });
                cy.get("[data-cy=fire] input").click();
                cy.get("@state").should("have.been.calledWithMatch", { enabled: false });
            });
        });
    }
});
