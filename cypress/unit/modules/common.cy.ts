import { MODULE_COMPONENTS, type ModuleInterface } from "@/modules/modules";
import { Ship } from "@/ship/Ship";
import { Component } from "@/types/Component";

describe("common module behaviour", () => {
    const component = new Component({ id: "foo", volume: 1 });
    const ship = new Ship({ components: [component] });
    const props: ModuleInterface = {
        context: {
            ship,
            component,
        },
        persistence: {},
        prototype: {},
        state: {},
        readonly: false,
    };
    for (const module of MODULE_COMPONENTS.values()) {
        if (module == null) {
            continue;
        }
        describe(`<${(module as any).__name}>`, () => {
            it("mounts", () => {
                cy.mount(module as any, {
                    props: props,
                });
            });
            for (const simulation of [false, true]) {
                it(`handles simulation=${simulation}`, () => {
                    cy.mount(module as any, {
                        props: props,
                        global: {
                            provide: {
                                simulation,
                            },
                        },
                    });
                });
            }
        });
    }
});
