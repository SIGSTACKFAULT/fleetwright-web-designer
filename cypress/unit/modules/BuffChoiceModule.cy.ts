import type { ModuleInterface } from "@/modules/modules";
import { Ship } from "@/ship/Ship";
import { Component } from "@/types/Component";

import BuffChoiceModule from "@/modules/BuffChoiceModule.vue";

describe("<BuffChoiceModule />", () => {
    const component = new Component({ id: "foo", volume: 1 });
    const ship = new Ship({ components: [component] });
    const props: ModuleInterface = {
        context: {
            ship,
            component,
        },
        prototype: { choices: [{ add: { foo: 1 } }, { add: { bar: 2 } }] },
        state: {},
        persistence: {},
        readonly: true,
    };
    it("initializes buff_choice", () => {
        // see: https://on.cypress.io/mounting-vue
        cy.mount(BuffChoiceModule, {
            props: {
                ...props,
                "onUpdate:info": cy.spy().as("info"),
            },
        });
        cy.get("@info").should("be.calledWithMatch", { buff_choice: 0 });
    });
    it("edits buff_choice", () => {
        // see: https://on.cypress.io/mounting-vue
        cy.mount(BuffChoiceModule, {
            props: {
                ...props,
                "onUpdate:info": cy.spy().as("info"),
            },
        });
        cy.get("[data-cy=buff-choice]").parent().click();
        cy.get("[data-cy=choice-1]").click();
        cy.get("@info").should("be.calledWithMatch", { buff_choice: 1 });
        cy.get("[data-cy=choice-0]").click();
        cy.get("@info").should("be.calledWithMatch", { buff_choice: 0 });
    });
});
