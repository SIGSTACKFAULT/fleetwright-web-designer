import { reactive } from "vue";

import type { ModuleInterface } from "@/modules/modules";
import { Ship } from "@/ship/Ship";
import { Component } from "@/types/Component";
import { get_single_module } from "@/types/interfaces";

import WeaponDirectModule from "@/modules/WeaponDirectModule.vue";

const prototype = get_single_module(Cypress.env("data").components.laser.modules, "weapon_direct");

describe("<WeaponDirectModule />", () => {
    const component = new Component({ id: "foo", volume: 3 });
    const ship = new Ship({ components: [component] });
    const props: ModuleInterface = {
        context: {
            ship,
            component,
        },
        prototype,
        state: {},
        persistence: {},
        readonly: false,
    };
    it("zeroes out info if empty", () => {
        // see: https://on.cypress.io/mounting-vue
        cy.mount(WeaponDirectModule, {
            props: {
                ...props,
                "onUpdate:info": cy.spy().as("info"),
            },
            global: {
                provide: {
                    simulation: true,
                },
            },
        });
        cy.get("@info").should("have.been.calledOnceWithExactly", {
            range: 0,
            power: 0,
            special: 0,
        });
    });
    it("edits range/power/speical", () => {
        const persistence = reactive({});
        cy.mount(WeaponDirectModule, {
            props: {
                ...props,
                persistence,
                "onUpdate:info": cy.spy((event) => Object.assign(persistence, event)).as("info"),
            },
            global: {
                provide: {
                    simulation: true,
                },
            },
        });
        cy.get("[data-cy=upgrades]").click();
        cy.get("[data-cy=range]   [data-cy=increment]").click();
        cy.get("[data-cy=power]   [data-cy=increment]").click();
        cy.get("[data-cy=special] [data-cy=increment]").click();
        cy.get("@info").should("have.been.calledWith", {
            range: 1,
            power: 1,
            special: 1,
        });
        cy.get("[data-cy=range]   [data-cy=increment]").should("be.disabled");
        cy.get("[data-cy=power]   [data-cy=increment]").should("be.disabled");
        cy.get("[data-cy=special] [data-cy=increment]").should("be.disabled");
    });
    it("toggles enabled state", () => {
        // see: https://on.cypress.io/mounting-vue
        cy.mount(WeaponDirectModule, {
            props: {
                ...props,
                "onUpdate:info": cy.spy().as("info"),
                "onUpdate:state": cy.spy().as("state"),
            },
            global: {
                provide: {
                    simulation: true,
                },
            },
        });
        cy.get("[data-cy=enabled]").click();
        cy.get("@state").should("have.been.calledWithMatch", { enabled: true });
        cy.get("[data-cy=enabled]").click();
        cy.get("@state").should("have.been.calledWithMatch", { enabled: false });
    });
});
