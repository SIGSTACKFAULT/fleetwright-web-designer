import MissileEditor from "@/ship/missiles/MissileEditor.vue";

function props(size: "light" | "heavy" = "light") {
    return {
        guidance: "command_guidance",
        payload: "kinetic",
        propulsion: "none",
        count: 2,
        size: size,
        max_missiles: 5,
        "onUpdate:guidance": cy.spy().as("guidance"),
        "onUpdate:payload": cy.spy().as("payload"),
        "onUpdate:propulsion": cy.spy().as("propulsion"),
        "onUpdate:count": cy.spy().as("count"),
        onDelete: cy.spy().as("delete"),
    };
}

describe("<MissileEditor />", () => {
    beforeEach(() => {
        cy.viewport(1000, 500);
    });
    it("delete button works", () => {
        cy.mount(MissileEditor, {
            props: props(),
        });
        cy.get("[data-cy=delete]").click();
        cy.get("@delete").should("have.been.calledOnce");
    });
    it("count dropdown works", () => {
        cy.mount(MissileEditor, {
            props: props("light"),
        });
        cy.get("[data-cy-name=count] input").clear().type("5");
        cy.get("@count").should("have.been.calledWith", 5);
    });
    for (const part of ["guidance", "payload", "propulsion"] as const) {
        it(`${part} dropdown`, () => {
            cy.mount(MissileEditor, {
                props: props("heavy"),
            });
            // #TODO don't hard-coded these
            // expect(component.$.exposed?.single_price.value).to.eq(1);
            // expect(component.$.exposed?.price.value).to.eq(2);
            cy.get(`[data-cy=${part}]`).click();
            cy.get(".v-select__content").within(() => {
                cy.get(".v-list-item").each((el) => {
                    cy.wrap(el).click({ force: true });
                    cy.get(`@${part}`).should("have.been.calledWith", el.attr("data-cy-key"));
                });
            });
        });
    }
    it("prevents you from increasing the missile count above the max", () => {
        cy.mount(MissileEditor, {
            props: { ...props("light") },
        }).then(({ wrapper }) => {
            cy.get("[data-cy=increment]").click();
            cy.get("@count").should("have.been.calledWith", 3);
            cy.then(() => wrapper.setProps({ count: 5 }));
            cy.get("[data-cy=increment]").should("be.disabled");
        });
    });
});
