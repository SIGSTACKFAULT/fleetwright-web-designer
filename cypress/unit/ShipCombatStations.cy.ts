import { CREW_LABELS, data } from "@/globals";
import { Ship } from "@/ship/Ship";
import type { DehydratedCombatStation, DehydratedStationCrew } from "@/types/dehydrated";

import ShipCombatStations from "@/ship/ShipCombatStations.vue";

describe("<ShipCombatStations />", () => {
    beforeEach(() => {
        cy.viewport(1000, 500);
    });
    afterEach(() => {
        cy.get("@console.error").should("not.have.been.called");
        cy.get("@console.warn").should("not.have.been.called");
    });
    it("doesn't make the total NaN after removing all crew", () => {
        cy.mount(ShipCombatStations, {
            props: {
                ship: new Ship({ name: "foo", armour: 10, components: [], combat_stations: {} }),
            },
            global: {
                provide: {
                    simulation: false,
                },
            },
        });
        cy.get(".v-expansion-panel-title").each((el) => {
            cy.wrap(el).click();
        });
        cy.get("[data-cy=total]").should("contain.text", "0");
        cy.get("input[type=number]").first().type("123").clear();
        cy.get("[data-cy=total]").should("contain.text", "0");
    });
    it("loads existing combat station data", () => {
        const combat_stations: Record<string, DehydratedCombatStation> = {};
        for (const id in data.combat_stations) {
            const crew: Partial<DehydratedStationCrew> = {};
            for (const type of CREW_LABELS.keys()) {
                crew[type] = 1337;
            }
            combat_stations[id] = { crew };
        }
        cy.mount(ShipCombatStations, {
            props: {
                ship: new Ship({ name: "foo", armour: 10, components: [], combat_stations }),
            },
            global: {
                provide: {
                    simulation: false,
                },
            },
        });
        cy.get(".v-expansion-panel-title").each((el) => {
            cy.wrap(el).click();
        });
        cy.get("input[type=number]").each((el) => {
            cy.wrap(el).should("have.value", "1337");
        });
    });
    it("'assigned to combat stations' works", () => {
        cy.mount(ShipCombatStations as any, {
            props: {
                ship: new Ship({ name: "foo", armour: 10, components: [], combat_stations: {} }),
                simulation_state: [],
            },
            global: {
                provide: {
                    simulation: false,
                },
            },
        });
        cy.get(".v-expansion-panel-title").each((el) => {
            cy.wrap(el).click();
        });
        let total = 0;
        cy.get("input[type=number]").each((el) => {
            const n = Math.floor(Math.random() * 1000) + 1;
            cy.wrap(el).type(n.toString());
            cy.then(() => {
                total += n;
                cy.get("[data-cy=total]").should("contain.text", total);
            });
        });
    });
});
