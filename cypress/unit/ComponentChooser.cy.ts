import ComponentAddWidget from "@/ship/ComponentChooser.vue";

describe("<ComponentChooser />", () => {
    it("has at least one item per category", () => {
        cy.viewport(500, 1000);
        const select = cy.spy().as("select");
        cy.mount(ComponentAddWidget, {
            props: {
                onSelect: select,
            },
        });
        cy.get(".v-expansion-panel").withinEach((element) => {
            cy.wrap(element).click();
            cy.get(".v-list-item");
        });
    });
    it("emits the @select event", () => {
        cy.viewport(500, 1000);
        const select = cy.spy().as("select");
        cy.mount(ComponentAddWidget, {
            props: {
                onSelect: select,
            },
        });
        cy.get(".v-expansion-panel").withinEach((group) => {
            cy.wrap(group).click();
            cy.get(".v-list-item").each((item) => {
                cy.wrap(item).click();
                cy.get("@select").should("have.been.calledWith", item.attr("test-component"));
            });
        });
    });
});
