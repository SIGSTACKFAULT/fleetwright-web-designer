import { Hex } from "honeycomb-grid";

import { Ship } from "@/ship/Ship";

import VTT from "@/views/VTTView.vue";

describe("<VTTView />", () => {
    before(() => {
        cy.fixture("vtt-server.json").then(async ({ endpoint }) => {
            const url = new URL(endpoint);
            url.protocol = "http";
            url.pathname = "matchmake";
            cy.log("Checking server is up", url);
            const response = await fetch(url);
            expect(response.ok);
        });
    });
    beforeEach(function () {
        cy.viewport(1500, 1000);
        const ship = new Ship({
            name: "test",
            armour: 0,
            components: [{ id: "test", volume: 1 }],
            combat_stations: {},
        });
        // https://github.com/cypress-io/cypress/issues/17048
        // cy.stub(useShips, "useShips").returns([ship]);
        // cy.clearAllLocalStorage();
        cy.window().then((win) => {
            // win.localStorage.clear();
            win.localStorage.setItem("ships", JSON.stringify([ship.toJSON()]));
            cy.log(`localStorage size: ${win.localStorage.length}`);
        });
        cy.wait(500);
        cy.fixture("vtt-server.json").then(async ({ endpoint }) => {
            cy.mount(VTT, {
                global: {
                    provide: {
                        DEBUG: true,
                        DEBUG_LEVEL: 1,
                    },
                    stubs: ["v-snackbar"],
                },
                props: {
                    endpoint,
                },
            }).then(({ component }) => {
                this.component = component;
            });
            cy.contains("button.bg-primary", "connect", { matchCase: false }).should(
                "not.be.disabled",
            );
            cy.contains("button.bg-primary", "connect", { matchCase: false }).click();
            cy.contains("button.bg-primary", "connect", { matchCase: false }).should("be.disabled"); // wait for connection
        });
    });

    it.skip("clicking to select ships works", function () {
        // works in `cy open` but not `cy run`
        const grid = this.component.$.exposed?.grid?.value;
        cy.get("button[value=ships]").click();
        cy.get("[data-cy=ship-list]").within(() => {
            cy.get("button").first().click();
        });
        cy.then(() => {
            // select the ship we just created at 0,0
            grid.click_handler(0, 0);
        });
        cy.get("button[value=selection]").should("have.attr", "aria-selected", "true");
        cy.then(() => {
            // deselect
            grid.click_handler(1000, 1000);
        });
        cy.get("button[value=ships]").click();
        cy.then(() => {
            // make sure it works more than once
            grid.click_handler(0, 0);
        });
        cy.get("button[value=selection]").should("have.attr", "aria-selected", "true");
    });
    it.skip("doesn't leak memory in hex hover handling", function () {
        const ITERATIONS = 5;
        const grid = this.component.$.exposed?.grid?.value;
        expect(grid).to.not.equal(undefined);
        for (let i = 0; i < ITERATIONS; i++) {
            cy.then(() => {
                grid.forEach((tile: Hex) => {
                    cy.then(() => {
                        grid.mousemove_handler(tile.x, tile.y);
                    });
                    cy.wait(1); // doesn't work without this for some reason
                    cy.then(() => {
                        grid.mousemove_handler(grid.world_size, grid.world_size);
                    });
                    cy.wait(1);
                });
            });
        }
    });
});
