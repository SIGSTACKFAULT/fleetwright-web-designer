import { data, data_promise } from "@/globals";
import { Ship } from "@/ship/Ship";

import ShipCombatStation from "@/ship/ShipCombatStation.vue";

await data_promise;

describe("<ShipCombatStation />", () => {
    beforeEach(() => {
        cy.viewport(1000, 500);
        cy.mount(ShipCombatStation, {
            props: {
                ship: new Ship({ name: "test" }),
                prototype: data.combat_stations?.damcon!,
                crew: {},
                persistence: {},
                "onUpdate:crew": cy.spy().as("crew"),
            },
        });
        cy.get(".v-expansion-panel-title").click();
    });
    it("renders", () => {});
    it("edits crew numbers", () => {
        cy.get("[data-crew-type]").each((el) => {
            const num = Math.floor(Math.random() * 1000);
            const crewtype: string = el.attr("data-crew-type") ?? "";
            cy.wrap(crewtype).should("not.be.empty");
            cy.wrap(el).find("input").clear().type(num.toString());
            cy.get("@crew")
                .its("lastCall.args.0")
                .should("include", { [crewtype]: num });
            cy.wrap(el).find("input").clear();
            cy.get("@crew")
                .its("lastCall.args.0")
                .should("include", { [crewtype]: 0 });
        });
    });
    it("calculates total crew", () => {
        let sum = 0;
        cy.get("input")
            .each((el) => {
                const num = Math.floor(Math.random() * 1000);
                cy.wrap(el).type(num.toString());
                sum += num;
            })
            .then(() => {
                cy.get("[data-cy=total]").should("contain.text", sum.toLocaleString());
            });
    });
});
