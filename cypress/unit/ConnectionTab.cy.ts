import ConnectionTab from "@/vtt/ConnectionTab.vue";

function mount_wrapped() {
    return cy.mount(ConnectionTab, {
        props: {
            endpoint: "endpoint",
            auto_connect: false,
            connected: false,
            loading: false,
            room: undefined,
            onConnect: cy.spy().as("connect"),
            onDisconnect: cy.spy().as("disconnect"),
            "onUpdate:auto_connect": cy.spy().as("auto_connect"),
            "onUpdate:endpoint": cy.spy().as("endpoint"),
        },
    });
}

describe("<ConnectionTab />", () => {
    it("edits endpoint", () => {
        mount_wrapped().then(() => {
            cy.get("#server-url").should("have.value", "endpoint");
            cy.get("#server-url").clear().type("foobarbaz");
            cy.get("@endpoint").should("have.been.calledWith", "foobarbaz");
        });
    });
    it("emits 'connect' event", () => {
        mount_wrapped().then(() => {
            cy.contains("button.bg-primary", "connect", { matchCase: false }).click();
            cy.get("@connect").should("have.been.called");
        });
    });
    it("auto-connect tickbox", () => {
        mount_wrapped().then(() => {
            cy.get("#auto-connect").click();
            cy.get("@auto_connect").should("have.been.calledWith", true);
        });
    });
    it("disconnects", () => {
        mount_wrapped().then(({ wrapper }) => {
            cy.then(() => {
                wrapper.setProps({ connected: true });
            });
            cy.get("[data-cy=disconnect]").click();
            cy.get("@disconnect").should("have.been.called");
        });
    });
});
