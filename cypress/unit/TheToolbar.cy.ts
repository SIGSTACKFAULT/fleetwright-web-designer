import TheToolbar from "@/components/TheToolbar.vue";

describe("<TheToolbar />", () => {
    it("renders", () => {
        // see: https://on.cypress.io/mounting-vue
        cy.mount(TheToolbar, {
            props: {
                debug: false,
                debug_level: 1,
                theme: "dark",
            },
        });
    });
});
