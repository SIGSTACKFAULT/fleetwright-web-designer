import ShipNameDialog from "@/components/dialogs/ShipNameDialog.vue";

describe("<ShipNameDialog />", () => {
    it("mounts without dialog prop", () => {
        cy.mount(ShipNameDialog, {
            props: {
                name: "",
                prefix: "",
                callsign: "",
            },
        });
    });
    it("works", () => {
        cy.viewport(1000, 500);
        cy.mount(ShipNameDialog, {
            props: {
                name: "",
                prefix: "",
                callsign: undefined,
                dialog: true,
                "onUpdate:name": cy.spy().as("name"),
                "onUpdate:prefix": cy.spy().as("prefix"),
                "onUpdate:callsign": cy.spy().as("callsign"),
            },
        });
        cy.get("[data-cy=name]").type("Hello").wait(200);
        cy.get("[data-cy=prefix]").type("ABC").wait(200);
        cy.get("[data-cy=callsign]").type("World").wait(200);
        cy.get("@name").should("have.been.calledWith", "Hello");
        cy.get("@prefix").should("have.been.calledWith", "ABC");
        cy.get("@callsign").should("have.been.calledWith", "World");
        cy.get("[data-cy=CommonDialog-close]").click();
        cy.get("[data-cy=CommonDialog-close]").should("not.exist");
    });
});
