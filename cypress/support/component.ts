// ***********************************************************
// This example support/component.ts is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************
// Import commands.js using ES2015 syntax:
// Alternatively you can use CommonJS syntax:
// require('./commands')
// Import global styles
import { mount } from "cypress/vue";
import VueSortable from "vue3-sortablejs";
import VuetifyNotifier from "vuetify-notifier";

import "./commands";
import router from "@/plugins/router";
import { vuetify } from "@/plugins/vuetify";

import "@/styles/main.scss";

// Augment the Cypress namespace to include type definitions for
// your custom command.
// Alternatively, can be defined in cypress/support/component.d.ts
// with a <reference path="./component" /> at the top of your spec.
/* eslint-disable @typescript-eslint/no-namespace */
declare global {
    namespace Cypress {
        interface Chainable {
            mount: typeof mount;
        }
    }
}

Cypress.Commands.add("mount", (component, options = {}) => {
    // https://stackoverflow.com/a/76128540/2729876
    options.global = options.global ?? {};
    options.global.plugins = options.global.plugins ?? [];
    options.global.plugins.push(vuetify);
    options.global.plugins.push(VuetifyNotifier);
    options.global.plugins.push(router);
    options.global.plugins.push(VueSortable);
    options.global.stubs = options.global.stubs ?? {};
    (options.global.stubs as any).transition = false;
    options.global.components = options.global.components || {};
    return mount(component, options);
});

// Example use:
// cy.mount(MyComponent)
