/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }

Cypress.Commands.add("withinEach", { prevSubject: true }, (subject, callback) => {
    cy.wrap(subject).each((item) => {
        cy.wrap(item).within(callback);
    });
    return subject;
});

Cypress.Commands.add("loadSample", (partial_sample_name, options = {}) => {
    if (options.visit != false) {
        cy.visit(options.visit ?? "/");
    }
    cy.get(".sample-dropdown-button").click();
    cy.contains(".v-list-item", partial_sample_name).click();
    if (options.edit ?? true) {
        cy.get(".ship-card [data-cy=edit]").last().click();
    }
});

Cypress.Commands.add("switchTab", (tab: string) => {
    cy.get(`a.v-tab[value=${tab}]`).click();
    cy.get(`a.v-tab[value=${tab}]`).should("have.attr", "aria-selected", "true");
});

beforeEach(() => {
    cy.spy(console, "warn").as("console.warn");
    cy.spy(console, "error").as("console.error");
});

afterEach(() => {
    cy.get("@console.error").should("not.have.been.called");
    cy.get("@console.warn").should("not.have.been.called");
    cy.get(".error").should("not.exist");
});

declare global {
    namespace Cypress {
        interface Chainable {
            withinEach(callback: (item: JQuery<Element>) => void): Chainable<Element>;
            loadSample(
                partial_sample_name: string,
                options?: { edit?: boolean; visit?: string | false },
            ): void;
            switchTab(tab: string): void;
        }
    }
}

export {};
