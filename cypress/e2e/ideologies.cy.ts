import { evaluate } from "mathjs";

const IDEOLOGY_VALUES = [-2, -1, +1, +2] as const;

describe("verify policy requirements", () => {
    const government = Cypress.env("government");
    const all_ideologies = Array.from(
        (function* () {
            for (const S of IDEOLOGY_VALUES) {
                for (const I of IDEOLOGY_VALUES) {
                    for (const E of IDEOLOGY_VALUES) {
                        for (const P of IDEOLOGY_VALUES) {
                            yield { S, I, E, P };
                        }
                    }
                }
            }
        })(),
    );
    for (const policy of Object.keys(government.policies)) {
        console.log(policy, typeof policy);
        it(policy, () => {
            let bad = 0;
            for (const i of all_ideologies) {
                const group = government.policies[policy];
                let good = false;
                for (const proto of Object.values<any>(group)) {
                    // if any of these pass, this ideology is good
                    if (proto.requirements == undefined) {
                        // something with no requirements
                        good = true;
                        break;
                    }
                    if (evaluate(proto.requirements, i)) {
                        good = true;
                        break;
                    }
                }
                if (!good) {
                    cy.log(`problem with [S:${i.S}/I:${i.I}/E:${i.E}/P:${i.P}]`);
                    bad++;
                }
            }
            cy.wrap(bad).should("eq", 0);
        });
    }
});
