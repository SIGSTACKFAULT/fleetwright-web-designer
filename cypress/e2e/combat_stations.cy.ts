describe("combat station editing", () => {
    //
    it("nerve center buff_choice works", () => {
        cy.loadSample("blank");
        cy.get("section[data-cy=combat-stations]").should("exist").scrollIntoView();
        cy.contains(".v-expansion-panel", "Nerve Center")
            .click()
            .within(() => {
                cy.get("[data-cy=crew-biological] input").type("123");
            });
        cy.get("[data-cy=buff-choice]").should("exist").click();
        cy.get(".v-list-item").contains("sensors").click({ scrollBehavior: false });
        cy.get("[data-cy=overview-sensors] .value").invoke("text").then(parseInt).should("eq", 2);
    });
});
