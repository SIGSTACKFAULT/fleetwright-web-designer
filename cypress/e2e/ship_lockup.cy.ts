describe("loading ships", () => {
    beforeEach(() => {
        cy.viewport(2000, 1000);
    });
    // #TODO can't reproduce a bug where the app locks up when loading a ship
    it("doesn't randomly lock up", () => {
        cy.loadSample("Pegasus", { edit: false });
        for (let i = 0; i < 10; i++) {
            // #TODO better fix for "`cy.click()` failed because the page updated while this command was executing."
            cy.get(".ship-card [data-cy=edit]").should("exist").click();
            cy.wait(50);
            cy.get("[data-cy=back]").should("exist").click();
            cy.wait(50);
        }
    });
});
