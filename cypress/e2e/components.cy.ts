const data = Cypress.env("data");
const groups = new Set<string>();
for (const id in data.components) {
    const component = data.components[id];
    groups.add(component.type);
}

describe("components", () => {
    for (const group of groups) {
        describe(`group: ${group}`, () => {
            for (const [id, prototype] of Object.entries<any>(data.components).filter(
                ([, comp]) => comp.type == group,
            )) {
                it(`component: ${id}`, () => {
                    cy.wrap(prototype).should("have.property", "name");
                    cy.wrap(prototype).should("have.property", "type");
                    cy.wrap(prototype).should("have.property", "density");
                    cy.wrap(prototype).should("have.property", "durability");
                    cy.wrap(prototype).should("have.property", "price");
                    cy.visit("/");
                    cy.get("[data-cy=ships] [data-cy=import]").click();
                    const json_data = JSON.stringify({
                        name: "test",
                        armour: 0,
                        components: [{ id, volume: 1, armour: false }],
                    });
                    cy.get("textarea")
                        .should("not.be.disabled")
                        .type(json_data, { parseSpecialCharSequences: false, delay: 0 });
                    cy.get("[data-cy=import-confirm]").click();
                    cy.get("[data-cy=CommonDialog-close]").click();
                    cy.get(".ship-card").first().click();
                    cy.get(`.ship-component[data-component=${id}]`)
                        .should("exist")
                        .within(() => {
                            if (prototype.fixed_volume == undefined) {
                                cy.get("[data-cy=increment]")
                                    .should("exist")
                                    .should("not.be.disabled");
                                cy.get("[data-cy=decrement]")
                                    .should("exist")
                                    .should("not.be.disabled");
                            } else {
                                cy.get("[data-cy=increment]").should("not.exist");
                                cy.get("[data-cy=decrement]").should("not.exist");
                            }
                        });
                });
            }
        });
    }
});
