describe("samples", () => {
    const samples = Cypress.env("samples");
    for (const sample of samples.filter((x: any) => "value" in x)) {
        it(`loads ${sample.title}`, () => {
            cy.visit("/");
            cy.get(".sample-dropdown-button").click();
            cy.wait(100);
            cy.get(".v-overlay .v-list-item").contains(sample.title).click();
            cy.wait(100);
            cy.get(".ship-card").first().click();
            if (sample.__allow_warnings != true) {
                // insist that the samples have no warnings
                // except some of them need to be excluded (like the blank sample)
                cy.get("#ship-name").should("exist");
                cy.get("[data-cy=ShipWarnings] .warning").should("not.exist");
            }
            cy.get("[aria-label=Back]").click();
        });
    }
});
