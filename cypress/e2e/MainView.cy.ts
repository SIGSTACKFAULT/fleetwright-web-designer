import { create_faction_via_wizard } from "./create_faction_via_wizard";

describe("Main View functionality", () => {
    beforeEach(() => {
        cy.visit("/");
    });
    describe("ships section", () => {
        it("creates blank ships", () => {
            cy.get("section[data-cy=ships]").within(() => {
                cy.contains("button", "new", { matchCase: false }).click();
                cy.get(".ship-card").should("exist");
                cy.contains("button", "new", { matchCase: false }).click();
                cy.get(".ship-card").should("have.length", 2);
            });
        });
        it("imports ships", () => {
            const ship = {
                name: "foo",
                armour: 1337,
                components: [{ id: "bar", volume: 1234 }],
            };
            cy.get("section[data-cy=ships]").within(() => {
                cy.contains("button", "import", { matchCase: false }).click();
            });
            cy.get(".v-overlay").within(() => {
                cy.get("textarea")
                    .should("exist")
                    .should("have.length", 1)
                    .type(JSON.stringify(ship), { parseSpecialCharSequences: false, delay: 0 });
                cy.contains("button", "import", { matchCase: false }).click();
                cy.get("[data-cy=CommonDialog-close]").click();
            });
            cy.get(".ship-card").should("exist");
        });
        it("edits ship names", () => {
            const SHIBBOLETH = "shibboleth: MainView.cy.ts (ship rename)";
            cy.loadSample("Newsflash", { edit: false, visit: false });
            cy.get("[data-cy=ShipCard]").find("[data-cy=cog]").click();
            cy.contains("rename", { matchCase: false }).click();
            cy.get("[data-cy=name] input").clear().type(SHIBBOLETH);
            cy.get("[data-cy=CommonDialog-close]").click();
            cy.contains("[data-cy=ShipCard]", SHIBBOLETH).should("exist");
        });
        it("edits faction names", () => {
            const SHIBBOLETH = "shibboleth: MainView.cy.ts (faction rename)";
            cy.wait(1000); // visit doesn't work without this; no clue why
            create_faction_via_wizard();
            cy.get("[data-cy=FactionCard]").find("[data-cy=cog]").click();
            cy.contains("rename", { matchCase: false }).click();
            cy.get("[data-cy=faction-name] input").clear().type(SHIBBOLETH);
            cy.get("[data-cy=CommonDialog-close]").click();
            cy.contains("[data-cy=FactionCard]", SHIBBOLETH).should("exist");
        });
    });
});
