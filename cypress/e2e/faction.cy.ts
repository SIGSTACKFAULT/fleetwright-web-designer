describe("Faction edits", () => {
    beforeEach(() => {
        cy.visit("/");
        cy.get("[data-cy=import-faction]").click();
        cy.get(".v-overlay").within(() => {
            cy.get("textarea").type("{}", { parseSpecialCharSequences: false });
            cy.contains("button", "import", { matchCase: false }).click();
        });
        cy.visit("/#/faction/0/edit");
    });

    describe("shuffle", () => {
        beforeEach(() => {
            cy.get("[data-cy=FactionMetaButtons]")
                .contains("shuffle all", { matchCase: false })
                .click();
        });
        it("generates nonzero CI", () => {
            cy.get("[data-cy=chip-CI]")
                .should("exist")
                .within(() => {
                    cy.get(".value")
                        .invoke("text")
                        .then((text) => {
                            cy.wrap(parseInt(text)).should("be.greaterThan", 0);
                        });
                });
        });
        it("generates complete faction", () => {
            cy.get("[data-cy=FactionMetaButtons]")
                .contains("turn planner", { matchCase: false })
                .should("not.be.disabled");
        });
        it("uses all FP", () => {
            cy.get("[data-cy=chip-FP]")
                .get(".value")
                .invoke("text")
                .then((text) => {
                    cy.wrap(parseInt(text.split("/")[0])).should("eq", 0);
                });
        });
        it("LazySystemPanel exists", () => {
            cy.get("[data-cy=LazySystemPanel]").click();
        });
    });
});
