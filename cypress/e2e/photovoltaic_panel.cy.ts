describe("Photovoltaic Panel", () => {
    it("light level slider should be invisible in blank ship", () => {
        cy.visit("/");
        cy.get("#add-ship").click();
        cy.visit("/#/ship/0");
        cy.get("#ship-name").should("exist");
        cy.get("[data-cy=light-level-slider]").should("not.exist");
    });
    it("light level slider should be visible ship with panel", () => {
        cy.visit("/");
        cy.get("#add-ship").click();
        cy.visit("/#/ship/0");
        cy.get("#ship-name").should("exist");
        cy.contains("Powerplants").click();
        cy.contains("Photovoltaic Solar Panel").click();
        cy.get("[data-cy=light-level-slider]").should("exist");
    });
    it("Light level should affect photovoltaic panel energy", () => {
        cy.visit("/");
        cy.get("#add-ship").click();
        cy.visit("/#/ship/0");
        cy.get("#ship-name").should("exist");
        cy.contains("Powerplants").click();
        cy.contains("Photovoltaic Solar Panel").click();
        cy.get(".v-card-text [data-cy=increment]").click({ shiftKey: true });
        cy.get("[data-cy=simulation]").click();
        cy.get("[data-component=photovoltaic] .v-input input").click();
        cy.get("[data-cy=overview-energy] .value").should("contain.text", "1");
        cy.get("[data-cy=light-level-slider]").click(); // Click in the center to set the light level to 50%
        cy.get("[data-cy=overview-energy] .value").should("contain.text", "0");
    });
});
