describe("ship overview", () => {
    beforeEach(() => {
        cy.viewport(2000, 1000);
        cy.visit("/");
    });
    it("overview values are present", () => {
        cy.loadSample("Pegasus");
        cy.get("[data-cy=simulate-all]").click();
        for (const item of ["volume", "mass", "cost", "integrity", "sensors", "energy", "heat"]) {
            cy.get(`[data-cy=overview-${item}] .value`).contains(/\d+/);
        }
        cy.get("[data-cy=overview-crew] .value").contains(/\d+\/\d+/);
        cy.get("[data-cy=overview-wiakia] .value").contains(/\d+:\d+/);
    });
    it("simulation toggle disabled with no components", () => {
        cy.loadSample("blank");
        cy.get("[data-cy=simulation] input").should("have.attr", "disabled");
        cy.get(".component-widget .v-expansion-panel-title").first().click(); // add the first component
        cy.get(".component-group .v-list-item").first().click();
        cy.get("[data-cy=simulation] input").should("not.have.attr", "disabled");
    });
    it("simulation toggle changes chip colors", () => {
        cy.loadSample("Pegasus");
        cy.get("[data-cy=simulation] input").should("not.have.attr", "disabled");
        let bg_class: string;
        cy.get("[data-cy=overview-thrust]").then((el) => {
            const bg_classes = el
                .attr("class")
                ?.split(/\s+/)
                .filter((c) => c.match(/bg-\w+/));
            cy.wrap(bg_classes).should("not.be", undefined);
            cy.wrap(bg_classes).should("have.length", 1);
            bg_class = (bg_classes as string[])[0];
        });
        cy.get("[data-cy=simulation] input").click();
        cy.get("[data-cy=overview-thrust]").then((el) => {
            cy.wrap(el).should("not.have.class", bg_class);
        });
    });
});
