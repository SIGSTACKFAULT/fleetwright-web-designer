export function visit_wizard() {
    cy.visit("/#/faction/wizard");
}

export function next() {
    cy.get("[data-cy=next]").should("not.be.disabled").click();
}

export function phase1() {
    cy.get("[data-cy=faction-name]").type("create_faction_via_wizard()");
    cy.get("[data-cy=gov-table]").contains("Gestalt").click();
}

export function phase2() {
    cy.get("[data-cy=phase2-row]").withinEach(() => {
        cy.get("[data-cy=ideology-2]").click();
    });
}

export function phase3() {
    cy.get("[data-cy=shuffle-all]").click();
}

export function phase4() {
    cy.get("[data-cy=system-name] input").type("qwirpqoier");
    cy.get("[data-cy-name=edit_ci] input").first().type("180");
}

export function create_faction_via_wizard() {
    visit_wizard();
    phase1();
    next();
    phase2();
    next();
    phase3();
    next();
    phase4();
    next();
    next();
}
