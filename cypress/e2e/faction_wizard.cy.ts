import {
    create_faction_via_wizard,
    next,
    phase1,
    phase2,
    phase3,
    visit_wizard,
} from "./create_faction_via_wizard";

// failed to import it from Slot.ts due to typescript nonsense
export const SLOT_TYPES = [
    // "Star",
    // "Empty",
    "Asteroid Belt",
    "Rocky",
    "Terraformed",
    "Gas Giant",
    // "Void",
] as const;

describe("faction wizard", () => {
    it("works", () => {
        create_faction_via_wizard();
    });
    for (const type of SLOT_TYPES) {
        it(`All slots as ${type} work`, () => {
            // at some point during RealSystem development i had a problem where some slots didn't work
            // but only sometimes
            // and i think only asteroid belts
            visit_wizard();
            phase1();
            next();
            phase2();
            next();
            phase3();
            next();
            cy.get("[data-cy=SlotCard]").each((el, i) => {
                if (i == 0 || i == 10) {
                    // star or void
                    return;
                }
                cy.wrap(el).within(() => {
                    cy.get("[data-cy=edit-slot-button]").click();
                });
                cy.get("[data-cy=select-slot-type]").click();
                cy.get(".v-overlay").contains(type).click();
                // ...
                cy.get("body").type("{esc}");
            });
            cy.get("[data-cy=NumberEditor]").each((el) => {
                cy.wrap(el).find("[data-cy=increment]").click();
                cy.get("[data-cy=ci-used]").invoke("text").should("eq", "1");
                cy.wrap(el).find("[data-cy=decrement]").click();
            });
        });
    }
});
