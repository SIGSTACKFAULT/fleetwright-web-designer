describe("ship editor", () => {
    beforeEach(() => {
        cy.viewport(2000, 1000);
    });
    it("edits armour mass", () => {
        cy.loadSample("Newsflash");
        cy.get("[data-cy=armour-mass]").within((el) => {
            cy.wrap(el).get("[data-cy=increment]").click().click().click();
            cy.get("[data-cy=number]").should("contain.text", 3);
        });
    });
    it("edits ship name, prefix, callsign", () => {
        cy.loadSample("Pegasus");
        cy.get("#ship-name-edit-button").click();
        cy.get("[data-cy=name] input").clear().type("asdfasdf");
        cy.contains("#ship-name", "asdfasdf");
        cy.get("[data-cy=prefix] input").clear().type("FOO");
        cy.contains("#ship-prefix", "FOO");
        cy.get("[data-cy=callsign] input").clear().type("Wolf 1");
        cy.contains("#ship-callsign", "Wolf 1");
        cy.get("[data-cy=CommonDialog-close]").click();
    });
    it("print button works", () => {
        cy.loadSample("Newsflash");
        cy.window().then((window) => {
            cy.stub(window, "print").as("print");
        });
        cy.contains("button", "print", { matchCase: false }).click();
        cy.get("@print").should("have.been.called");
    });
    it("back button works", () => {
        cy.loadSample("Newsflash");
        cy.get("[aria-label=Back]").click();
        cy.get(".ship-card");
    });
    it("deletes ships", () => {
        cy.loadSample("Newsflash", { edit: false });
        cy.get("[aria-label=Overflow]").click();
        cy.get("[aria-label=Delete]").click({ shiftKey: true, scrollBehavior: "center" });
        cy.get(".ship-card").should("not.exist");
    });
    it("deletes components", () => {
        cy.loadSample("blank");
        cy.get(".component-widget .v-expansion-panel").first().click();
        cy.get(".v-list-item").first().click();
        cy.get(".ship-component").first().get("[data-cy=overflow]").click();
        cy.get(".v-menu").within(() => {
            cy.get("[data-cy=trash]").click({ scrollBehavior: "center" });
        });
        cy.get(".ship-component").should("not.exist");
    });
    describe("missile editing", () => {
        beforeEach(() => {
            cy.loadSample("Jumping");
            cy.get("[data-cy=missiles]").first().click();
        });
        it("add missiles", () => {
            // there should already be one missile but the vls is full so the add button is disabled
            cy.get("[data-cy=add-missile]").should("be.disabled");
            cy.get("[data-cy=delete]").click();
            cy.get("[data-cy=add-missile]").click();
            cy.get("[data-cy=add-missile]").click();
            cy.get("[data-cy=MissileEditor]").then((el) => {
                cy.wrap(el.length).should("eq", 2);
            });
        });
        it("remove missiles", () => {
            cy.get("[data-cy=delete]").click();
            cy.get("[data-cy=MissileEditor]").should("not.exist");
        });
    });
    it("ship export", () => {
        cy.loadSample("Jumping");
        cy.get("#ship-name-edit-button").click();
        cy.get("[data-cy=name] input").clear().type("foobarbaz {esc}");
        cy.get("[data-cy=export]").click();
        cy.get("textarea[readonly]")
            .invoke("val")
            .should("match", /name: "?foobarbaz"?/);
    });
});
