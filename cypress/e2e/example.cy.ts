// https://docs.cypress.io/api/introduction/api.html

describe("toolbar", () => {
    it("displays", () => {
        cy.visit("/");
        cy.contains(".v-toolbar-title", "FLEETWRIGHT SHIP DESIGNER", { matchCase: false });
    });
});

describe("footer", () => {
    it("displays", () => {
        cy.visit("/");
        cy.get("footer").should("exist");
    });
    it("report bug link exists", () => {
        cy.visit("/");
        cy.get("a#report-bug").should("exist");
    });
});

describe("VTT Exists", () => {
    it("vtt route works", () => {
        cy.visit("/#/vtt");
        cy.contains("button", "Server");
    });
});
