import { create_faction_via_wizard } from "./create_faction_via_wizard";

function ten_of_each_mi() {
    cy.get("[data-cy=add-mi]").click();
    cy.get("[data-cy=BuildOrder][data-type=mi]").click();
    cy.get("[data-cy=BuildOrder][data-type=mi] tr[data-facility]").withinEach((el) => {
        cy.wrap(el);
        cy.get("[data-cy=increment]").click({ shiftKey: true });
    });
}

describe("Turn Planner", () => {
    beforeEach(() => {
        cy.loadSample("Pegasus");
        create_faction_via_wizard();
        cy.visit("/#/faction/0/turn-planner");
    });
    it("plans turns", () => {
        // we have no yards so shipyard job button should be disabled
        cy.get("[data-cy=add-job]").should("be.disabled");
        cy.get("[data-cy=add-ci]").click();
        cy.get("[data-cy=BuildOrder][data-type=ci]")
            .click()
            .within(() => {
                cy.get("[data-cy-name=ci-amount] input").clear().type("10");
            });
        ten_of_each_mi();
        // we have yards now
        cy.get("[data-cy=add-job]").should("be.enabled").click();
    });
    it.only("Execute Overwrite works", () => {
        cy.get("[data-cy=add-ci]").click();
        cy.get("[data-cy-name=ci-amount]").clear().type("12");
        ten_of_each_mi();
        cy.get("[data-cy=execute]").click();
        cy.get("[data-cy=execute-overwrite]").click();
        cy.get("[data-cy=FactionCard]")
            .eq(0)
            .within(() => {
                cy.get(".chip[data-cy=ci] .value").invoke("text").should("eq", "192");
                cy.get(".chip[data-cy=mi] .value").invoke("text").should("eq", "80");
            });
    });
    it("Execute Copy works", () => {
        cy.get("[data-cy=add-ci]").click();
        cy.get("[data-cy-name=ci-amount]").clear().type("12{delete}");
        ten_of_each_mi();
        cy.get("[data-cy=execute]").click();
        cy.get("[data-cy=execute-copy]").click();
        cy.get("[data-cy=FactionCard]").should("have.length", 2);
        cy.get("[data-cy=FactionCard]")
            .eq(0)
            .within(() => {
                cy.get(".chip[data-cy=ci] .value").invoke("text").should("eq", "180");
                cy.get(".chip[data-cy=mi] .value").invoke("text").should("eq", "0");
            });
        cy.visit("/");
        cy.get("[data-cy=FactionCard]")
            .eq(1)
            .within(() => {
                cy.get(".chip[data-cy=ci] .value").invoke("text").should("eq", "192");
                cy.get(".chip[data-cy=mi] .value").invoke("text").should("eq", "80");
            });
    });
    it("financials work", () => {
        // check that the delta from the overview matches the actual change

        cy.get("[data-cy=add-ci]").click();
        cy.get("[data-cy=BuildOrder][data-type=ci]")
            .click()
            .within(() => {
                cy.get("[data-cy-name=ci-amount] input").clear().type("10");
            });
        cy.get(".chip[data-cy=bottom_line] .value")
            .invoke("text")
            .then((text) => {
                cy.log(`expecting bank to be ${text} after execution`);
                const match = text.match(/[\d+-]+/)!;
                cy.wrap(match).should("not.be.null");
                const expected_balance = parseInt(match[0]);
                cy.wrap(expected_balance).should("not.be.NaN");
                cy.get("[data-cy=execute]").click();
                cy.get("[data-cy=execute-copy]").click();
                cy.get("[data-cy=FactionCard]").eq(1).find("[data-cy=edit]").click();
                cy.get(".chip[data-cy=bank] .value")
                    .invoke("text")
                    .then((text) => {
                        cy.log(`bank shows ${text} after execution`);
                        const match = text.match(/[\d+-]+/)!;
                        cy.wrap(match).should("not.be.null");
                        const actual_balance = parseInt(match[0]);
                        cy.wrap(expected_balance).should("not.be.NaN");
                        cy.wrap(actual_balance).should("eq", expected_balance);
                    });
            });
        // cy.get("[data-cy=execute]").click();
        // cy.get("[data-cy=execute-copy]").click();
    });
});
