describe("vtt connection works", () => {
    it("connects once", () => {
        cy.visit("/#/vtt");
        cy.fixture("vtt-server.json").then(({ endpoint }) => {
            cy.get("#server-url").clear().type(endpoint);
        });
        cy.get("button[data-cy=connect]").click();
        cy.get("button[data-cy=connect]").should("be.disabled");
    });
    it("connects twice", () => {
        cy.visit("/#/vtt");
        cy.fixture("vtt-server.json").then(({ endpoint }) => {
            cy.get("#server-url").clear().type(endpoint);
        });
        cy.get("button[data-cy=connect]").click();
        cy.get("button[data-cy=connect]").should("be.disabled");
        cy.get("button[data-cy=disconnect]").click();
        cy.get("button[data-cy=connect]").click();
        cy.get("button[data-cy=connect]").should("be.disabled");
    });
    it("connects after switching tabs", () => {
        cy.visit("/#/vtt");
        cy.fixture("vtt-server.json").then(({ endpoint }) => {
            cy.get("#server-url").clear().type(endpoint);
        });
        cy.get("button[data-cy=connect]").click();
        cy.get("button[data-cy=connect]").should("be.disabled");
        cy.get("button[data-cy=disconnect]").click();
        cy.switchTab("edit");
        cy.wait(500);
        cy.switchTab("vtt");
        cy.get("button[data-cy=connect]").click();
        cy.get("button[data-cy=connect]").should("be.disabled");
    });
});
