//! decides which tool is active

use super::Tool;
use bevy::prelude::*;

pub struct ToolIconPlugin;
impl Plugin for ToolIconPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup)
            .add_systems(Update, (hide_entire_thing, move_follower, change_tool_icon));
    }
}

#[derive(Component)]
struct MouseFollowerRoot;
#[derive(Component)]
struct MouseFollower;
#[derive(Component)]
struct ToolIcon;
#[derive(Component)]
pub struct ToolText;

fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    let icon_size = Val::Px(24.);
    commands
        .spawn((
            NodeBundle {
                style: Style {
                    width: Val::Percent(100.0),
                    height: Val::Percent(100.0),
                    // border: UiRect::all(Val::Px(5.)),
                    ..default()
                },
                ..default()
            },
            MouseFollowerRoot,
            Name::new("Tool Icon Parent"),
        ))
        .with_children(|parent| {
            parent
                .spawn((
                    NodeBundle {
                        style: Style {
                            display: Display::Flex,
                            border: UiRect::all(Val::Px(5.0)),
                            position_type: PositionType::Absolute,
                            column_gap: Val::Px(5.0),
                            align_items: AlignItems::Center,
                            ..default()
                        },
                        visibility: Visibility::Hidden,
                        ..default()
                    },
                    Name::new("Mouse Follower"),
                    MouseFollower,
                ))
                .with_children(|parent| {
                    parent.spawn((
                        NodeBundle {
                            style: Style {
                                width: icon_size,
                                height: icon_size,
                                ..default()
                            },
                            // a `NodeBundle` is transparent by default, so to see the image we have to its color to `WHITE`
                            background_color: Color::WHITE.into(),
                            visibility: Visibility::Inherited,
                            ..default()
                        },
                        ToolIcon,
                        Name::new("Tool Icon"),
                        UiImage::new(asset_server.load("todo.qoi")),
                    ));
                    parent.spawn((
                        TextBundle {
                            text: Text::from_section(
                                "",
                                TextStyle {
                                    color: Color::GREEN,
                                    font_size: 25.,
                                    ..default()
                                },
                            ),
                            visibility: Visibility::Inherited,
                            ..default()
                        },
                        ToolText,
                    ));
                });
        });
}

/// hide this entire getup if we're in Tool::Select
fn hide_entire_thing(
    tool: Res<State<Tool>>,
    mut root: Query<&mut Visibility, With<MouseFollowerRoot>>,
) {
    let mut root = root.single_mut();
    if **tool == Tool::Select {
        *root = Visibility::Hidden;
    } else {
        *root = Visibility::Visible;
    }
}

fn move_follower(
    window: Query<&Window, With<bevy::window::PrimaryWindow>>,
    mut icon: Query<(&mut Style, &mut Visibility), With<MouseFollower>>,
) {
    let (mut style, mut vis) = icon.single_mut();
    let window = window.single();
    if let Some(position) = window.cursor_position() {
        // offset from bottom-left so it shows to the top-right of the cursor
        style.left = Val::Px(position.x);
        style.bottom = Val::Px(window.height() - position.y);
        *vis = Visibility::Inherited;
    } else {
        // mouse off-screen
        *vis = Visibility::Hidden;
    };
}

fn change_tool_icon(
    tool: Res<State<Tool>>,
    icon: Query<Entity, With<ToolIcon>>,
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    if tool.is_changed() {
        let entity = icon.single();
        let path = (**tool).get_icon();
        if let Some(path) = path {
            commands
                .entity(entity)
                .insert(UiImage::new(asset_server.load(path)))
                .insert(Visibility::Visible);
        } else {
            commands.entity(entity).insert(Visibility::Hidden);
        }
    }
}
