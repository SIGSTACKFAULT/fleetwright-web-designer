use bevy::prelude::*;
use hexx::Hex;

use crate::{
    custom_gizmos,
    grid::{Grid, GridMouseLocation},
    tools::Tool,
};

use super::tool_icon::ToolText;

pub struct MeasureToolPlugin;
impl Plugin for MeasureToolPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup)
            .add_systems(Update, measure_tool)
            // .add_systems(OnEnter(ToolState::Some(Tool::Measure)), (start_measure,))
            .add_systems(OnExit(Tool::Measure), (measure_tool_exit,))
            .init_resource::<MeasureToolState>();
    }
}

#[derive(Resource, Default)]
struct MeasureToolState {
    start: Option<Hex>,
}

#[derive(Component)]
struct MeasureToolText;

/// render the text this many times bigger then scale it by 1/this
static TEXT_SCALE: f32 = 5.0;

fn setup(mut commands: Commands) {
    commands.spawn((
        Text2dBundle {
            text: Text::from_section(
                "",
                TextStyle {
                    font_size: 20. * TEXT_SCALE,
                    ..default()
                },
            ),
            transform: Transform::from_scale(Vec3::splat(1. / TEXT_SCALE)),
            ..default()
        },
        MeasureToolText,
        Name::new("Measure Tool Text"),
    ));
}

fn measure_tool(
    //
    mut gizmos: Gizmos,
    mouse: Res<GridMouseLocation>,
    tool: Res<State<Tool>>,
    grid: Res<Grid>,
    mut state: ResMut<MeasureToolState>,
    mut text: Query<&mut Text, With<ToolText>>,
) {
    if **tool == Tool::Measure {
        let Some(mouse) = mouse.location else {
            // mouse is off the grid
            return;
        };
        if state.start.is_none() {
            // just started measuring
            state.start = Some(mouse);
        }
        let start = state.start.unwrap();
        let length = start.distance_to(mouse);
        if length > 0 {
            // now draw the arrow
            custom_gizmos::arrow(&grid, &mut gizmos, start, mouse, Color::WHITE);
        }
        text.single_mut().sections[0].value = format!("{}", length);
    }
}

fn measure_tool_exit(
    //
    mut text: Query<&mut Text, With<ToolText>>,
    mut state: ResMut<MeasureToolState>,
) {
    let mut text = text.single_mut();
    text.sections[0].value = "".to_string();
    state.start = None;
}
