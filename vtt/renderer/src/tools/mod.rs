mod burn;
mod dragndrop;
mod jump;
mod measure;
mod selection;
mod tool_icon;
mod tool_manager;

use crate::interface_rx::OnStateChange;
use bevy::prelude::*;
pub use dragndrop::DragNDropAble;
pub use selection::Selectable;
pub use tool_manager::Tool;

/// Implements ALL the tools. mission-critical.
pub struct ToolPlugin;
impl Plugin for ToolPlugin {
    fn build(&self, app: &mut App) {
        app //.
            .add_plugins(tool_manager::ToolManagerPlugin)
            .add_plugins(tool_icon::ToolIconPlugin)
            .add_plugins(measure::MeasureToolPlugin)
            .add_plugins(dragndrop::DragNDropPlugin)
            .add_systems(Update, burn::burn_tool)
            .add_systems(Update, jump::jump_tool)
            .add_systems(
                Update,
                (
                    selection::handle_selectable_click,
                    selection::draw_selection_circles,
                ),
            )
            .add_systems(OnEnter(Tool::ShipBurn), |world: &mut World| {
                world.run_schedule(OnStateChange)
            });
    }
}
