//! decides which tool is active and puts it in ToolState

use bevy::prelude::*;

pub struct ToolManagerPlugin;

impl Plugin for ToolManagerPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(bevy_rapid_qoi::QOIPlugin)
            .add_systems(Update, decide_tool)
            .add_state::<Tool>();
    }
}

#[derive(States, Copy, Clone, Debug, PartialEq, Eq, Hash, Default)]
pub enum Tool {
    #[default]
    Select,
    ShipBurn,
    ShipBurnAlt,
    ShipJump,
    DragNDrop,
    Measure,
    // AddMissile,
}

impl Tool {
    fn from_keycode(k: KeyCode) -> Option<Tool> {
        match k {
            // top row
            KeyCode::Q => Some(Tool::ShipBurn),
            KeyCode::W => Some(Tool::ShipBurnAlt),
            KeyCode::E => Some(Tool::ShipJump),
            KeyCode::T => Some(Tool::DragNDrop),
            KeyCode::A => Some(Tool::Measure),
            _ => None,
        }
    }

    pub fn get_icon(&self) -> Option<&'static str> {
        match *self {
            Self::Select => None,
            Self::ShipBurn => Some("burn.qoi"),
            Self::ShipBurnAlt => Some("burn2.qoi"),
            Self::ShipJump => Some("jump.qoi"),
            Self::DragNDrop => Some("drag.qoi"),
            Self::Measure => Some("measure.qoi"),
            // Self::AddMissile => Some("missile_plus.qoi"),
        }
    }
}

/// look at the keyboard and decide which tool should be active
fn decide_tool(
    //
    kb: Res<Input<KeyCode>>,
    current: Res<State<Tool>>,
    mut next: ResMut<NextState<Tool>>,
) {
    if !kb.is_changed() {
        // #TODO still triggerd by keyboard autorepeat
        return;
    }
    let tools: Vec<_> = kb
        .get_pressed()
        .filter_map(|k| Tool::from_keycode(*k))
        .collect();
    match tools.len() {
        0 => {
            // nothing we care about held down
            if *current != Tool::Select {
                next.set(Tool::default());
            }
        }
        1 => {
            // exactly one key we care about held down
            if tools[0] != **current {
                next.set(tools[0])
            }
        }
        _ => {
            let current_tool_is_held = tools.contains(&current);
            if current_tool_is_held {
                // holding down the key for the current tool *and* some other stuff
                // do nothing
            } else {
                // current tool not held
                // and holding several other things
                // pick one arbitrarily
                next.set(tools[0]);
            }
        }
    }
}
