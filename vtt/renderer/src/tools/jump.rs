use crate::custom_gizmos;
use crate::grid::{Grid, GridClickInside, GridMouseLocation, GridQueryReadOnly};
use crate::objects::Jump;
use crate::tools::Tool;
use bevy::prelude::*;
use hexx::Hex;

pub fn jump_tool(
    mut commands: Commands,
    mut gizmos: Gizmos,
    tool: Res<State<Tool>>,
    grid: Res<Grid>,
    ships: Query<GridQueryReadOnly>,
    mouse: Res<GridMouseLocation>,
    mut click: EventReader<GridClickInside>,
) {
    if **tool != Tool::ShipJump {
        return;
    }
    let Some(mouse) = mouse.location else {
        // off the grid
        return;
    };
    let selected: Vec<_> = ships
        .iter()
        .filter(|i| i.jump.is_some())
        .filter(|i| i.selected())
        .collect();
    let Some(primary) = selected.first() else {
        // nothing selected
        return;
    };
    let delta = mouse - primary.pos();
    // draw the arrows
    for ship in selected.iter() {
        custom_gizmos::arrow(
            &grid,
            &mut gizmos,
            ship.pos(),
            ship.pos() + delta,
            Color::RED,
        );
    }
    if let Some(_click) = click.read().last() {
        for item in selected.into_iter() {
            if delta == Hex::ZERO {
                commands.entity(item.entity).insert(Jump(None));
                if cfg!(feature = "bound") {
                    crate::interface_tx::update_jump_null(item.uuid.0.to_string());
                }
            } else {
                let target = item.pos() + delta;
                commands.entity(item.entity).insert(Jump(Some(target)));
                if cfg!(feature = "bound") {
                    crate::interface_tx::update_jump(item.uuid.0.to_string(), target.x, target.y);
                }
            }
        }
    }
}
