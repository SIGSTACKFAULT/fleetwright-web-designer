use crate::grid::{GridClickInside, GridLocation, GridMouseEvent};
use crate::interface_rx::OnStateChange;
use crate::objects::UUID;
use crate::tools::Tool;
use bevy::prelude::*;
use hexx::Hex;
use uuid::Uuid;

#[derive(Default, Debug, Component, Eq, PartialEq, Reflect)]
#[reflect(Component)]
pub struct DragNDropAble;

#[derive(Default, Debug, Component, Eq, PartialEq, Reflect)]
#[reflect(Component)]
pub struct GridDragging;

#[derive(Debug, Clone, Hash, PartialEq, Eq, States, Reflect, Default)]
pub enum DragNDropState {
    #[default]
    Idle,
    Dragging {
        current: Hex,
        entity: Entity,
        uuid: Uuid,
    },
}

pub struct DragNDropPlugin;
impl Plugin for DragNDropPlugin {
    fn build(&self, app: &mut App) {
        app //.
            .add_systems(
                Update,
                (click_handler, move_handler, release_handler)
                    .chain()
                    .run_if(in_state(Tool::DragNDrop)),
            )
            .add_systems(OnExit(Tool::DragNDrop), cancel)
            .add_state::<DragNDropState>()
            .register_type::<DragNDropAble>();
    }
}

fn cancel(mut next: ResMut<NextState<DragNDropState>>, mut commands: Commands) {
    info!("Canceling dragndrop");
    next.set(DragNDropState::Idle);
    // run OnStateChange in case an actual update happened while we were dragging
    commands.add(|world: &mut World| {
        world.run_schedule(OnStateChange);
    });
}

fn click_handler(
    mut click: EventReader<GridClickInside>,
    draggables: Query<(Entity, &GridLocation, &UUID), With<DragNDropAble>>,
    mut next: ResMut<NextState<DragNDropState>>,
) {
    let Some(click) = click.read().last() else {
        return;
    };
    let Some(clicked) = draggables
        .iter()
        .find(|(_e, loc, _s)| loc.0 == click.location)
    else {
        // clicked, but not on anything we care about
        return;
    };
    next.set(DragNDropState::Dragging {
        current: click.location,
        entity: clicked.0,
        uuid: clicked.2 .0,
    });
}

fn release_handler(
    buttons: Res<Input<MouseButton>>,
    state: Res<State<DragNDropState>>,
    mut next: ResMut<NextState<DragNDropState>>,
) {
    if buttons.just_released(MouseButton::Left) {
        next.set(DragNDropState::Idle);
        let DragNDropState::Dragging {
            current,
            entity: _,
            uuid,
        } = **state
        else {
            return;
        };
        if cfg!(feature = "bound") {
            info!("teleporting {}", uuid);
            crate::interface_tx::teleport(uuid.to_string(), current.x, current.y);
        }
    }
}

fn move_handler(
    state: Res<State<DragNDropState>>,
    mut commands: Commands,
    mut events: EventReader<GridMouseEvent>,
    mut next: ResMut<NextState<DragNDropState>>,
) {
    let DragNDropState::Dragging {
        current: _,
        entity,
        uuid,
    } = **state
    else {
        return;
    };
    if let Some(event) = events.read().last() {
        if let Some(current) = event.curr {
            next.set(DragNDropState::Dragging {
                current,
                entity,
                uuid,
            });
            commands.entity(entity).insert(GridLocation::from(current));
        }
    }
}
