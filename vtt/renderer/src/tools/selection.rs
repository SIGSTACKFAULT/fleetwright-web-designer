use crate::{grid::*, tools::Tool};
use bevy::prelude::*;
use itertools::Itertools;

/// indicates that this object is selectable, and the selection state
#[derive(Component, Copy, Clone, Debug, PartialEq, Eq, Default)]
pub enum Selectable {
    #[default]
    Deselcted,
    Selected,
}

impl Selectable {
    /// is the "Selected" variant
    pub fn selected(&self) -> bool {
        match self {
            Selectable::Deselcted => false,
            Selectable::Selected => true,
        }
    }
}

impl std::ops::Not for Selectable {
    type Output = Selectable;
    fn not(self) -> Self::Output {
        match self {
            Selectable::Deselcted => Selectable::Selected,
            Selectable::Selected => Selectable::Deselcted,
        }
    }
}

/// draw circles around selected ships
pub fn draw_selection_circles(
    objects: Query<(&Selectable, &GridLocation)>,
    mut gizmos: Gizmos,
    grid: Res<Grid>,
) {
    for (selectable, transform) in objects.iter() {
        if *selectable == Selectable::Selected {
            gizmos.circle_2d(grid.layout.hex_to_world_pos(transform.0), 10., Color::WHITE);
        }
    }
}

/// handle clicks on Selectables
pub fn handle_selectable_click(
    mut click: EventReader<GridClickInside>,
    mut ships: Query<GridQuery>,
    kb: Res<Input<KeyCode>>,
    tool: Res<State<Tool>>,
) {
    if **tool != Tool::Select {
        return;
    }
    let Some(click) = click.read().last() else {
        return;
    };
    // pick an arbitrary ship on the clicked hex to be the clicked one
    let Some(clicked_index) =
        ships
            .iter()
            .position(|GridQueryReadOnlyItem { loc, sel, .. }| {
                loc.0 == click.location && sel.is_some()
            })
    else {
        // clicked not on a ship
        return;
    };
    let mut ships = ships.iter_mut().collect_vec();
    let GridQueryItem {
        sel: Some(mut clicked),
        uuid: clicked_uuid,
        ..
    } = ships.remove(clicked_index)
    else {
        panic!();
    };
    let others_selected = ships.iter().filter(|gq| gq.selected()).count();

    if kb.pressed(KeyCode::ShiftRight) || kb.pressed(KeyCode::ShiftLeft) {
        *clicked = !*clicked;
    } else if others_selected > 0 {
        for item in ships.iter_mut() {
            item.set_selected(false);
        }
        *clicked = Selectable::Selected;
        if cfg!(feature = "bound") {
            info!("selecting {:?}", clicked_uuid);
            crate::interface_tx::update_primary_selection(Some(clicked_uuid.0.to_string()));
        }
    } else {
        *clicked = !*clicked;
        if cfg!(feature = "bound") {
            match *clicked {
                Selectable::Selected => {
                    crate::interface_tx::update_primary_selection(Some(clicked_uuid.0.to_string()))
                }
                Selectable::Deselcted => crate::interface_tx::update_primary_selection(None),
            }
        }
    };
}
