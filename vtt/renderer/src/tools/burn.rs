use crate::custom_gizmos;
use crate::grid::{Grid, GridClickInside, GridMouseLocation, GridQuery};
use crate::tools::Tool;
use bevy::prelude::*;
use hexx::Hex;

pub fn burn_tool(
    mut gizmos: Gizmos,
    tool: Res<State<Tool>>,
    grid: Res<Grid>,
    mut ships: Query<GridQuery>,
    mouse: Res<GridMouseLocation>,
    click: EventReader<GridClickInside>,
) {
    if **tool != Tool::ShipBurn {
        return;
    }
    let mut selected: Vec<_> = ships.iter_mut().filter(|obj| obj.selected()).collect();
    if selected.is_empty() {
        // Holding Q, but nothing selected
        // maybe someday show something on the screen
    } else {
        selected.sort_by_key(|l| l.accel());
        let slowest = &selected[0];
        let max_burn = slowest.accel();
        let origin = slowest.after_move();
        custom_gizmos::big_hexagon(&grid, &mut gizmos, origin, max_burn);
        if let Some(mouse) = mouse.location {
            let mut delta = mouse - origin;
            if delta.length() > max_burn as i32 {
                for x in delta.line_to(Hex::ZERO) {
                    // clamp delta to max_burn
                    if x.length() <= max_burn as i32 {
                        delta = x;
                        break;
                    };
                }
            }
            for ship in selected.iter_mut() {
                let from = ship.after_move();
                let to = from + delta;
                if delta != Hex::ZERO {
                    custom_gizmos::arrow(&grid, &mut gizmos, from, to, Color::CYAN);
                }
                if !click.is_empty() {
                    info!("click while holding Q at {:?}; delta is {:?}", mouse, delta);
                    if let Some(burn) = ship.burn.as_mut() {
                        burn.0 = delta;
                    }
                    if cfg!(feature = "bound") {
                        crate::interface_tx::update_burn(ship.uuid.0.to_string(), delta.x, delta.y);
                    }
                }
            }
        }
    }
}
