use crate::grid::{Grid, GridLocation, GridQuery};
use crate::objects::{ShipBundle, ShipResources};
use bevy::prelude::*;
use bevy_inspector_egui::bevy_egui::{egui, EguiContexts};
use hexx::Hex;

#[derive(Component)]
struct StepButtonMarker;
pub struct StandalonePhysicsPlugin;

impl Plugin for StandalonePhysicsPlugin {
    fn build(&self, app: &mut App) {
        app //
            .add_systems(Startup, spawn_test_ship)
            .add_systems(Update, standalone_ui);
    }
}

#[allow(clippy::type_complexity)]
fn standalone_ui(
    mut commands: Commands,
    mut egui: EguiContexts,
    mut ships: Query<GridQuery>,
    grid: Res<Grid>,
    sr: Res<ShipResources>,
) {
    egui::Window::new("Standalone").show(egui.ctx_mut(), |ui| {
        if ui.button("Standalone Physics Step").clicked() {
            info!("step");
            // update ship positions
            for mut item in ships.iter_mut() {
                if let Some(mut burn) = item.burn {
                    item.vel.0 += burn.0;
                    burn.0 = Hex::ZERO;
                }
            }
            commands.run_system(grid.apply_velocity);
            commands.run_system(grid.clamp_positions);
        }
        if ui.button("Spawn Ship").clicked() {
            commands.spawn(ShipBundle::new(&sr).with_acceleration(10));
        }
    });
}

/// spawn some ships for testing
fn spawn_test_ship(
    //
    mut commands: Commands,
    sr: Res<ShipResources>,
) {
    commands.spawn(ShipBundle::new(&sr).with_acceleration(10));
    commands
        .spawn(ShipBundle::new(&sr).with_acceleration(12))
        .insert(GridLocation(Hex::NEG_ONE));
}
