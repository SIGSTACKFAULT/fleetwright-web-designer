//! (q,r) text in top-right corner

use crate::grid::GridMouseLocation;
use bevy::prelude::*;

/// marker for the top-right (q,r) text
#[derive(Component)]
pub struct QRText;

impl Plugin for QRText {
    fn build(&self, app: &mut App) {
        app //
            .add_systems(Startup, spawn_qr_text)
            .add_systems(FixedUpdate, update_qr_text);
    }
}

fn spawn_qr_text(mut commands: Commands) {
    let coords = TextBundle::from_section(
        "",
        TextStyle {
            font_size: 24.,
            color: Color::WHITE,
            ..default()
        },
    )
    .with_style(Style {
        position_type: PositionType::Absolute,
        top: Val::Px(5.0),
        right: Val::Px(5.0),
        ..default()
    });
    commands.spawn(coords).insert(QRText {});
}

fn update_qr_text(
    mut text_query: Query<(&mut Text, &mut Visibility), With<QRText>>,
    hover: Res<GridMouseLocation>,
) {
    if hover.is_changed() {
        let (mut t, mut v) = text_query.single_mut();
        if let Some(hex) = hover.location {
            t.sections[0].value = format!("({}, {})", hex.x, hex.y).to_string();
            *v = Visibility::Visible;
        } else {
            *v = Visibility::Hidden;
        }
    }
}
