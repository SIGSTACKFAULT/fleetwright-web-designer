//! handles displaying the grid
//! top-right (q,r) text in qr_text.rs

mod events;
mod physics;
mod qr_text;
mod query;
mod transform;

use bevy::ecs::system::SystemId;
use bevy::prelude::*;
use hexx::Hex;
use hexx::MeshInfo;
use itertools::Itertools;

pub use events::*;
pub use query::*;
pub use transform::*;

pub const HEX_SIZE: Vec2 = Vec2::splat(10.0);
pub const MAP_RADIUS: u32 = 100;

pub type HexVector = Hex;

pub struct GridPlugin {}
impl Plugin for GridPlugin {
    fn build(&self, app: &mut App) {
        let bounds = hexx::HexBounds::new(Hex::ZERO, MAP_RADIUS);
        let layout = hexx::HexLayout {
            hex_size: HEX_SIZE,
            orientation: hexx::orientation::HexOrientation::Pointy,
            ..default()
        };
        let grid = Grid {
            bounds,
            layout,
            apply_velocity: app.world.register_system(physics::apply_velocity),
            clamp_positions: app.world.register_system(physics::clamp_positions),
        };

        app // .
            .add_plugins(qr_text::QRText)
            .add_systems(Startup, grid_setup)
            .add_systems(Update, grid_events)
            .add_systems(Update, hilight_hovered_hex)
            .add_systems(Update, transform::update_grid_transform)
            .add_event::<GridClickInside>()
            .add_event::<GridClickOutside>()
            .add_event::<GridMouseEvent>()
            .init_resource::<GridMouseLocation>()
            .insert_resource(grid)
            .register_type::<GridLocation>()
            .register_type::<GridZ>();
    }
}

/// marker for the parent entity of the whole grid
#[derive(Component)]
struct GridParentMarker;

/// marker for grid hexes
#[derive(Component)]
struct GridTileMarker;

/// marker for the entity we move around to hilight a hex
#[derive(Component)]
struct GridHoverMarker;

#[derive(Debug, Resource)]
pub struct Grid {
    pub bounds: hexx::HexBounds,
    pub layout: hexx::HexLayout,
    pub apply_velocity: SystemId,
    pub clamp_positions: SystemId,
}

impl Grid {
    pub fn get_transform(&self, hex: Hex, z: f32) -> Transform {
        let pos = self.layout.hex_to_world_pos(hex);
        Transform::from_xyz(pos.x, pos.y, z)
    }
}

/// generates MeshInfo at `hex` with `trans`
/// #TODO some sort of caching
fn mesh_info(layout: &hexx::HexLayout, hex: Hex, trans: Option<Transform>) -> MeshInfo {
    let mut mesh_info = hexx::PlaneMeshBuilder::new(layout)
        .facing(Vec3::Z)
        // .at(hex)
        .build();
    let Vec2 { x, y } = layout.hex_to_world_pos(hex);
    let trans = Transform::from_xyz(x, y, 0.).mul_transform(match trans {
        None => Transform::default(),
        Some(trans) => trans,
    });
    for point in mesh_info.vertices.iter_mut() {
        *point = trans.transform_point(*point);
    }
    mesh_info
}

const CHUNK_SIZE: usize = 9000;
/// collection of meshes which make a plane of `layout` size
/// (offsets already included; put them all at the origin)
fn hexagonal_plane(layout: &hexx::HexLayout, bounds: &hexx::HexBounds) -> Vec<Mesh> {
    let trans = Transform::from_scale(Vec3::splat(0.95));
    let mut result = Vec::new();
    for chunk in bounds
        .all_coords()
        .map(|hex| mesh_info(layout, hex, Some(trans)))
        .chunks(CHUNK_SIZE)
        .into_iter()
    {
        let mut info = MeshInfo::default();
        // unsure if this actually helps
        info.vertices.reserve(CHUNK_SIZE * 6);
        info.normals.reserve(CHUNK_SIZE);
        info.uvs.reserve(CHUNK_SIZE);
        info.indices.reserve(CHUNK_SIZE * 8);

        for hex in chunk {
            info.merge_with(hex);
        }
        let mut mesh = Mesh::new(bevy::render::render_resource::PrimitiveTopology::TriangleList);
        mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, info.vertices);
        mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, info.normals);
        mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, info.uvs);
        mesh.set_indices(Some(bevy::render::mesh::Indices::U16(info.indices)));
        result.push(mesh);
    }
    info!("{} chunks", result.len());
    result
}

/// single-hex mesh
fn hex_mesh(layout: &hexx::HexLayout) -> Mesh {
    let info = mesh_info(layout, Hex::ZERO, None);
    let mut mesh = Mesh::new(bevy::render::render_resource::PrimitiveTopology::TriangleList);
    mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, info.vertices);
    mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, info.normals);
    mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, info.uvs);
    mesh.set_indices(Some(bevy::render::mesh::Indices::U16(info.indices)));
    mesh
}

pub fn grid_setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    grid: Res<Grid>,
) {
    let single_mesh = meshes.add(hex_mesh(&grid.layout));
    let grid_meshes = hexagonal_plane(&grid.layout, &grid.bounds);
    let default_mat = materials.add(Color::hex("#333").unwrap().into());
    let selected_mat = materials.add(Color::hex("#666").unwrap().into());

    let parent = commands
        .spawn((
            GridParentMarker,
            Name::from("Grid"),
            SpatialBundle::default(),
        ))
        .id();

    for (i, mesh) in grid_meshes.into_iter().enumerate() {
        let mesh = meshes.add(mesh);
        commands
            .spawn((
                ColorMesh2dBundle {
                    mesh: mesh.into(),
                    material: default_mat.clone(),
                    transform: Transform::from_translation(Vec3 {
                        x: 0.,
                        y: 0.,
                        z: -1.,
                    }),
                    ..default()
                },
                Name::from(format!("Grid Chunk {i}")),
            ))
            .set_parent(parent);
    }

    // hexagon we use to hilight the hovered hex
    commands.spawn((
        ColorMesh2dBundle {
            mesh: single_mesh.into(),
            material: selected_mat.clone(),
            transform: Transform::from_xyz(0., 0., 1.0),
            visibility: Visibility::Hidden,
            ..default()
        },
        Name::from("Grid Hover"),
        GridHoverMarker,
    ));
}

fn hilight_hovered_hex(
    mut commands: Commands,
    grid: Option<Res<Grid>>,
    hover: Res<GridMouseLocation>,
    entity: Query<Entity, With<GridHoverMarker>>,
) {
    let Some(grid) = grid else {
        return;
    };
    if hover.is_changed() {
        match hover.location {
            Some(hex) => {
                commands
                    .entity(entity.single())
                    .insert(Visibility::Visible)
                    .insert(grid.get_transform(hex, -0.5).with_scale(Vec3::splat(1.05)));
            }
            None => {
                commands.entity(entity.single()).insert(Visibility::Hidden);
            }
        }
    }
}
