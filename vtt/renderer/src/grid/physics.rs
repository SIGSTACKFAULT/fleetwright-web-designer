//! grid physcis
//! only actually used when compiled as standalone

use crate::grid::{Grid, GridQuery, GridQueryItem};
use bevy::prelude::*;
use hexx::Hex;

/// move all objects by their velocity
pub fn apply_velocity(mut objects: Query<GridQuery>) {
    for GridQueryItem { mut loc, vel, .. } in objects.iter_mut() {
        loc.0 += vel.0;
    }
}

/// clamp positions of everything inside the grid
pub fn clamp_positions(grid: Res<Grid>, mut objects: Query<GridQuery>) {
    for GridQueryItem {
        mut loc, mut vel, ..
    } in objects.iter_mut()
    {
        if !grid.bounds.is_in_bounds(loc.0) {
            vel.0 = Hex::ZERO; // so it doesn't try to slide along the grid edge
            for x in loc.0.line_to(Hex::ZERO) {
                if grid.bounds.is_in_bounds(x) {
                    loc.0 = x;
                    break;
                }
            }
        }
    }
}
