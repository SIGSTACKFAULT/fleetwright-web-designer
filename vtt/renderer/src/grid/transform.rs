use bevy::prelude::*;
use hexx::Hex;

use super::HexVector;

/// for anything which is located on the grid.
/// the real transform will automagically be updated
#[derive(Clone, Component, Debug, Default, Reflect)]
#[reflect(Component)]
pub struct GridLocation(pub Hex);

/// set the z-height on the grid to anything other than zero
#[derive(Clone, Component, Debug, Default, Reflect)]
#[reflect(Component)]
pub enum GridZ {
    #[default]
    Objects,
    Gizmos,
}

impl GridZ {
    fn z(&self) -> f32 {
        match self {
            Self::Objects => 1.0,
            Self::Gizmos => 2.0,
        }
    }
}

/// velocity component used by physics
#[derive(Clone, Component, Debug, Default)]
pub struct GridVelocity(pub Hex);

impl From<Hex> for GridLocation {
    fn from(hex: Hex) -> Self {
        Self(hex)
    }
}

impl From<HexVector> for GridVelocity {
    fn from(velocity: Hex) -> Self {
        Self(velocity)
    }
}

pub fn update_grid_transform(
    mut commands: Commands,
    query: Query<(Entity, &GridLocation, Option<&GridZ>), Changed<GridLocation>>,
    grid: Res<crate::grid::Grid>,
) {
    for (entity, gt, z) in query.iter() {
        commands.entity(entity).insert(grid.get_transform(
            gt.0,
            match z {
                Some(z) => z.z(),
                None => 0.,
            },
        ));
    }
}

/// components for stuff which wants to be on the grid
#[derive(Bundle, Default, Debug)]
pub struct GridSpatialBundle {
    location: GridLocation,
    z: GridZ,
    velocity: GridVelocity,
}
