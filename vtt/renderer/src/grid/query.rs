use bevy::ecs::query::WorldQuery;
use hexx::Hex;

use crate::{
    grid::{GridLocation, GridVelocity},
    objects::{Acceleration, DeltaV, Jump, UUID},
    tools::Selectable,
};

#[derive(WorldQuery)]
#[world_query(mutable)]
pub struct GridQuery {
    pub entity: bevy::ecs::entity::Entity,
    pub uuid: &'static UUID,
    pub loc: &'static mut GridLocation,
    pub vel: &'static mut GridVelocity,
    pub sel: Option<&'static mut Selectable>,
    pub acceleration: Option<&'static mut Acceleration>,
    pub burn: Option<&'static mut DeltaV>,
    pub jump: Option<&'static mut Jump>,
}

#[duplicate::duplicate_item(x; [ GridQueryReadOnlyItem ]; [ GridQueryItem ])]
#[allow(dead_code)]
impl x<'_> {
    pub fn pos(&self) -> Hex {
        self.loc.0
    }

    /// ship's position after it jumps (if it's going to jump this turn)
    /// or it's regular position if it's not jumping
    pub fn after_jump(&self) -> Hex {
        self.jump.as_ref().map(|j| j.0).flatten().unwrap_or(self.pos())
    }

    pub fn after_move(&self) -> Hex {
        self.after_jump() + self.vel.0
    }

    pub fn after_burn(&self) -> Hex {
        self.after_move() + self.burn.as_ref().map(|dv| dv.0).unwrap_or_default()
    }

    /// ship's final position next turn
    pub fn after_turn(&self) -> Hex {
        self.after_burn()
    }

    /// the selected state, or `false` if the component isn't present
    pub fn selected(&self) -> bool {
        self.sel.as_ref().is_some_and(|s| s.selected())
    }

    /// the acceleration, or zero if the component isn't present
    pub fn accel(&self) -> u32 {
        self.acceleration.as_ref().map(|a| a.0).unwrap_or_default()
    }
}

#[allow(dead_code)]
impl GridQueryItem<'_> {
    /// sets the selected state to `to` if this item is selectable.
    /// panics if not selectable.
    pub fn set_selected(&mut self, to: bool) {
        if let Some(selected) = self.sel.as_mut() {
            **selected = match to {
                true => Selectable::Selected,
                false => Selectable::Deselcted,
            }
        } else {
            panic!("tried to set selection state on unselectable object")
        }
    }

    pub fn set_burn(&mut self, to: Hex) {
        if let Some(burn) = self.burn.as_mut() {
            burn.0 = to;
        } else {
            panic!("tried to set burn on unburnable object")
        }
    }
}
