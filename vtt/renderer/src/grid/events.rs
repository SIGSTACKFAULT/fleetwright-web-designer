use crate::grid::Grid;
use bevy::input::mouse::MouseButtonInput;
use bevy::input::ButtonState;
use bevy::prelude::*;
use hexx::Hex;

/// current mouse location
/// `None` indicates either off the grid or off the screen.
#[derive(Resource, Debug, Default)]
pub struct GridMouseLocation {
    pub location: Option<Hex>,
}

/// fired on click outside the grid
#[derive(Event, Debug)]
pub struct GridClickOutside;

/// `location` was clicked
#[derive(Event, Debug)]
pub struct GridClickInside {
    pub location: Hex,
}

/// fired only when the mouse moves between hexes or from off the grid to on the grid.
/// `None` indicates either off the grid or off the screen.
#[derive(Event)]
pub struct GridMouseEvent {
    pub prev: Option<Hex>,
    pub curr: Option<Hex>,
}

impl std::fmt::Debug for GridMouseEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} => {:?}", self.prev, self.curr)
    }
}

#[allow(clippy::too_many_arguments)]
pub fn grid_events(
    mut hover_res: ResMut<GridMouseLocation>,
    mut hover_evt: EventWriter<GridMouseEvent>,
    mut click_inside_evt: EventWriter<GridClickInside>,
    mut click_outside_evt: EventWriter<GridClickOutside>,
    grid: Option<Res<Grid>>,
    windows: Query<&Window, With<bevy::window::PrimaryWindow>>,
    cameras: Query<(&Camera, &GlobalTransform)>,
    mut buttons: EventReader<MouseButtonInput>,
) {
    let Some(grid) = grid else {
        return;
    };
    let window = windows.single();
    let (camera, cam_transform) = cameras.single();
    if let Some(position) = window
        .cursor_position()
        .and_then(|p| camera.viewport_to_world_2d(cam_transform, p))
    {
        let location = grid.layout.world_pos_to_hex(position);
        if grid.bounds.is_in_bounds(location) {
            for ev in buttons.read() {
                if ev.state == ButtonState::Pressed && ev.button == MouseButton::Left {
                    click_inside_evt.send(GridClickInside { location });
                }
            }
            if hover_res.location != Some(location) {
                hover_evt.send(GridMouseEvent {
                    prev: hover_res.location,
                    curr: Some(location),
                });
            }
            hover_res.location = Some(location);
        } else {
            for ev in buttons.read() {
                if ev.state == ButtonState::Pressed && ev.button == MouseButton::Left {
                    click_outside_evt.send(GridClickOutside {});
                }
            }
            // outside the grid
            if hover_res.location.is_some() {
                hover_evt.send(GridMouseEvent {
                    prev: hover_res.location,
                    curr: None,
                });
            }
            hover_res.location = None;
        }
    } else {
        // outside the screen
        if hover_res.location.is_some() {
            hover_evt.send(GridMouseEvent {
                prev: hover_res.location,
                curr: None,
            });
        }
        hover_res.location = None;
    };
}
