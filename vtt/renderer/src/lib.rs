mod camera;
mod chevron;
mod custom_gizmos;
mod grid;
mod interface_rx;
mod interface_tx;
mod objects;
mod standalone_physics;
mod tools;

use bevy::{prelude::*, window::PresentMode};
use std::time::Duration;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn start() {
    let mut app = App::new();
    app.add_plugins(DefaultPlugins.set(WindowPlugin {
        primary_window: Some(Window {
            canvas: Some("#vtt-renderer".to_string()),
            fit_canvas_to_parent: true,
            present_mode: PresentMode::AutoNoVsync,
            ..default()
        }),
        ..default()
    }));
    app.add_plugins(camera::CameraPlugin);
    app.add_plugins(grid::GridPlugin {});
    app.add_plugins(objects::ShipPlugin);
    app.add_plugins(tools::ToolPlugin);
    app.add_plugins(interface_rx::InterfaceRxPlugin);
    app.insert_resource(Time::<Fixed>::from_hz(10.));

    #[cfg(feature = "framepace")]
    {
        app.add_plugins(bevy_framepace::FramepacePlugin);
        app.insert_resource(bevy_framepace::FramepaceSettings {
            limiter: bevy_framepace::Limiter::from_framerate(144.),
        });
    }
    #[cfg(feature = "reactive")]
    reactive_mode(&mut app);
    #[cfg(feature = "fps")]
    {
        app //.
            .add_plugins(bevy_screen_diagnostics::ScreenDiagnosticsPlugin::default())
            .add_plugins(bevy_screen_diagnostics::ScreenFrameDiagnosticsPlugin)
            // .add_plugins(bevy_screen_diagnostics::ScreenEntityDiagnosticsPlugin)
            ;
    }
    #[cfg(any(feature = "inspector", feature = "standalone"))]
    app.add_plugins(
        bevy_inspector_egui::quick::WorldInspectorPlugin::default().run_if(
            bevy::input::common_conditions::input_toggle_active(true, KeyCode::Grave),
        ),
    );
    #[cfg(feature = "standalone")]
    app.add_plugins(standalone_physics::StandalonePhysicsPlugin);
    app.run();
}

/// turn on "reactive mode" which means the event loop only runs when the mouse is moved around
#[allow(dead_code)]
fn reactive_mode(app: &mut App) {
    app.insert_resource(bevy::winit::WinitSettings {
        focused_mode: bevy::winit::UpdateMode::Reactive {
            wait: Duration::from_millis(100),
        },
        unfocused_mode: bevy::winit::UpdateMode::ReactiveLowPower {
            wait: Duration::from_secs(60),
        },
        ..Default::default()
    });
}
