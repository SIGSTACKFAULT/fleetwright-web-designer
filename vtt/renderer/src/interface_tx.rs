//! us talking to JS

use hexx::Hex;
use wasm_bindgen::prelude::*;

#[wasm_bindgen(raw_module = "./vtt-server/schema")]
extern "C" {
    pub type GridObject;
    #[wasm_bindgen(extends=GridObject)]
    pub type ShipObject;
    #[wasm_bindgen(extends=GridObject)]
    pub type MissileObject;

    #[wasm_bindgen(method, getter)]
    pub fn q(this: &GridObject) -> i32;
    #[wasm_bindgen(method, getter)]
    pub fn r(this: &GridObject) -> i32;
    #[wasm_bindgen(method, getter)]
    pub fn uuid(this: &GridObject) -> String;
    #[wasm_bindgen(method, getter)]
    pub fn velocity(this: &GridObject) -> QRHex;
    #[wasm_bindgen(method, getter)]
    pub fn burn(this: &GridObject) -> QRHex;
    #[wasm_bindgen(method, getter)]
    pub fn jump(this: &GridObject) -> Option<QRHex>;
    #[wasm_bindgen(method)]
    pub fn acceleration(this: &ShipObject) -> u32;
}

#[wasm_bindgen(raw_module = "./src/vtt/renderer_interface")]
extern "C" {
    pub fn get_everything() -> Option<Vec<GridObject>>;
    pub fn get_ships() -> Option<Vec<ShipObject>>;
    pub fn get_missiles() -> Option<Vec<MissileObject>>;

    pub fn get_ship(uuid: String) -> Option<ShipObject>;
    pub fn update_primary_selection(uuid: Option<String>);
    pub fn update_burn(uuid: String, q: i32, r: i32);
    pub fn update_jump(uuid: String, q: i32, r: i32);
    pub fn update_jump_null(uuid: String);
    pub fn teleport(uuid: String, q: i32, r: i32);
}

impl GridObject {
    pub fn hex(&self) -> Hex {
        Hex {
            x: self.q(),
            y: self.r(),
        }
    }
}

#[wasm_bindgen(module = "honeycomb-grid")]
extern "C" {
    /// the hex type from honeycomb-grid
    /// exists only to be .into()ed a hexx::Hex
    #[wasm_bindgen(js_name = "Hex")]
    pub type QRHex;
    #[wasm_bindgen(method, getter)]
    pub fn q(this: &QRHex) -> i32;
    #[wasm_bindgen(method, getter)]
    pub fn r(this: &QRHex) -> i32;
}

impl From<QRHex> for Hex {
    fn from(val: QRHex) -> Self {
        Hex {
            x: val.q(),
            y: val.r(),
        }
    }
}
