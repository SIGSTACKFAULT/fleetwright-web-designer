use bevy::prelude::*;
use bevy_pancam::PanCamPlugin;

use crate::grid;

pub struct CameraPlugin;

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(PanCamPlugin {});
        app.add_systems(Startup, spawn_camera);
        app.add_systems(Update, update_pancam);
        app.insert_resource(ClearColor(Color::hex("#222").unwrap()));
        #[cfg(feature = "gizmos_bounds")]
        app.add_systems(Update, draw_bounds_gizmos);
    }
}

fn spawn_camera(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

fn update_pancam(
    mut commands: Commands,
    grid: Option<Res<grid::Grid>>,
    camera: Query<Entity, With<Camera2d>>,
) {
    if let Some(grid) = grid {
        if grid.is_changed() {
            let arbitrary_extra_space = 2.5;
            let diameter = (grid.bounds.radius as f32)
                * grid.layout.hex_size.max_element()
                * arbitrary_extra_space;
            let x = diameter * f32::sqrt(3.);
            let y = diameter * 1.;
            commands
                .entity(camera.single())
                .insert(bevy_pancam::PanCam {
                    grab_buttons: vec![MouseButton::Right, MouseButton::Middle],
                    zoom_to_cursor: true,
                    min_scale: 0.1,
                    // max_scale: Some(100.),
                    max_x: Some(x),
                    min_x: Some(-x),
                    max_y: Some(y),
                    min_y: Some(-y),
                    ..default()
                });
        }
    }
}

#[cfg(feature = "gizmos_bounds")]
fn draw_bounds_gizmos(pancam_query: Query<&bevy_pancam::PanCam>, mut gizmos: Gizmos) {
    let pancam = pancam_query.single();
    let temp = Rect::from_corners(
        Vec2 {
            x: pancam.min_x.unwrap(),
            y: pancam.min_y.unwrap(),
        },
        Vec2 {
            x: pancam.max_x.unwrap(),
            y: pancam.max_y.unwrap(),
        },
    );
    gizmos.rect(Vec3::ZERO, Quat::IDENTITY, temp.size(), Color::GREEN);
}
