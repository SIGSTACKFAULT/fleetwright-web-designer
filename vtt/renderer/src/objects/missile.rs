use bevy::prelude::*;
use uuid::Uuid;

use crate::{
    grid::GridSpatialBundle,
    objects::{ShipResources, UUID},
    tools::{DragNDropAble, Selectable},
};

use super::{Acceleration, DeltaV};

#[derive(Bundle, Default)]
pub struct MissileBundle {
    uuid: UUID,
    acceleration: Acceleration,
    burn: DeltaV,
    grid: GridSpatialBundle,
    name: Name,
    sprite: SpriteBundle,
    drag: DragNDropAble,
    selectable: Selectable,
}

impl MissileBundle {
    /// generate Name component
    fn name(uuid: Uuid) -> Name {
        Name::from(format!("Missile<{:x}>", uuid.as_fields().0))
    }

    pub fn new(sr: &ShipResources) -> Self {
        let uuid = default();
        MissileBundle {
            uuid: UUID(uuid),
            grid: GridSpatialBundle::default(),
            name: Self::name(uuid),
            sprite: SpriteBundle {
                texture: sr.missile.clone(),
                sprite: Sprite {
                    custom_size: Some(Vec2::splat(12.)),
                    ..default()
                },
                ..default()
            },
            ..default()
        }
    }

    /// update the ship and name to have the given uuid
    pub fn with_uuid(mut self, uuid: Uuid) -> Self {
        self.uuid.0 = uuid;
        self.name = Self::name(uuid);
        self
    }

    /// update acceleration
    pub fn with_acceleration(mut self, acceleration: u32) -> Self {
        self.acceleration.0 = acceleration;
        self
    }
}
