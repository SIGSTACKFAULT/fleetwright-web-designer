use super::Acceleration;
use super::DeltaV;
use super::ShipResources;
use super::UUID;
use crate::grid::GridSpatialBundle;
use crate::tools::{DragNDropAble, Selectable};
use bevy::prelude::*;

use uuid::Uuid;

#[derive(Component, Debug, Reflect)]
#[reflect(Component)]
#[derive(Default)]
pub struct Ship {
    // nothing anymore
}

#[derive(Bundle, Default)]
pub struct ShipBundle {
    uuid: UUID,
    ship: Ship,
    acceleration: Acceleration,
    burn: DeltaV,
    grid: GridSpatialBundle,
    name: Name,
    mesh: ColorMesh2dBundle,
    drag: DragNDropAble,
    selectable: Selectable,
}

impl ShipBundle {
    /// generate Name component
    fn name(uuid: Uuid) -> Name {
        Name::from(format!("Ship<{:x}>", uuid.as_fields().0))
    }

    pub fn new(sr: &ShipResources) -> Self {
        let _ship = Ship::default();
        let uuid = Uuid::new_v4();
        ShipBundle {
            uuid: UUID(uuid),
            ship: Ship::default(),
            grid: GridSpatialBundle::default(),
            name: Self::name(uuid),
            mesh: ColorMesh2dBundle {
                mesh: sr.chevron.clone(),
                material: sr.green.clone(),
                ..default()
            },
            ..default()
        }
    }

    /// update the ship and name to have the given uuid
    pub fn with_uuid(mut self, uuid: Uuid) -> Self {
        self.uuid = UUID(uuid);
        self.name = ShipBundle::name(uuid);
        self
    }

    /// update acceleration
    pub fn with_acceleration(mut self, acceleration: u32) -> Self {
        self.acceleration.0 = acceleration;
        self
    }
}
