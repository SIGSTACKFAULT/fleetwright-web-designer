use super::*;
use crate::{
    grid::{GridLocation, GridVelocity},
    interface_tx,
};
use bevy::prelude::*;
use uuid::Uuid;

use super::{ShipBundle, ShipResources};

/// check for ships in [`interface_tx::get_ships`] which don't appear in [`ShipResources::uuids`]
/// and spawn them
pub fn spawn_ships(mut commands: Commands, mut sr: ResMut<ShipResources>) {
    let ships = interface_tx::get_ships().unwrap_or_default();
    for so in ships {
        let uuid = Uuid::parse_str(so.uuid().as_str()).unwrap();
        if !sr.uuids.contains_key(&uuid) {
            let id = commands.spawn(ShipBundle::new(&sr).with_uuid(uuid)).id();
            sr.uuids.insert(uuid, id);
        }
    }
}

/// check for ships in [`interface_tx::get_missiles`] which don't appear in [`ShipResources::uuids`]
/// and spawn them
pub fn spawn_missiles(mut commands: Commands, mut sr: ResMut<ShipResources>) {
    let missiles = interface_tx::get_missiles().unwrap_or_default();
    for so in missiles {
        let uuid = Uuid::parse_str(so.uuid().as_str()).unwrap();
        if !sr.uuids.contains_key(&uuid) {
            info!("spawning missile {} {} {}", so.uuid(), so.q(), so.r());
            let id = commands
                .spawn(
                    MissileBundle::new(&sr)
                        .with_uuid(uuid)
                        .with_acceleration(10),
                )
                .id();
            info!("spawned {:?}", id);
            sr.uuids.insert(uuid, id);
        }
    }
}

/// get updates on:
/// - positions
/// - velocities
/// - burns
pub fn update_objects(mut commands: Commands, sr: Res<ShipResources>) {
    for external_object in interface_tx::get_everything().unwrap() {
        let uuid = Uuid::parse_str(external_object.uuid().as_str()).unwrap();
        let entity = *sr
            .uuids
            .get(&uuid)
            .unwrap_or_else(|| panic!("enity for GridObject {} should already exist!", uuid));
        commands.entity(entity).insert((
            GridLocation(external_object.hex()),
            GridVelocity(external_object.velocity().into()),
            DeltaV(external_object.burn().into()),
        ));
    }
}

/// get updates on
/// - accelrations
/// - jumps
pub fn update_ships(
    //
    mut commands: Commands,
    sr: Res<ShipResources>,
) {
    let state_ships = interface_tx::get_ships();
    if let Some(state) = state_ships {
        for external_ship in state.iter() {
            let uuid = Uuid::parse_str(external_ship.uuid().as_str()).unwrap();
            if let Some(entity) = sr.uuids.get(&uuid) {
                commands.entity(*entity).insert((
                    Acceleration(external_ship.acceleration()),
                    Jump(external_ship.jump().map(|h| h.into())),
                ));
            } else {
                warn!("Ship {} should have already been spawned!", uuid);
            }
        }
    }
}

/// despawn objects which no longer appear in [`interface_tx::get_everything`]
pub fn despawn_objects(
    //
    mut commands: Commands,
    mut sr: ResMut<ShipResources>,
) {
    let objs = interface_tx::get_everything().unwrap_or_default();
    let external_uuids: Vec<_> = objs
        .iter()
        .map(|s| Uuid::parse_str(s.uuid().as_str()).unwrap())
        .collect();

    sr.uuids.retain(|uuid, entity| {
        if external_uuids.contains(uuid) {
            true
        } else {
            commands.entity(*entity).despawn_recursive();
            false
        }
    });
}
