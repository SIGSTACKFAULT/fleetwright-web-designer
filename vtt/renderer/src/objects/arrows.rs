use super::DeltaV;
use crate::{
    custom_gizmos,
    grid::{Grid, GridQueryReadOnly},
    tools::Tool,
};
use bevy::prelude::*;

pub fn draw_velocity(
    mut gizmos: Gizmos,
    grid: Res<Grid>,
    _tool: Res<State<Tool>>,
    ships: Query<GridQueryReadOnly>,
) {
    for item in ships.iter() {
        let from = item.after_jump();
        let to = item.after_move();
        if from.distance_to(to) > 0 {
            custom_gizmos::arrow(
                &grid,
                &mut gizmos,
                item.after_jump(),
                item.after_move(),
                Color::WHITE,
            );
        }
    }
}

pub fn draw_burn(
    mut gizmos: Gizmos,
    grid: Res<Grid>,
    tool: Res<State<Tool>>,
    ships: Query<GridQueryReadOnly>,
) {
    for item in ships.iter() {
        if let Some(DeltaV(_burn)) = item.burn {
            if (**tool == Tool::ShipBurn || **tool == Tool::ShipBurnAlt) && item.selected() {
                // ship is selected and we're holding Q
                // another system will be drawing the new burn vector
            } else {
                let from = item.after_move();
                let to = item.after_burn();
                if from.distance_to(to) > 0 {
                    custom_gizmos::arrow(&grid, &mut gizmos, from, to, Color::CYAN)
                }
            }
        }
    }
}

pub fn draw_jump(
    mut gizmos: Gizmos,
    grid: Res<Grid>,
    tool: Res<State<Tool>>,
    ships: Query<GridQueryReadOnly>,
) {
    for item in ships.iter() {
        if **tool == Tool::ShipJump && item.selected() {
            continue;
        }
        let from = item.loc.0;
        let to = item.after_jump();
        if from.distance_to(to) > 0 {
            custom_gizmos::arrow(&grid, &mut gizmos, from, to, Color::RED);
        }
    }
}
