mod arrows;
mod get_updates;
mod missile;
mod ship;

use crate::interface_rx::OnStateChange;
use bevy::ecs::system::RunSystemOnce;
use bevy::utils::HashMap;
use bevy::{prelude::*, sprite::Mesh2dHandle};
pub use get_updates::*;
use hexx::Hex;
pub use missile::MissileBundle;
pub use ship::{Ship, ShipBundle};
use uuid::Uuid;

/// the component that stores a `Uuid`
/// not to be confused with the similarly named `Uuid` type
#[derive(Component, Debug)]
pub struct UUID(pub Uuid);

impl Default for UUID {
    fn default() -> Self {
        Self(Uuid::new_v4())
    }
}

/// Next turn's Delta-V
/// (vector)
#[derive(Component, Debug, Default)]
pub struct DeltaV(pub Hex);

/// Maxiumum Acceleration
#[derive(Component, Debug, Default)]
pub struct Acceleration(pub u32);

/// Jumping to this position on the next turn
#[derive(Component, Debug, Default)]
pub struct Jump(pub Option<Hex>);

pub struct ShipPlugin;
impl Plugin for ShipPlugin {
    fn build(&self, app: &mut App) {
        app //
            .init_resource::<ShipResources>()
            .add_systems(
                Update,
                (arrows::draw_burn, arrows::draw_velocity, arrows::draw_jump),
            )
            .register_type::<Ship>()
            .register_type::<ShipResources>();
        if cfg!(feature = "bound") {
            app.add_systems(
                OnStateChange,
                (
                    spawn_ships,
                    spawn_missiles,
                    despawn_objects,
                    update_objects,
                    update_ships,
                )
                    .chain(),
            );
        }
    }
}

#[derive(Resource, Clone, Reflect)]
#[allow(dead_code)]
pub struct ShipResources {
    chevron: Mesh2dHandle,
    red: Handle<ColorMaterial>,
    green: Handle<ColorMaterial>,
    blue: Handle<ColorMaterial>,
    pub missile: Handle<Image>,
    /// mapping from Ship UUID to Bevy Entity
    pub uuids: HashMap<Uuid, Entity>,
}

impl FromWorld for ShipResources {
    fn from_world(world: &mut World) -> Self {
        world.run_system_once(
            |asset_server: Res<AssetServer>,
             mut meshes: ResMut<Assets<Mesh>>,
             mut colours: ResMut<Assets<ColorMaterial>>| {
                ShipResources {
                    chevron: meshes.add(crate::chevron::chevron()).into(),
                    red: colours.add(Color::RED.into()),
                    green: colours.add(Color::GREEN.into()),
                    blue: colours.add(Color::BLUE.into()),
                    missile: asset_server.load("missile.qoi"),
                    uuids: HashMap::new(),
                }
            },
        )
    }
}
