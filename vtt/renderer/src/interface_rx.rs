/// Stuff for JS talking to us
use bevy::{
    ecs::{schedule::ScheduleLabel, system::NonSend},
    prelude::*,
};
use std::sync::{
    mpsc::{channel, Receiver, Sender, TryRecvError},
    RwLock,
};
use wasm_bindgen::prelude::*;

/// triggered by the js side when the server sends us some new data
#[derive(ScheduleLabel, Hash, PartialEq, Eq, Debug, Copy, Clone)]
pub struct OnStateChange;

/// used to move data into the bevy App world
static INTO_APP: RwLock<Option<Sender<InterfaceMessage>>> = RwLock::new(None);

fn into_app_sender() -> Sender<InterfaceMessage> {
    INTO_APP.read().unwrap().as_ref().unwrap().clone()
}

#[derive(Debug)]
enum InterfaceMessage {
    /// indicates that a state change came down from the server
    /// triggers the [`OnStateChange`] schedule
    StateChange,
}

pub struct InterfaceRxPlugin;
impl Plugin for InterfaceRxPlugin {
    fn build(&self, app: &mut App) {
        app //
            .init_non_send_resource::<InterfaceRx>()
            .add_systems(Update, (check_messages,));
    }
}

struct InterfaceRx {
    rx: Receiver<InterfaceMessage>,
}

impl FromWorld for InterfaceRx {
    fn from_world(_world: &mut bevy::prelude::World) -> Self {
        info!("init InterfaceRX");
        let (tx, rx) = channel::<InterfaceMessage>();
        let mut lock = INTO_APP.try_write().unwrap();
        *lock = Some(tx);
        InterfaceRx { rx }
    }
}

/// check the channel and do stuff
fn check_messages(
    //
    res: NonSend<InterfaceRx>,
    mut commands: Commands,
) {
    loop {
        match res.rx.try_recv() {
            Ok(msg) => {
                info!("rx {:?}", msg);
                match msg {
                    InterfaceMessage::StateChange => commands.add(|world: &mut World| {
                        info!("running OnStateChange schedule");
                        let _ = world.try_run_schedule(OnStateChange);
                    }),
                }
            }
            Err(TryRecvError::Empty) => break,
            Err(TryRecvError::Disconnected) => panic!("the sender disconnected!"),
        }
    }
}

/// just for visual seperation
mod send {
    use super::*;

    #[wasm_bindgen]
    pub fn send_state_change() {
        into_app_sender()
            .send(InterfaceMessage::StateChange)
            .unwrap();
    }
}
