//! Generates chevron mesh for ships
//! someday will be a whole system which generates all the meshes we need for brackets and puts them in a resource

use bevy::prelude::*;
use bevy::render::{mesh::Indices, render_resource::PrimitiveTopology};

fn verts() -> Vec<Vec3> {
    vec![
        Vec3::ZERO,
        Vec3 {
            x: -5.,
            y: -5.,
            z: 0.,
        },
        Vec3 {
            x: -5.,
            y: 0.,
            z: 0.,
        },
        Vec3 {
            x: 0.,
            y: 5.,
            z: 0.,
        },
        Vec3 {
            x: 5.,
            y: 0.,
            z: 0.,
        },
        Vec3 {
            x: 5.,
            y: -5.,
            z: 0.,
        },
        Vec3 {
            x: 5.,
            y: -5.,
            z: 0.,
        },
    ]
}

pub fn chevron() -> Mesh {
    let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
    let verts = verts();
    let len = verts.len() as u16;
    mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, verts);
    let indicies = (1..len - 1).flat_map(|i| vec![0, i, i + 1]).collect();
    mesh.set_indices(Some(Indices::U16(indicies)));
    mesh
}
