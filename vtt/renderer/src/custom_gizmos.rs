use crate::grid::{Grid, HEX_SIZE};
use bevy::prelude::*;
use hexx::Hex;
use std::f32::consts::TAU;

/// arrow `from` -> `to`
pub fn arrow(
    //
    grid: &Res<Grid>,
    gizmos: &mut Gizmos<'_>,
    from: Hex,
    to: Hex,
    color: Color,
) {
    let tip = HEX_SIZE.max_element() / 2.;
    let start = grid.layout.hex_to_world_pos(from);
    let end = grid.layout.hex_to_world_pos(to);
    let theta = f32::atan2(end.y - start.y, end.x - start.x); // radians
    let head1 = Vec2 {
        x: end.x + f32::cos(theta + TAU * (3. / 8.)) * tip,
        y: end.y + f32::sin(theta + TAU * (3. / 8.)) * tip,
    };
    let head2 = Vec2 {
        x: end.x + f32::cos(theta + TAU * (5. / 8.)) * tip,
        y: end.y + f32::sin(theta + TAU * (5. / 8.)) * tip,
    };
    gizmos.line_2d(start, end, color);
    gizmos.line_2d(end, head1, color);
    gizmos.line_2d(end, head2, color);
}

/// Big hexagon which encloses `radius` around `origin`.
/// Needs color argument
pub fn big_hexagon(
    //
    grid: &Res<Grid>,
    gizmos: &mut Gizmos,
    origin: Hex,
    radius: u32,
) {
    let radius = (radius as f32 + 0.75) * f32::sqrt(3.);
    let corners = grid.layout.hex_corners(Hex::ZERO);
    let start = grid.layout.hex_to_world_pos(origin);
    for (a, b) in corners.iter().clone().zip(corners.iter().cycle().skip(1)) {
        gizmos.line_2d(
            (*a * radius).rotate(Vec2::Y) + start,
            (*b * radius).rotate(Vec2::Y) + start,
            Color::WHITE,
        );
    }
}
