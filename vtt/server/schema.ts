import { Schema, type } from "@colyseus/schema";
import { MapSchema } from "@colyseus/schema";
import { Grid, Hex, line } from "honeycomb-grid";
import { v4 as uuidv4 } from "uuid";
import { parse } from "yaml";

import { acceleration as simulate_acceleration } from "@/engines";
import { Ship } from "@/ship/Ship";

// https://docs.colyseus.io/colyseus/state/schema/

export class HexVector extends Schema {
    @type("int64") q: number;
    @type("int64") r: number;
    constructor(q: number = 0, r: number = 0) {
        super();
        this.q = q;
        this.r = r;
    }
    iszero() {
        return this.q == 0 && this.r == 0;
    }
    isnonzero() {
        return !this.iszero();
    }
}

export abstract class GridObject extends Schema {
    @type("string") uuid: string;
    @type("int64") q: number;
    @type("int64") r: number;
    @type(HexVector) velocity: HexVector; // current velocity
    @type(HexVector) burn: HexVector; // upcoming DV

    constructor(uuid: string, q: number, r: number, dq: number = 0, dr: number = 0) {
        super();
        this.uuid = uuid;
        this.q = q;
        this.r = r;
        this.velocity = new HexVector(dq, dr);
        this.burn = new HexVector(0, 0);
    }

    apply_velocity() {
        this.q += this.velocity.q;
        this.r += this.velocity.r;
    }

    apply_burn() {
        this.velocity.q += this.burn.q;
        this.velocity.r += this.burn.r;
    }

    /**
     * if necessary, clamp position so it's on the grid
     * @param grid
     */
    clamp_position(grid: Grid<Hex>) {
        if (grid.getHex({ q: this.q, r: this.r }) == undefined) {
            // outside the grid
            // draw a line towards the origin and get the first one that's actually in the grid
            const clamped = grid
                .traverse(
                    line({
                        start: { q: this.q, r: this.r },
                        stop: { q: 0, r: 0 },
                    }),
                )
                .toArray()[0];
            this.q = clamped.q;
            this.r = clamped.r;
            this.velocity.q = this.velocity.r = 0;
        }
    }
}

export class ShipObject extends GridObject {
    constructor(yaml: string, q: number, r: number, dq: number = 0, dr: number = 0) {
        super(uuidv4(), q, r, dq, dr);
        this.ship_yaml = yaml;
        this.jump = null;
    }
    @type("string") ship_yaml: string;
    @type(HexVector) jump: HexVector | null; // upcoming DV
    _cached_hydrated: Ship | undefined;

    hydrate(): Ship {
        this._cached_hydrated ??= new Ship(parse(this.ship_yaml));
        return this._cached_hydrated;
    }

    acceleration() {
        return Math.floor(simulate_acceleration(this.hydrate()));
    }

    velocity_tip(): HexVector {
        return new HexVector(this.q + this.velocity.q, this.r + this.velocity.r);
    }

    toString(): string {
        return `ShipObject("${this.hydrate().name.slice(0, 10)}...", ${this.q}, ${this.r})`;
    }
}

export class MissileObject extends GridObject {
    constructor(q: number, r: number, dq: number = 0, dr: number = 0) {
        super(uuidv4(), q, r, dq, dr);
    }
}

export class Player extends Schema {
    @type("string") id: string;
    @type("boolean") ready: boolean;
    constructor(id: string) {
        super();
        this.id = id;
        this.ready = false;
    }
}

export class MySchema extends Schema {
    constructor() {
        super();
        this.grid_radius = 100;
        this.ships = new MapSchema();
        this.missiles = new MapSchema();
        this.players = new MapSchema();
    }
    @type("int32") grid_radius: number = 100;
    @type({ map: ShipObject }) ships: MapSchema<ShipObject>;
    @type({ map: MissileObject }) missiles: MapSchema<MissileObject>;
    @type({ map: Player }) players: MapSchema<Player>;

    /**
     * iterable over all the `GridObject`s
     */
    *everything(): Iterable<GridObject | ShipObject | MissileObject> {
        for (const ship of this.ships.values()) {
            yield ship;
        }
        for (const missile of this.missiles.values()) {
            yield missile;
        }
    }

    get_uuid(uuid: string): GridObject | ShipObject | MissileObject | undefined {
        return this.ships.get(uuid) ?? this.missiles.get(uuid) ?? undefined;
    }
}
