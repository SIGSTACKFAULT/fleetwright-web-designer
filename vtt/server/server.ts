import { Room, Server } from "colyseus";
import type { Client } from "colyseus";
import { Grid, Hex, Orientation, defineHex, spiral } from "honeycomb-grid";
import * as winston from "winston";

import "./data_dump_helper";
import { create_room_logger } from "./logging";
import { HexVector, MissileObject, MySchema, Player, ShipObject } from "./schema";
import { data_promise } from "@/globals";

const server = new Server();
class GameRoom extends Room<MySchema> {
    patchRate = 1000;
    autoDispose = false;
    logger!: winston.Logger;
    grid!: Grid<Hex>;

    onCreate(options: any) {
        this.logger = create_room_logger(this);
        this.logger.info("room created", { options });
        this.setState(new MySchema());
        this.grid = new Grid( // TODO this in the state
            defineHex({ orientation: Orientation.POINTY }),
            spiral({ radius: this.state.grid_radius }),
        );

        this.onMessage("*", (client, type, message) => {
            this.log_client_event(client, `*${type}`, message, "warn");
        });
        this.onMessage("chat", (client, message) => {
            const content = String(message);
            this.log_client_event(client, "chat", content);
            this.broadcast("chat", { author: client.id, content });
        });
        this.onMessage("add_ship", (client, message) => {
            const uuid = this.add_ship(message.yaml, message.q, message.r);
            this.log_client_event(client, "add_ship", { q: message.q, r: message.r, uuid: uuid });
        });
        this.onMessage("add_missile", (client, message) => {
            const uuid = this.add_missile(message.q, message.r);
            this.log_client_event(client, "add_missile", {
                q: message.q,
                r: message.r,
                uuid: uuid,
            });
        });
        this.onMessage("delete_ship", (client, message) => {
            this.log_client_event(client, "delete_ship", { uuid: message.uuid });
            this.state.ships.delete(message.uuid);
        });
        this.onMessage("change_burn", (client, message) => {
            this.log_client_event(client, "change_burn", message);
            const object = this.state.get_uuid(message.uuid);
            object!.burn = new HexVector(message.vector.q, message.vector.r);
            this.broadcastPatch();
        });
        this.onMessage("change_jump", (client, message) => {
            this.log_client_event(client, "change_jump", message);
            const ship = this.state.ships.get(message.uuid);
            if (ship === undefined) {
                this.logger.warn(`${message.uuid} does not exist`);
                return;
            }
            if ("jump" in ship) {
                if (message.vector == null) {
                    ship.jump = null;
                } else {
                    ship.jump = new HexVector(message.vector.q, message.vector.r);
                }
                this.broadcastPatch();
            } else {
                this.logger.warn(`${message.uuid} is not a ship`);
            }
        });
        this.onMessage("teleport", (client, message) => {
            this.log_client_event(client, "teleport", message);
            const object = this.state.get_uuid(message.uuid);
            if ("pos" in message) {
                this.logger.info("teleport", message.pos);
                object!.q = message.pos.q;
                object!.r = message.pos.r;
            }
            this.broadcastPatch();
        });
        this.onMessage("ready_up", (client, message) => this.ready_up(client, message));
    }

    async ready_up(client: Client, { state }: { state: boolean }) {
        this.state.players.get(client.id)!.ready = state;
        // check if all players are ready
        this.log_client_event(client, "ready_up", { state });
        if (this.are_all_players_ready()) {
            this.state.players.forEach((p) => (p.ready = false));
            this.step();
        }
    }

    are_all_players_ready(): boolean {
        const ready_count = Array.from(this.state.players.values()).reduce<number>(
            (prev, current) => prev + (current.ready ? 1 : 0),
            0,
        );
        const player_count = this.state.players.size;
        if (ready_count == player_count) {
            this.logger.info("all players ready");
            return true;
        } else {
            return false;
        }
    }

    step() {
        this.logger.info("step");
        this.apply_jump();
        this.apply_burn();
        this.apply_velocity();
        this.broadcast("step");
        this.broadcastPatch();
    }

    /**
     * teleports all ships to their jump location
     */
    apply_jump() {
        this.state.ships.forEach((ship) => {
            if (ship.jump != null) {
                ship.q = ship.jump.q;
                ship.r = ship.jump.r;
                ship.jump = null;
            }
        });
    }

    /**
     * adds the burn of all ships to their velocity
     */
    apply_burn() {
        for (const obj of this.state.everything()) {
            obj.apply_burn();
            obj.burn.q = obj.burn.r = 0;
        }
    }

    /**
     * moves all ships by their velocity
     */
    apply_velocity() {
        for (const obj of this.state.everything()) {
            obj.apply_velocity();
            obj.clamp_position(this.grid);
        }
    }

    add_ship(yaml: string, q: number, r: number) {
        const obj = new ShipObject(yaml, q, r, 0, 0);
        this.state.ships.set(obj.uuid, obj);
        return obj.uuid;
    }

    add_missile(q: number, r: number) {
        const obj = new MissileObject(q, r, 0, 0);
        this.state.missiles.set(obj.uuid, obj);
        return obj.uuid;
    }

    onJoin(client: Client, options: any) {
        this.log_client_event(client, "joined", { options });
        this.broadcast("chat", { author: "system", content: `*${client.id} joined*` });
        const player = new Player(client.id);
        this.state.players.set(client.id, player);
    }
    onLeave(client: Client, consented?: boolean) {
        this.log_client_event(client, "left", { consented });
        this.state.players.delete(client.id);
        this.broadcast("chat", { author: "system", content: `*${client.id} left*` });
    }

    /**
     * standardized log incoming message
     */
    log_client_event(client: Client, message: string, meta: any = {}, level: string = "info") {
        this.logger.log(level, message, {
            clientId: client.id,
            ...meta,
        });
    }
}

async function main() {
    const port = parseInt(process.env.PORT ?? "9119");
    server.define("main", GameRoom);
    await data_promise;
    server.listen(port, undefined, undefined, () => {
        winston.info("server up", { port });
    });
}

main();
