import { defineConfig } from "cypress";
import { readFileSync } from "fs";
import { parse } from "yaml";

const samples = parse(readFileSync("./static/samples.yaml").toString());
const data = parse(readFileSync("./static/junctspace.yaml").toString());
const government = parse(readFileSync("./static/government.yaml").toString());

export default defineConfig({
    e2e: {
        specPattern: "cypress/e2e/**/*.{cy,spec}.{js,jsx,ts,tsx}",
        retries: { openMode: 0, runMode: 2 },
        baseUrl: "http://localhost:4173",
        video: false,
    },
    component: {
        specPattern: "cypress/unit/**/*.{cy,spec}.{js,ts,jsx,tsx}",
        retries: { openMode: 0, runMode: 2 },
        devServer: {
            framework: "vue",
            bundler: "vite",
        },
        video: false,
    },
    env: {
        samples,
        data,
        government,
    },
    numTestsKeptInMemory: 10,
});
