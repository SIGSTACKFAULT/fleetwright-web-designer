import { readFileSync, writeFileSync } from "fs";
import { globSync } from "glob";
import { parse, stringify } from "yaml";

export default function compile() {
    // open relative to the script so we're cwd-agnostic
    const files = globSync(module.path + "/fleetwright-meta/**/*.fw");
    const result = {
        components: {},
        resources: {},
        missiles: {},
        combat_stations: {},
    };
    for (const file of files) {
        const it = parse(readFileSync(file).toString());
        Object.assign(result.components, it.components, it.component);
        Object.assign(result.resources, it.resources);
        Object.assign(result.missiles, it.missiles, it.missile);
        Object.assign(result.combat_stations, it.combat_stations);
    }
    const yaml_data = stringify(result, { sortMapEntries: true });
    const output = module.path + "/static/junctspace.yaml";
    writeFileSync(output, yaml_data);
    console.log(`built ${output} (${Math.round(yaml_data.length / 1024)}kib)`);
}

if (require.main == module) {
    compile();
}
