# fleetwright-web-designer

Fleetwright Ship Designer written in Vue.

This contains the actual editor code.
Component statistics and such are in [fleetwright-meta](https://gitlab.com/SIGSTACKFAULT/fleetwright-meta)

## Recommended IDE Setup

- [VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur)
  - Volar Takeover Mode: disable (workspace) `vscode.typescript-language-features`
- [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Project Setup

[install rust](https://www.rust-lang.org/tools/install) to compile vtt-renderer.

```sh
yarn
git submodule init
git submodule update
```

### Compile and Hot-Reload for Development

```sh
yarn dev
```

### Compile and Minify for Production

(builds to `public` for CI reasons)

```sh
yarn build
```

## Cypress tests

```
yarn build
yarn test:unit:ci
yarn test:e2e:ci
```

## VTT-Renderer

Bevy "game" which is the main VTT view.  
[Install rust and cargo](https://www.rust-lang.org/tools/install)

**run as standalone desktop app:**

```
yarn vtt-standalone
```

**build for web:**

```
cargo install wasm-pack
yarn wasm-pack
```

## Contributing

- anything in issues is fair game.
  - make a MR for any issue as you wish
- I've littered the project with comments including `#TODO` and barely any of them are in gitlab
- MRs MUST pass all pipelines
  - use `npm run lint:fix` and `npm run format:fix`

## Vibe Sheet

- Comp/Con
- ASCBI
- EVE in-game ship editor
  - PyFA
- NEBULOUS
- Factorio (Mod system)
