/** @type {import('tailwindcss').Config} */

module.exports = {
    content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx,scss}"],
    theme: {
        extend: {},
        fontFamily: {
            mono: ["Fira Code", "monospace"],
        },
    },
    plugins: [],
};
