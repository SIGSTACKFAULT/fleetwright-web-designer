import vue from "@vitejs/plugin-vue";
import { execSync } from "node:child_process";
import { URL, fileURLToPath } from "node:url";
import { defineConfig } from "vite";
import wasmPack from "vite-plugin-wasm-pack";

import compile from "./compile_dump";

let web_describe;
let meta_describe;

try {
    web_describe = execSync("git describe --dirty").toString().trim();
} catch {
    web_describe = "<MISSING>";
}
try {
    meta_describe = execSync("git -C ./fleetwright-meta describe --always --dirty")
        .toString()
        .trim();
} catch {
    meta_describe = "<MISSING>";
}

compile();

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        wasmPack("./vtt/renderer"),
        //
    ],
    publicDir: "static",
    base: "./",
    resolve: {
        alias: {
            "@": fileURLToPath(new URL("./src", import.meta.url)),
            "@server": fileURLToPath(new URL("./vtt/server", import.meta.url)),
        },
    },
    define: {
        __WEB_GIT_DESCRIBE__: JSON.stringify(web_describe),
        __META_GIT_DESCRIBE__: JSON.stringify(meta_describe),
        __APP_NAME__: "'Fleetwright Ship Designer'",
        __APP_ACRONYM__: "'FSD'",
    },
    build: {
        sourcemap: true,
        outDir: "public",
        rollupOptions: {
            output: {
                manualChunks: {
                    // local
                    globals: ["src/globals.ts"],
                    Ship: ["src/ship/Ship.ts"],
                    Component: ["src/types/Component.ts"],
                    Faction: ["src/faction/Faction.ts"],
                    // packages
                    mathjs: ["mathjs"],
                    pixi: ["pixi.js", "@pixi/layers"],
                },
            },
        },
    },
});
