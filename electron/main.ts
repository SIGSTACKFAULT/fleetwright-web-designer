const { app, BrowserWindow } = require("electron");
const path = require("path");

function create_window() {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            preload: path.join(__dirname, "preload.ts"),
        },
    });
    win.loadFile("public/index.html");
}

app.whenReady().then(() => {
    create_window();
    app.on("activate", () => {
        if (BrowserWindow.getAllWindows().length === 0) {
            create_window();
        }
    });
});
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});
